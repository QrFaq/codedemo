import cv2, threading, random
from cmds import CMDS, CMARKERS

class Detector(threading.Thread):
    """
        Crazy driver recognition class.
        Processing steps:
        0. thread is working in loop after running by master
        1. if there wasn't any master `shutdown`\kill command
            thread is continue to run
        2. processing batch will be read in FIFO order from
            the databse (video fragments/batch storage src) queue
        3. Process the batch (plug explanation):
        3.1. for each selected frame from the batch
        3.2. apply `crazy driver` recognition algorithm,
            in plug case: select one of the `CMARKERS` states
            randomly
        3.3. After `marking` process for each frame:
            batch marker state will be choosed by selecting
            the most fruquent state in batch via all batch-frame
            analysis, i.e. if batch=[
                F0-state, F1-state, F2-state, F3-state]=[0,1,1,2],
            where more frequent state via all frames is `1` and as
            a result `batch-state`=1
        4. Marked batch is placed into processed result queue for
            catching and result visualization by `Gui` (in that case),
            `output queue element`=(src link, batch, mark)
    """
    def __init__(self, queue, cqueue, oqueue, logger, name="Detector", args=(), kwargs=None):
        """
            queue - queue of batches for processing
            cqueue - queue of thread commands
            oqueue - queue of batches for visualization
            logger - MultiHandler logger
            *batch: video sample
        """
        threading.Thread.__init__(self, name=name, args=(), kwargs=None)
        self.daemon = True

        self.dbq = queue# batch db
        self.cmdq = cqueue# queue of thread commands
        self.outpuq = oqueue# queue for storing recognition results
        self.id = threading.get_ident()
        self.lock = threading.Lock()

        self.logger = logger

    def recognFrame(self, frame):
        """
            Recognition plug:
            randomly choose frame mark based on `CMARKERS`

            frame: opencv img, src image
            return: one of the keys above (frame mark)
        """
        fmark = -1
        try:
            if frame is not None:
                fmark = random.choice([
                    CMARKERS["no crazy"],
                    CMARKERS["crazy on left"],
                    CMARKERS["crazy on right"],
                    CMARKERS["not recognized"]
                ])
        except Exception as error:
            self.logger.error(str(error))
        return fmark

    def recognBatch(self, batch):
        """
            Crazy driver batch recognition plug.
            Plug logick:
            1. analyze each frame (frames marking)
            2. mark batch by more frequent element from marked frames

            batch - list of opencv imgs, video frames list
            return: batch mark as key from `recognFrame`
        """
        bmark = -1
        try:
            # frame marking
            fmarks = [ self.recognFrame(frame) for frame in batch ]
            
            # batch marking
            most_frequent = lambda List: max(set(List), key = List.count) 
            bmark = most_frequent(fmarks)
        except Exception as error:
            self.logger.error(str(error))
        return bmark

    def recogn(self):
        """
            Recognize crazy driver in batch from database
            queue (batch-queue) and store results in output queue.
            `output queue element`=(src link, batch, mark)
        """
        try:
            if not self.dbq.empty():
                self.logger.debug("batch recognition")
                vlink, batch = self.dbq.get()
                bmark = self.recognBatch(batch)
                self.outpuq.put((vlink, batch, bmark))
        except Exception as error:
            self.logger.error(str(error))
    
    def run(self):
        self.logger.info("thread start")
        while True:
            try:
                # track master thread command
                if not self.cmdq.empty():
                    cmd = self.cmdq.get()
                    self.logger.info("cmd="+str(cmd))
                    if cmd == CMDS["kill"]:
                        self.logger.info("kill")
                        break
                
                # start batch processing
                self.recogn()
            except Exception as error:
                self.logger.error(str(error))
        self.logger.info("thread shutdown")
        