import cv2, time, threading
from queue import Queue
from MultiHandler import create_logger
from FrameReader import FrameReader
from Gui import Gui
from Detector import Detector
from cmds import CMDS

class DetectorDemo:
    """
        Small functional multythread demonstration of `crazy
        driver detector` (with some plugs) based on video\rtsp
        video sources (supported by OpenCV lib). Demo could be
        used for processin images with small changes.
        
        Current Working demo steps:
        0. Initialize launch paramters:
            - videos source lists;
            - thread name (for logging purposes)
            - log storage path
            - batch size, i.e. number of readed frames
                from each video src
        1. Setup and launch threads:
            - frame reader threads (one thread per camera)
            - one detector thread for processing batches
            - one gui thread for result visualization
            - main thread is used as master-thread to controll
                all above
        2. Master thread sends `read batch` command to the
            frame-reader threads and asks them to read `n`
            batches to queue from each and wait 1s just as an
            example
        3. Processing threads (detector & gui) are waiting for
            putted batches in queue and is starting to process
            them as soon as they been loaded
        4. Master waits 10 s and sending `shut down` command to
            threads and wait untill each thread will be shutted
            down after the command was sent
    """
    def __init__(self, settings):
        self.gLogger = create_logger(
            settings["logger_name"],
            settings["log_fpath"]
        )
        self.gLogger.info("DetectorDemo test")

        self.threads = []
        self.threads_cmds = []
        self.batch_queue = Queue()
        self.drawq = Queue()

        # list of processing links
        self.vlink_list = settings["vlink_list"]
        self.batch_size = settings["batch_size"]

        self.nreaders = None

    def init_threads(self):
        """
            Initialize threads with there rounding (cmd ques, data ques, etc)
        """
        # create one frame-reader thread per link
        for ind, vlink in enumerate(self.vlink_list): 
            self.threads_cmds.append(Queue())
            self.threads.append(FrameReader(
                vlink=vlink,
                queue=self.batch_queue,
                cqueue=self.threads_cmds[ind],
                logger=self.gLogger,
                bsize=self.batch_size
            ))
        self.nreaders = len(self.vlink_list)

        # create detector thread
        self.threads_cmds.append(Queue())
        self.threads.append(Detector(
            queue=self.batch_queue,
            cqueue=self.threads_cmds[self.nreaders],
            oqueue=self.drawq,
            logger=self.gLogger
        ))

        # add gui thread
        self.threads_cmds.append(Queue())
        self.threads.append(Gui(
            queue=self.drawq,
            cqueue=self.threads_cmds[-1],
            logger=self.gLogger
        ))
    
    def run_threads(self):
        for thr in self.threads:
            thr.start()
            time.sleep(0.1)

    def shutdown_threads(self):
        """
            Shutdown threads & wait until they finished
        """
        for i, thr in enumerate(self.threads):
            self.threads_cmds[i].put(CMDS["kill"])
            thr.join()

    def run(self, n=25):
        """
            Extract n batches from each frame and process them

            n: number of batches to rad from each src video link
        """
        self.gLogger.info("start demo")
        self.gLogger.info( "nbatches=%s"%(self.batch_queue.qsize()) )
        for ind, thr in enumerate(self.threads[0:self.nreaders]):
            self.gLogger.info("> ind=%s, %s"%(ind, len(self.threads)) )
            for i in range(0, n):
                self.threads_cmds[ind].put(CMDS["get batch"])
            time.sleep(1)
        
        time.sleep(10)
        self.shutdown_threads()
        self.gLogger.info("shutdown demo")
