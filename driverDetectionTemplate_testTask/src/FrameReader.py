import cv2, threading, time
from cmds import CMDS

class FrameReader(threading.Thread):
    """
        Thread class for extracting frames from the
        different videos sources based on OpenCV lib.

        This thread is a slave-thread, which is controlled
        by master thread via `command queue` and rules by
        one video soure. Working steps:
        0. thread is working in loop after running by master
        1. wait for a commnand from the master thread:
        1.1. `shutdown`\kill command
        1.2. `extracting batch` from the video source
        2. Extracted batch will be placed into the storage queue
            for processing batches by other threads
    """
    def __init__(self, vlink, queue, cqueue, logger, bsize=5, name="frReaderThread", args=(), kwargs=None):
        """
            vlink - stream video src, string
            queue - batch db queue
            cqueue - queue of thread commands
            logger - MultiHandler logger
            bsize - number of frame per batch
            *batch: video sample
        """
        threading.Thread.__init__(self, name=name, args=(), kwargs=None)
        self.daemon = True

        self.link = vlink
        self.vcap = cv2.VideoCapture(vlink)
        
        self.bsize = bsize
        self.dbq = queue
        self.cmdq = cqueue
        self.id = threading.get_ident()
        self.lock = threading.Lock()

        self.logger = logger

    def retFrame(self):
        """
            Retreive frame from the video stream by OpenCV
            Res: opencv img or None
        """
        try:
            ret, frame = self.vcap.read()
            self.logger.debug("read frame status:%s, shape=%s"%(ret, frame.shape))
            if ret:
                return frame
            else:
                return None
        except Exception as error:
            self.logger.error(str(error))
        return None
    
    def retBatch(self, bsize):
        """
            Retreive frames from the video stream (batch) and place it into the DB
            DB format: typle(string:src, list:opencv imgs/None)
        """
        try:
            batch = [ self.retFrame() for i in range(0, bsize) ]
            self.dbq.put((self.link, batch))
        except Exception as error:
            self.logger.error(str(error))
    
    def run(self):
        self.logger.info("thread start")
        while True:
            # track master thread command
            cmd = self.cmdq.get()
            self.logger.info("cmd="+str(cmd))
            if cmd == CMDS["kill"]:
                self.logger.info("kill")
                break
            elif cmd == CMDS["get batch"]:
                self.logger.info("get batch")
                self.retBatch(self.bsize)
            time.sleep(0.2)
        
        # kill opencv stream obj
        self.vcap.release()
        self.logger.info("thread shutdown")
