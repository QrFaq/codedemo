import cv2, threading
from cmds import CMDS, CMARKERS

class Gui(threading.Thread):
    """
        Thread class for visualization of processd
        batches based on OpenCV lib.

        Working steps:
        0. thread is working in loop after running by master
        1. if there wasn't any master `shutdown`\kill command
            thread is continue to run
        2. retreive (src link, batch, batch-mark) from the batch-queue
        3. draw marker on each batch frame based on batch-marker
        4. draw processd batch frames one-by one order in OpenCV window
    """
    def __init__(self, queue, cqueue, logger, name="GuiThread", args=(), kwargs=None):
        """
            queue - queue of batches for visualization
            cqueue - queue of thread commands
            logger - MultiHandler logger
            *batch: video sample
        """
        threading.Thread.__init__(self, name=name, args=(), kwargs=None)
        self.daemon = True

        self.dbq = queue
        self.cmdq = cqueue
        self.id = threading.get_ident()
        self.lock = threading.Lock()

        self.wname = "Gui"
        self.logger = logger
        self.dframe = None

    def drawMark(self, frame, mark, color = (255, 0, 0) , thickness = 3):
        """
            Detector Marker visualization

            frame: opencv img, src image
            mark: `CMARKERS` int, detector result
            return: opencv img with added visualization elements
        """
        try:
            spoint = None
            epoint = None

            # select diagonal rectangular points for
            if mark == CMARKERS["crazy on left"]:# left screen half
                spoint = (0, 0)
                epoint = ( int(frame.shape[1]/2)+1, int(frame.shape[0]) )
            elif mark == CMARKERS["crazy on right"]:# right screen half
                spoint = ( int(frame.shape[1]/2), 0 )
                epoint = ( frame.shape[1]-1, frame.shape[0]-1 )
            
            # draw rectangular marker
            if not (spoint is None):
                frame = cv2.rectangle(frame, spoint, epoint, color, thickness)
        except Exception as error:
            self.logger.error(str(error))
        return frame

    def drawMarkedFrame(self, frame, mark):
        """
            Frame & marking visualization by OpenCV in window
            named by `self.name`
            
            frame: opencv img, src image
            mark: `CMARKERS` int, detector result
        """
        try:
            if frame is not None:
                self.dframe = self.drawMark(frame, mark)
                cv2.imshow(self.wname, self.dframe)
        except Exception as error:
            self.logger.error(str(error))

    def drawBatch(self):
        """
            Display batch images in window by OpenCV
        """
        try:
            if not self.dbq.empty():
                vlink, batch, bmark = self.dbq.get()
                self.logger.info("bmark=%s"%(bmark))
                for frame in batch:
                    self.drawMarkedFrame(frame, bmark)
        except Exception as error:
            self.logger.error(str(error))
    
    def run(self):
        self.logger.info("thread start")
        while True:
            try:
                # track master thread command
                if not self.cmdq.empty():
                    cmd = self.cmdq.get()
                    self.logger.info("cmd="+str(cmd))
                    if cmd == CMDS["kill"]:
                        self.logger.info("kill")
                        break
                
                # start visualization with tracking
                # 'q' cmd as `close window` cmd
                if not( cv2.waitKey(1) & 0xFF == ord('q') ):
                    self.logger.debug("display")
                    self.drawBatch()
            except Exception as error:
                self.logger.error(str(error))
        
        cv2.destroyAllWindows()
        self.logger.info("thread shutdown")