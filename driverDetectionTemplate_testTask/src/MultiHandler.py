# src: https://gist.github.com/NelsonMinar/74d94f8bcb78fae150e3
# + super small py2.7 -> py3.x edit
import logging, time, random, os

class MultiHandler(logging.Handler):
    def __init__(self, dirname):
        super(MultiHandler, self).__init__()
        self.files = {}
        self.dirname = dirname
        if not os.access(dirname, os.W_OK):
            raise Exception("Directory %s not writeable" % dirname)

    def flush(self):
        self.acquire()
        try:
            for fp in self.files.values():
                fp.flush()
        finally:
            self.release()

    def _get_or_open(self, key):
        """
            Get the file pointer for the given key, or else open the file
        """
        self.acquire()
        try:
            if self.files.__contains__(key):
                return self.files[key]
            else:
                fp = open(os.path.join(self.dirname, "%s.log" % key), "a")
                self.files[key] = fp
                return fp
        finally:
            self.release()

    def emit(self, record):
        # No lock here; following code for StreamHandler and FileHandler
        try:
            fp = self._get_or_open(record.threadName)
            msg = self.format(record)
            fp.write('%s\n' % msg.encode("utf-8"))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

def create_logger(logger_name="demo", log_fpath="/media/truecrypt2/Project_storage/kaspersky_test/tmp"):
    _L = logging.getLogger(logger_name)

    ### Set up basic stderr logging; this is nothing fancy.
    log_format = '%(relativeCreated)6.1f %(threadName)12s: %(levelname).1s %(module)15.15s:%(lineno)-4d %(message)s'
    stderr_handler = logging.StreamHandler()
    stderr_handler.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(stderr_handler)

    ### Set up a logger that creates one file per thread
    multi_handler = MultiHandler(log_fpath)
    multi_handler.setFormatter(logging.Formatter(log_format))
    logging.getLogger().addHandler(multi_handler)

    ### Set default log level, log a message
    _L.setLevel(logging.INFO)
    _L.info("Run initiated")
    return _L