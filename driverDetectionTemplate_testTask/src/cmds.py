# control thread commands
CMDS = {
    "kill":-1,
    "get batch":1,
    "display all":2
}

# classification markers for crazy driver detector
CMARKERS = {
    "no crazy":0,# crazy driver is not presented on the frame
    "crazy on left":1,# crazy driver on the left screen half
    "crazy on right":2,# crazy driver on the right screen half
    "not recognized":-1,# detector cant process frame
}