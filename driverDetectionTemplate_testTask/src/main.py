"""
    Demo launch script example.
    For more details see `DetectorDemo.py`
"""
from DetectorDemo import DetectorDemo

if __name__ == "__main__":
    launch_settings = {
        # list of processing links
        "vlink_list": [
            "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov"
        ],
        # name of the logger in logs
        "logger_name":"demo",
        # file path to all thread logs
        "log_fpath":"/media/truecrypt2/Project_storage/kaspersky_test/tmp",
        # number of frames per camera video sample
        "batch_size":2
    }

    # launch demo
    demo = DetectorDemo(launch_settings)
    demo.init_threads()
    demo.run_threads()
    demo.run()