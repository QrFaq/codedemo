
'''
    -------------------------------------------------------    \
    --- !WARNING: hashes of vector<int> values                        -------==
    --- !WARNING: DB approximation                          -------==
    -------------------------------------------------------    /
        #       1 init 1GB csv into DB
        #       2 find them in it
        #   Optimisaztion ways:
        #       -hashing
        #       -parallelism
        #       -'zip files'
        #       -???
        #
        #   File format:
        #       -time, hash, ID, 128D vector
    -------------------------------------------------------    \
    --- DBRs' module job:                               -------==
    -------------------------------------------------------    /
        -input: 
            --128D vector
            --...
        Tasks:
            -get input
            -round 128D <float> to 128D <int>
            -get hash from 128D vector of int (not float)
            -search hash at the <hash_N> file:
                --select the same hashes of (128D <int>) list, i.e:
                    fname (hash file), fname (info file), ind (line number) of hash, 128D (float)
                    &&
                --if hash list empty (E)\not empty (NE):
                    ---NE :
                        (phase A: detection for int types!)
                        ----detect P(face is the same):
                            case(A) -P >= 0.6:
                                ----ID: get ID from <info_N> 
                                ----get by ind other data of User
                            case(B) -P < 0.6: 
                                ----ID: "empty, new detected face"
                        (phase B: more accurate detection   )
                    ---E case(Z) (it's new face):
                        ----ID: "empty, new detected face"
                        ----
                case(A)     other data could be return by <ind>
                case(B,Z)   NEW: 1.photo, 2.128D, 3.hash, 4.ID, 4.time
            ---------
            ---  Result: ID,hash,128D,photo,time
            ---------

            -generate command for logger:
                -- 0-!save/1-save face
                -- 0-skip log/1-do log into 1-day file:
            -commandes for gui (?)
            -send data to logger queue
            -send data to gui queue

    -------------------------------------------------------    \
    --- DB files & Row format:                                -------==
    -------------------------------------------------------    /
        -hash_N - file with faces hashes
            ind,hash file  
        -face_N - file with cropped face images in the row
            ind, cropped img
        -info_N - file with info about hashes
            -ind
            -time
            -ID: name, etc.
            -128D vector

'''



import cv2
#import openface
import os, time
import multiprocessing
import numpy as np
import time
import hashlib
import operator

class DBresolver(multiprocessing.Process):
    
    def __init__(self, 
        lqueue, llock, event                 #control & data queue
        ,cfg
        ):
        print("> \t\t\tDBresolver.initUI()\tSTART")
        #control elements
        multiprocessing.Process.__init__(self)
        [self.qdbr, self.qgui, self.qlog] = lqueue
        [self.ldbr, self.lgui, self.llog] = llock
        self.event = event

        self.fnameFace = cfg.fnameFace
        self.fnameInfo = cfg.fnameInfo
        self.fnameCfg = cfg.fnameCfg

        self.dbInfo = self.readDbInfo(self.fnameInfo)   #DB with time, 128D
        self.dbFace = self.readDbFace(self.fnameFace)   #DB with cropped images in row
        [self.dbCfg, self.dbInd] = self.readCfg(self.fnameCfg)
        #self.fconfig = self.readCfg(self.fnameCfg)
        print("> cfg= ", [self.dbCfg, self.dbInd])

        self.simRate = cfg.simRate #faces similirity rate

        #commads
        self.guiCmd = cfg.guiCmd
        self.logCmd = cfg.logCmd

        #code word for unknown faces:
        self.CODEWORD = cfg.CODEWORD#self.dbInfo = self.readDbInfo(self.fnameInfo)   #DB with time, 128D
        self.CODEWORD_IND = self.dbCfg

        #self.dmaxSz = 20
        #self.dequeId = deque(maxlen=self.dmaxSz)

        print("> \t\t\tDBresolver.initUI()\tCOMPLETE")


    def run(self):
        print("> \t\t\tDBresolver.run()\tSTART")
        try:
            while True:
                if self.event.is_set():#keyboard.is_pressed('q'):
                    print('> \t\t\tDBresolver.run() TERMINATE -> START')
                    break
                else:
                    #get element from queue
                    if not self.qdbr.empty():
                        with self.ldbr:
                            #reinit DB#
                            if len(self.dbInfo) is not 0:
                                if self.dbInfo[len(self.dbInfo)-1] == 'RELOADME':
                                    self.dbInfo = self.readDbInfo(self.fnameInfo)   #DB with time, 128D

                            #int, cv::Mat, list of [cropped face on img, dlib points, facePositionInfo]
                            [tmark, inimg, faces_info] = self.qdbr.get()

                            #define arrays for queues
                            lInd = []
                            lID = []
                            lFimg = []
                            lFeatures = []
                            lPosition = []
                            
                            #if faces exist on image
                            if faces_info:
                                for finfo in faces_info:
                                    [fCropImg, fFeatures, fPosition] = finfo
                                    
                                    #Fill data arrays
                                    #print("> fFeatures", fFeatures)
                                    [ID, otherData] = self.getFaceInfo(fFeatures) #extracted fi
                                    print("> ID", ID)
                                    #other data: indm time id vector
                                    #Face == tmark, fCropImg, fPositions others=>ind,time,ID,fFeatures
                                    
                                    #add data to the lists          #   :::Info:::
                                    lID.append(ID)                  #detected IDs
                                    lFimg.append(fCropImg)          #cropped faces
                                    lFeatures.append(fFeatures)     #detected features
                                    lPosition.append(fPosition)     #recognised face postions on frame
                                    lInd.append(self.dbInd)        #row counter in DB files
                                    #count index
                                    self.dbInd += 1

                                    #info resynchronization
                            
                            #send data to other modules
                            with self.lgui:#check faces is npt empty {if not a: === empty}
                                if not self.qgui.full():
                                    self.qgui.put([self.guiCmd, [tmark, inimg, lFimg, lID, lPosition]])
                                else:
                                    self.qgui.get()
                            with self.llog:
                                if not self.qlog.full():
                                    self.qlog.put([self.logCmd, [tmark, inimg, lFimg, lID, lPosition, lInd, lFeatures]])
                                else:
                                    test = self.qlog.get()
                            # Write to queue instructions:
                            #1 - current
                            #2 - detection res or history
                            #log: inimg (1), ID (2), fCropImg (1), fFeatures (1), otherData (2), tmark (1), 
                            #gui: inimg (1), ID (2), fCropImg (1), fPosition (1), tmark (1), 

        except Exception as error:
            print("> Error: DBresolver().run() "+str(error))
        
        #print('> ', self.qlog, self.qlog.qsize())
        #free memory again!
        while not self.qlog.empty():
            self.qlog.get()
        self.qlog.close()
        
        print("> \t\t\tDBresolver().run() TERMINATE -> COMPLETE")
        return


    def getHash(self, string):
        encstr = string.encode()
        hash_object = hashlib.md5(encstr)
        return hash_object.hexdigest()

    def getFaceInfo(self, fFeatures):
        ID = ""
        otherData = None
        codewordname = self.CODEWORD+"_"+str(self.CODEWORD_IND)
        try:
            #print("> \t\t\tStart getFaceInfo")
            ID = codewordname
            roundDigit = 8

            #round 128D <float> to 128D <int>
            iFeatures = [round(f,roundDigit) for f in fFeatures ]
            np_iFeatures = np.asarray(iFeatures)                        #usefull transform
            
            #if matchesHash:#empty matches
            #(Phase A: int check)
            if len(self.dbInfo) != 0:
                print("FLAG")
                matchesMSE = []
                for ind in range(len(self.dbInfo)):
                    dbInfoRow = self.dbInfo[ind]
                    dbFeature = [round(f, roundDigit) for f in dbInfoRow[3] ]        #<<----- index     #rounded dlib vector
                    np_dbFeature = np.asarray(dbFeature)                #usefull transform
                    #print("dbInfoRow", ind, dbInfoRow)
                        
                    mse = 100*((np_iFeatures - np_dbFeature) ** 2).mean(axis=0)
                    matchesMSE.append(mse)#
                print("> matchesMSE:",matchesMSE)

                #select the max MSE
                minIndMse, minMse = min(enumerate(matchesMSE), key=operator.itemgetter(1))
                print("[minIndMse, minMse]",[minIndMse, minMse])
                if minMse < self.simRate:
                    #[ind, strHash] = matchesHash[minIndMse]
                    dbInfoRow = self.dbInfo[minIndMse]
                    #output stuff
                    ID = dbInfoRow[2]                                               #<<----- index
                    otherData = dbInfoRow
                    print("> Detected ID:", ID)
                else:
                    ID = codewordname
                    self.dbInfo.append('RELOADME')
                    self.CODEWORD_IND += 1

            #(Phase B): doesn't support
            if ID is codewordname:
                otherData = None

            #print("> \t\t\tEnd getFaceInfo")
        except Exception as error:
            print("> Error: getFaceInfo().run() "+str(error))
        return [ID, otherData]

    def readDbInfo(self, fname):
        db = []
        with open(fname) as file:
            #read_data = file.read()
            for line in file:
                if line is "\n" or "":
                    continue
                line = line.split(",")
                temp = [
                    int(line[0]),               #ind
                    line[1],                    #time
                    line[2],                    #ID
                    list(map(float, line[3:]))#128D vector
                ]
                db.append(temp)
        file.closed
        return db

    '''def readDbHash(self, fname):
        db = []
        with open(fname) as file:
            #read_data = file.read()
            for line in file:
                if line is "\n" or "":
                    continue
                line = line.split(",")
                temp = [
                    int(line[0]),               #ind
                    line[1]                     #hash of 128D vector
                ]
                db.append(temp)
        file.closed
        return db'''

    def readDbFace(self, fname):
        db = []
        with open(fname) as file:
            #read_data = file.read()
            for line in file:
                if line is "\n" or "":
                    continue
                line = line.split(",")
                temp = [
                    int(line[0]),               #ind
                    line[1]                     #image
                ]
                db.append(temp)
        file.closed
        return db

    def readCfg(self, fname):
        cfg = []
        try:
            with open(fname) as file:
                #read_data = file.read()
                for line in file:
                    if line is "\n" or "":
                        continue
                    line = line.split(",")
                    temp = int(line[1])             #extact value
                    cfg.append(temp)
                    
            file.closed
        except Exception as error:
            print("> Error: readCfg().run() "+str(error))
        return cfg
