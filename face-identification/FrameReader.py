
'''
    Read images from webcams
    etc.
'''

import cv2
#import openface
import os
import multiprocessing
import dlib
import glob
from imutils import face_utils
import numpy as np
import imutils
import time


class FrameReader(multiprocessing.Process):
    
    def __init__(self, 
        queue, lock, event,                 #control & data queue
        cfg
        ):
        print("> \t\t\tFrameReader.initUI()\tSTART")
        #control elements
        multiprocessing.Process.__init__(self)
        self.queue = queue
        self.lock = lock
        self.event = event

        self.inFr = []                  #source image
        self.inGr = []                  #gray source image

        #cfg
        self.minFaceSq = cfg.minFaceSq
        self.rescaleSz = cfg.rescaleSz
        self.detector = cfg.detector
        self.facerec = cfg.facerec
        self.shapepred = cfg.shapepred
        self.camid = cfg.camid
        #self.videoCapture = None
        print("> \t\t\tFrameReader.initUI()\tCOMPLETE")


    def run(self):
        print("> \t\t\tFrameReader.run()\tSTART")
        self.initSrc(self.camid)
        i =0
        while True:
            try:
                if self.event.is_set():#keyboard.is_pressed('q'):
                    print('> \t\t\tFrameReader.run() TERMINATE -> START')
                    break
                else:
                    #pass
                    i+=1
                    self.getFrame()
                    st = time.time()
                    faces_info = self.cropFaces()
                    self.queue.put([ 
                        int(time.time()),   #int value
                        self.inGFr.copy(),  #input img: cv::Mat
                        faces_info          #[ [cropped face on img, dlib points, facePositionInfo], ...]
                    ])
                    et = np.round(time.time() - st,4)
                    #print("> Faces: ", len(faces_info), i, et)
                    #self.showSrc('face', faces_info[0][0])    
                    #self.showSrc('name', self.inFr)


                    #if not self.queue.empty():
                    #    with self.lock:
                    #        #pass
                    #        shfr = self.queue.get()
                    #        #self.showSrc(shfr)
                    #        #cv2.waitKey(1)
                    #        #print '> R: \'%s\',qsz= %s' % (number,queue.qsize())
                    #        #time.sleep(0.1)
                    #else:
                    #    pass

            except Exception as error:
                print("> Error: FrameReader().run() "+str(error))
                #self.killVSrc()
        self.killVSrc()
        print("> \t\t\tFrameReader().run() TERMINATE -> COMPLETE")
        return 

    def initSrc(self, camId):
        try:
            self.videoCapture = cv2.VideoCapture(camId)
        except cv2.error as e:
            print("> Error: "+str(e))

    def getFace(self):
        getFrame()
        return cropFaces()

    def getFrame(self):
        try:
            ret, self.inFr = self.videoCapture.read()
            #print("> shape= ",self.inFr.shape)
            #inFr = cv2.imread("draft/test2.jpg")

            self.inFr = cv2.resize(self.inFr, self.rescaleSz, interpolation = cv2.INTER_CUBIC)
            self.inGFr = self.inFr#cv2.cvtColor(self.inFr, cv2.COLOR_BGR2GRAY)
        except cv2.error as error:
            print("> getFrame() ", error)

    def showSrc(self, name, frame):
        try:
            print("> len= ", len(frame))
            if len(frame)>0:
                # Display the resulting frame
                cv2.imshow(name,frame)
                cv2.waitKey(5)
                #if cv2.waitKey(1) & 0xFF == ord('q'):
                #    break
        except cv2.error as e:
            print("> Error: "+str(e))


    def killVSrc(self):
        try:
            self.videoCapture.release()
            #cv2.destroyAllWindows()
        except cv2.error as e:
            print("> Error: "+str(e))

    def cropFaces(self):
        detFaces = []
        corrConst = 15
        try:
            analFrameArea = self.inFr
            analAreaSq = analFrameArea.shape[0]*analFrameArea.shape[1]
            #print()
            #print("> Face anal start: ", len(analFrameArea))
            frects = self.detector(analFrameArea, 0)#image upsampling = 1 time
            #print("rects: ", frects, len(frects))
            #print("Number of faces detected: {}".format(len(frects)))
            #extract faces from the image
            if len(frects) is not 0:
                for fnumb, frect in enumerate(frects):
                    shape = self.shapepred(analFrameArea, frect)#face landmarks

                    #exract 128D vector shape landmarks
                    fdescr = self.facerec.compute_face_descriptor(analFrameArea, shape)#WARNING: fat shit
                    #print("> len(fdescr): ", fdescr)


                    #dlib2opencv format (x,y,w,h)
                    #cvshape = face_utils.shape_to_np(shape)#opencv styel: (x,y,w,h)
                    #print("> cvshape: ", cvshape)

                    #dlib rect 2 vec
                    (fPosX, fPosY, fw, fh) = face_utils.rect_to_bb(frect)
                    vposeInfo = [fPosX, fPosY, fw, fh]

                    #check face square
                    fsq = fw * fh   #face square
                    #print("> FSQ: ",fsq, np.round(fsq/analAreaSq,4))
                    if np.round(fsq/analAreaSq,4) < self.minFaceSq:
                        print("> Warning: Face square is less then minimum", (fsq/analAreaSq), self.minFaceSq)
                        return None
                    else:
                        #extract image
                        detFaceArray = analFrameArea[
                            fPosY:fPosY+fh+corrConst,
                            fPosX:fPosX+fw+corrConst
                        ]
                        detFaces.append([detFaceArray, fdescr, vposeInfo])
        except Exception as error:
            print("> cropFaces() ", error)

        return detFaces