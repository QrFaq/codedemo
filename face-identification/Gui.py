#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

self.qgui.put(guiCmd, [tmark, inimg, lHash, lID, lFimg, lPosition])
"""
import  sys, os, multiprocessing, keyboard,time
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QGraphicsView, QGraphicsScene
from PyQt5.QtGui import QIcon, QPixmap, QImage
import cv2

#from PyQt5 import QtGui
 
class Gui(multiprocessing.Process):
 
    def __init__(self, queue, lock, event, cfg):
        #super(Gui, self).__init__(self)

        multiprocessing.Process.__init__(self)
        #super().__init__()
        self.title = 'PyQt5 test bitches!'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480

        #initialize thread stuff 
        self.queue = queue
        self.lock = lock
        self.event = event
        #self.initUI()
    
    def initUI(self):
        print("> \t\t\tGui.initUI()\tSTART")
        self.app = QApplication([])  
        
        self.qWidget = QWidget()
        #self.view = QGraphicsView()
        #self.scene = QGraphicsScene(self.view)
        #self.view.setScene(self.scene)


        #init widget window parameters
        self.qWidget.setWindowTitle(self.title)
        self.qWidget.setGeometry(self.left, self.top, self.width, self.height)
        self.label = QLabel(self.qWidget)

        print("> \t\t\tGui.initUI()\tCOMPLETE")

    def imgload(self):
        pixmap = None
        try:
            pixmap = QPixmap('draft/test2.jpg')
            pixmap = pixmap.scaled(self.width, self.height)
        except Exception as error:
            print("> Error: Gui().showImage() "+str(error))
        return pixmap

    def showImage(self, pixmap):
        try:
            if pixmap is not None:
                self.label.setPixmap(pixmap)
                #self.qWidget.resize(pixmap.width(),pixmap.height())
                self.qWidget.raise_()
                self.qWidget.show()
            else:
                print("> Gui().showImage().pixmap == None")
        except Exception as error:
            print("> Error: Gui().showImage() "+str(error))

    def imgCVtoPixmap(self, cvImage, options=[96,96,True]):
        pix = None
        try:
            resizeX, resizeY, inImgIsBGR = options
            cvRGBImg = None
            if inImgIsBGR:
                cvRGBImg = cv2.cvtColor(cvImage, cv2.COLOR_BGR2RGB)
            else:
                cvRGBImg = cvImage
            #print("> Len2(cvRGBImg)= ", len(cvRGBImg))
            image = QImage(
                cvRGBImg, 
                cvRGBImg.shape[1],
                cvRGBImg.shape[0], 
                cvRGBImg.shape[1] * 3,
                QImage.Format_RGB888
            )
            pix = QPixmap(image)
            pix = pix.scaled(resizeX, resizeY)#256
        except Exception as error:
            print("> Error: Gui().imgCVtoPixmap() "+str(error))
        return pix

    def addFaces2img(self, img, lFaceInfo):
        [lID, lPosition] = lFaceInfo
        for ind in range(len(lID)):
            [fPosX, fPosY, fw, fh] = lPosition[ind]
            ID = lID[ind]

            #draw rectangle
            cv2.rectangle(img,(fPosX,fPosY),(fPosX+fw,fPosY+fh),(0,0,255),3)
            
            #Draw ID
            shift = 10
            font = cv2.FONT_HERSHEY_SIMPLEX
            fontsz = int(fw/fh)
            txtthickness = 2
            cv2.putText(img, ID,(fPosX,fPosY-shift), 
                font, fontsz,(255,255,255),txtthickness,cv2.LINE_AA)
        return img


    def run(self):
        self.initUI()
        pixmap = self.imgload()
        print("> \t\t\tGui.run()\tSTART")
        while True:
            try:
                if self.event.is_set():#keyboard.is_pressed('q'):
                    print('> \t\t\tGui.run() TERMINATE -> START')
                    break
                else:
                    #get element from queue
                    if not self.queue.empty():
                        with self.lock:
                            #int, cv::Mat, list of [cropped face on img, dlib points, facePositionInfo]
                            [guiCmd, [tmark, inimg, lFimg, lID, lPosition]] = self.queue.get()
                            
                            #add faces to the image
                            shfr = self.addFaces2img(inimg, [lID, lPosition])

                            #print("> Len(shfr)= ", len(shfr))
                            pixmap = self.imgCVtoPixmap(
                                shfr, 
                                [self.width, self.height, True]
                            )                
                    #else:
                    #    #read local photo
                    #    pixmap = self.imgload()
                    if pixmap is None:
                        continue
                    self.showImage(pixmap)

                self.app.processEvents()
            except Exception as error:
                print("> Error: Gui().run() "+str(error))
        
        self.qWidget.close()
        self.label.close()
        self.app.quit()

        ##clear queue
        #while not self.queue.empty():
        #    self.queue.get()
        print("> \t\t\tGui().run() TERMINATE -> COMPLETE")
        return

'''
def guiworker(q,l,e):
    gui = Gui(q,l,e)
    gui.start()
    while True:
        try:
            if e.is_set():#keyboard.is_pressed('q'):
                print('> Buttod pressed')
                break
            else:
                pass
                #obj = q.get()
                #obj.do_something()
        except Exception as error:
            print("> Error: guiworker() "+str(error))
            break

    #clear queue
    while not q.empty():
        q.get()
    print("> guiworker() -END")
    return

if __name__ == '__main__':
    queue = multiprocessing.Queue()
    lock = multiprocessing.Lock()
    event = multiprocessing.Event()

    p = multiprocessing.Process(target=guiworker, args=(queue,lock,event,))
    p.start()
    while True:
        try:
            #print("main go")
            if keyboard.is_pressed('q'):
                print('> Button pressed')
                event.set()
                break
            else:
                pass
        except Exception as error:
            print("> Error: main() "+str(error))
    
    # Wait for the worker to finish
    print("1")
    queue.close()
    print("2")
    queue.join_thread()
    print("3")
    #p.join()
    print("> main() -END")
'''
