
'''
    Logger input data:
        -logCmd, 
        -List of:
            tmark       :catched frame time
            inimg       :input frame
            lHash       :list of face hashes
            lID         :list of face IDs
            lFimg       :list of cropped face imgs
            lFeatures   :list of 128D feature vectors

    Logger tasks:
        -DB exntension (add new face):
            --file log/Hash:
                (1)                     <-- F:info_N
                -1-ind
                -2-time
                -4-ID: name, etc.
                -5-128D vector
                (2)                     <-- F:hash_N
                -3-hash
            --log face images:          <-- F:face_N
                -time
                -ID
        -Temp day log (without dupllicates):
            --log file:
                -time
                -ID
                -128D vecor OR cropped face img
            --log img file:
                -time, img
            --input image with time ID
        -shell log:
            -tmark,
            -ID
    
    Logger cmd:
        **,***,*
        12,345,6
        DB,TTT,S
    Cmd legend:
        0 - Off
        1 - On
    
    DB:
        1) file log\hash:
            -writing time out
        2) log face images:
            -if exist -> don't write
            -if it exist:
                -> buffer (?) -- frame samples
                -> day temp file (?)
                    -> should compare
                        -> 128D
                        -> 
    T:
        3) temp log file
        4) temp log img file
        5) temp log input image
    S:
        6) shell log


    Optimisations:
        -skip empty\zero\face not found tasks
        -time\buffer check            

'''

import cv2
#import openface
import os, time
import multiprocessing
import numpy as np
import time
from collections import deque

class Logger(multiprocessing.Process):
    def __init__(self, 
        queue, lock, event                 #control & data queue
        ,cfg
        ):
        print("> \t\t\tLogger.initUI()\tSTART")
        #control elements
        multiprocessing.Process.__init__(self)
        self.queue = queue
        self.lock = lock
        self.event = event

        #code word for unknown faces:
        self.CODEWORD = cfg.CODEWORD

        print("> \t\t\tLogger.initUI()\tCOMPLETE")
        self.time = str(int(time.time()))

        self.fnameFace = cfg.fnameFace
        self.fnameInfo = cfg.fnameInfo

        self.dmaxSz = 20
        self.dequeId = deque(maxlen=self.dmaxSz) 

        self.dequeUId = deque(maxlen=5)

        #unknown face session index == UNKNOWN_CODEWORD_IND
        #[self.UNKNOWN_CODEWORD_IND , self.dbSInd] = cfg.fconfig


    #remove duplicates from history by deque
    def checkIdHist(self, lFimg, lID, lPosition, lInd, lFeatures):
        try:
            tlFimg = []
            tlID = []
            tlPosition = []
            tlInd = []
            tlFeatures = []
            for ind in range(len(lID)):
                ID = lID[ind]
                if not( ID in self.dequeId):
                    #add ID to history
                    self.dequeId.append(ID)

                    #store new data with unhist IDs in DB
                    tlFimg.append(lFimg[ind])
                    tlID.append(lID[ind])
                    tlPosition.append(lPosition[ind])
                    tlInd.append(lInd[ind])
                    tlFeatures.append(lFeatures[ind])
                else:
                    #cut presented in history IDs
                    continue
        except Exception as error:
            print("> Error: checkHashHist().run() "+str(error))
        return [tlFimg, tlID, tlPosition, tlInd, tlFeatures]



    def run(self):
        print("> \t\t\tLogger.run()\tSTART")
        try:
            while True:
                if self.event.is_set():#keyboard.is_pressed('q'):
                    print('> \t\t\tLogger.run() TERMINATE -> START')
                    break
                else:
                    #get element from queue
                    if not self.queue.empty():
                        with self.lock:
                            [cmd, [tmark, inimg, lFimg, lID, lPosition, lInd, lFeatures]] = self.queue.get()
                            [cmdDB, cmdT, cmdS] = cmd.split(',')
                            print("> Logger:",lID)
                            
                            if int(cmdDB[0]) or int(cmdDB[1]):
                                [lFimgDB, lIDDB, lPositionDB, lIndDB,lFeaturesDB] = self.checkIdHist(lFimg, lID, lPosition, lInd,lFeatures)

                                #1) DB file log hash
                                if int(cmdDB[0]):
                                    try:
                                        strListInfo = self.genDBdata(lIndDB, tmark, lIDDB, lFeaturesDB)
                                        #print(strListHash, strListInfo)
                                        #self.fwriteList(self.fnameHash, strListHash)
                                        self.fwriteList(self.fnameInfo, strListInfo)
                                    except Exception as error:
                                        print("cmdDB[0]:",error)
                                
                                #2) DB log face images:
                                if int(cmdDB[1]):
                                    try:
                                        strList = self.genImgTempLog(tmark, lFimgDB)
                                        self.fwriteList(self.fnameFace, strList)
                                    except Exception as error:
                                        print("cmdDB[1]:",error)

                            #3) T temp log file 
                            if int(cmdT[0]):
                                try:
                                    strList = self.genTempLog(tmark, lID, lFeatures)
                                    self.fwriteList("temp/temp_faceimg_"+self.time+".log", strList)
                                except Exception as error:
                                    print("cmdT[0]:",error)

                            #4) T temp log face img file
                            if int(cmdT[1]):
                                try:
                                    strList = self.genImgTempLog(tmark, lFimg)
                                    self.fwriteList("temp/temp_faceimg_"+self.time+".log", strList)
                                except Exception as error:
                                    print("cmdT[1]:",error)
                            
                            #5) T temp log input image
                            if int(cmdT[2]):
                                try:
                                    strList = self.genImgTempLog(tmark, [inimg])
                                    self.fwriteList("temp/temp_inimg_"+self.time+".log", strList)
                                except Exception as error:
                                    print("cmdT[2]:",error)

                            #5) S shell log
                            if int(cmdS[0]):
                                try:
                                    for ID in lID:
                                        print("> log: "+str(tmark)+"\t"+ID)
                                except Exception as error:
                                    print("cmdS[0]:",error)
        except Exception as error:
            print("> Error: Logger().run() "+str(error))
        print("> \t\t\tLogger().run() TERMINATE -> COMPLETE")
        return 

    def genImgTempLog(self, tmark, lFimg):
        strList = []
        try:
            if not lFimg:
                print("> genImgTempLog: error empty list of cropped faces")
                return []
            for img in lFimg:
                rimg = np.reshape(img,lFimg.shape[0]*lFimg.shape[1],1)
                string = tmark+','+",".join(rimg)
                strList.append(string)
        except Exception as error:
            print("> genImgTempLog:",error)
        return strList

    def dlibVectToStrList(self, dlibv):
        l = []
        for val in dlibv:
            l.append(str(val))
        return l

    def genTempLog(self, tmark, lID, lFeatures):
        strList = []
        try:
            sz = len(lID)
            isSameSize = ( sz == len(lFeatures) )
            if not isSameSize:
                print("> genTempLog: error input lists have different size")
                return []
            for ind in range(sz):
                tempFeature = self.dlibVectToStrList(lFeatures[ind])
                string = ",".join([str(tmark), lID[ind]] + tempFeature)
                strList.append(string)
        except Exception as error:
            print("> genTempLog:",error)
        return strList

    def genDBdata(self, lInd, tmark, lID, lFeatures):
        print(tmark, len(lInd), len(lID), len(lFeatures) )
        strListInfo = []
        try:
            sz = len(lInd)
            isSameSize =    \
                sz == len(lID) and \
                sz == len(lFeatures)
            if not isSameSize:
                print("> genDBdata: error input lists have different size")
                return []
            for ind in range(sz):
                tempID = lID[ind].split("_")
                if tempID[0] != self.CODEWORD:#skip saving existing faces
                    continue
                tempFeature = self.dlibVectToStrList(lFeatures[ind])
                temp = [str(lInd[ind]), str(tmark), lID[ind]] + tempFeature
                string = ",".join(temp)
                #print("string= ", string)
                strListInfo.append(string)
        except Exception as error:
            print("> genDBdata:",error)
        return strListInfo

    def fwriteList(self, fname, stringlist):
        with open(fname,'a') as f:
            for string in stringlist:
                f.write(string+'\n')

    def fwrite(self, fname, string):
        with open(fname,'a') as f:
            f.write(string+'\n')
