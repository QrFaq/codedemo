
import os
from os.path import isfile, join
from os import listdir

import sys, os, dlib, glob

class SoftCfg:
    def __init__(self):
        predictor_path = "test_scripts/shape_predictor_68_face_landmarks.dat"#"test_scripts/shape_predictor_5_face_landmarks.dat"#
        face_rec_model_path = "test_scripts/dlib_face_recognition_resnet_model_v1.dat"
                
        self.detector = dlib.get_frontal_face_detector()
        self.shapepred = dlib.shape_predictor(predictor_path)
        self.facerec = dlib.face_recognition_model_v1(face_rec_model_path)

        self.rescaleSz = (640, 320)#(1024,768)#(1920,1080)##   #rescale image frame size
        self.minFaceSq = 0.01      #minimal face square at the frame
        
        #unknown face ID
        self.CODEWORD = "unknown"

        self.fnameFace = "./DB/face_0"
        self.fnameInfo = "./DB/info_0"
        self.fnameCfg = "./main.cfg"
        self.camid = 1#"./test_scripts/test.mp4"#0#1#
        self.simRate = 0.2

        #
        self.guiCmd = None
        self.logCmd = "10,100,1"