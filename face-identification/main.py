 #!/bin/bash

'''
1 read webcam frame
2 crop face and get time
3 show image & cropped face
4 write 

Vocabulary:
    -dbr    = data base resolver
    -log    = logger
    -gui    = gui ;)
    -q*     = * queue
    -eK*    = * event Killer
    -l*     = * locker

Queues dependencies (pipe ends):
    reader  <-> dbr
    dbr     <-> log
    dbr     <-> gui  

Sending data in queues:
    qdbr -> list of [cropped face on img, dlib points, facePositionInfo]
    qgui -> ID, time, frame, list of [cropped face in img]
    qlog -> ID, time, list of [dlib points]

'''
import multiprocessing
import keyboard, time, os

#import classes from files
from FrameReader import FrameReader
from SoftCfg import SoftCfg
from Gui import Gui
from DBresolver import DBresolver
from Logger import Logger

def worker(e):#manager
    try:
        print('> \tworker() INITIALIZATION -> START')
        
        #list of dependent process names:
        namesProc = {#           <_----------------------------------------------------------------
            'dbr': 0,
            'gui': 1,
            'log': 2,
            'reader': 3 #must be the last!
            }

        #create queues, events, lockers
        qsz = 5     #maximum queue size
        eK = []
        q = []
        l = []
        for name in list(namesProc.keys()):
            eK.append(multiprocessing.Event())
            if name is 'reader':#skip excess queue
                continue
            else:
                q.append(multiprocessing.Queue(qsz))
                l.append(multiprocessing.Lock())

        ##########################################################################################
        ## define & run processes
        ##########################################################################################
        cfg = SoftCfg()
        n = len(namesProc)-1

        listProc = [#           <_-----------------------------------------------------------------
             DBresolver( q[0:n], l[0:n], eK[namesProc['dbr']], cfg)
            ,Gui(q[namesProc['gui']], l[namesProc['gui']], eK[namesProc['gui']], cfg)
            ,Logger(q[namesProc['log']], l[namesProc['log']], eK[namesProc['log']], cfg)
            ,FrameReader( q[namesProc['dbr']], l[namesProc['dbr']], eK[namesProc['reader']], cfg)
        ] 

        nameRunp = [ 'reader','gui', 'dbr', 'log' ] #<_----------------------------------------------------------
        for pname in nameRunp:
            listProc[namesProc[pname]].start()


        print('> \tworker() INITIALIZATION -> COMPLETE')
        print('> \tworker() INTERRUPTION MONITOR -> START')
        while True:
            try:
                if e.is_set():#keyboard.is_pressed('q'):
                    print('>\t Worker TERMINATION -> START')
                    break
                else:
                    pass
                    #obj = q.get()
                    #obj.do_something()
            except Exception as error:
                print("> Error: worker() "+str(error))
                break

        ##########################################################################################
        ## terminate dependent processes:                                                       ##
        ##########################################################################################
        for pname in  nameRunp:
            print("==pname ", pname)
            process = listProc [namesProc[pname]]
            eventKiller = eK   [namesProc[pname]]
            while process.exitcode is None:
                eventKiller.set()
                process.join(timeout=3)

        ##########################################################################################
        ## clear queue:
        ##########################################################################################
        print('> \t\tworker() FREEMEMORY START')
        #print(q)
        for qobj in q:
        #    print("> \t\t",qobj, qobj.qsize())
            while not qobj.empty():
                qobj.get()
            qobj.close()
        print('> \t\tworker() FREEMEMORY -> COMPLETE')
        

        #frameReader.join()
        #gui.join()
        print('>\t Worker TERMINATION -> DONE')
    except Exception as error:
        print("> Error:",error)
    return

if __name__ == "__main__":
    print('> main() INITIALIZATION -> START')
    eKworker = multiprocessing.Event()

    p = multiprocessing.Process(target=worker, args=(eKworker,))
    p.start()
    
    print('> main() INITIALIZATION -> COMPLETE')
    print('> main() INTERRUPTION MONITOR -> START')
    while True:
        try:
            #print("main go")
            if keyboard.is_pressed('q'):
                print('> main() SHUTDOWN -> START')
                break
            else:
                pass
        except Exception as error:
            print("> Error: main() "+str(error))
    
    while p.exitcode is None:
        print('> Terminate Worker -> START')
        eKworker.set()
        p.join()
        print('> Terminate Worker -> DONE')

    print('> main() SHUTDOWN -> COMPLETE')