import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import consumer.ConsumerInfo;
import consumer.ImageSaverConsumer;
import consumer.VideoSaverConsumer;
import logmanagers.SaveImageInfo;
import logmanagers.SaveVideoInfo;
import mysql.MysqlInfo;
import producer.IpcamOpenCvProducer;
import producer.KafkaVideoProducer;
import producer.ProducerInfo;
import templates.AdditionalParamsInfo;

public class KafkaThreadBuilder {
    /* Class Logger-messages */
    protected static String ERROR_NON_DOCUMENTED_ARGUMENT = "Non documented/unused-additional arguments found, pls, check `--help`";
    protected static String ERROR_RAIMAINING_ARGS = "remainig argument:";
    protected static String ERROR_UNUSED_ARGS = "Unused additional arguments detected";
    protected static String ERROR_WRONG_SELECTED_THREAD = "Wrong selected thread type -c or --class argument: ";
    protected static String ERROR_NO_VIDEO_SELECTED_FOR_PRODUCER = "-v or --video parameter is not set. Pls, set full video path";
    protected static String ERROR_SELECT_ONE_INPUT_TYPE = "please select only -v(--video) or -i(--ipcam) option";
    /* end */

    private static final Logger logger = LogManager.getLogger(KafkaThreadBuilder.class);
    private static CommandLine commandLine;
    private static Options options;
    private static HelpFormatter formatter;
    private static String header;
    private static String footer;

    /* Argument parser with constructor */
    private static void argsParser(String[] args)
    {
        Option option_H = Option.builder("h").longOpt("help").desc("Show application command manual").build();

        // main arguments
        Option option_C = Option.builder("c").longOpt("class").argName("opt3").hasArg().desc("Thread class name to start, choose one from the list [producer, consumer]").build();
        Option option_V = Option.builder("v").longOpt("video").hasArg().argName("opt1").desc("full video path for processing").build();
        Option option_I = Option.builder("i").longOpt("ipcam").hasArg().argName("opt2").desc("Camera IP, rtsp url").build();

        // property arguments
        Option option_PP = Option.builder().longOpt("producer_props").hasArg().argName("opt4").desc("Full path to `producer.properties` file").build();
        Option option_CP = Option.builder().longOpt("consumer_props").hasArg().argName("opt5").desc("Full path to `consumer.properties` file").build();
        Option option_MP = Option.builder().longOpt("mysql_props").hasArg().argName("opt6").desc("Full path to `mysql.properties` file").build();
        Option option_SIP = Option.builder().longOpt("save_img_props").hasArg().argName("opt7").desc("Full path to `save_img.properties` file").build();
        Option option_SVP = Option.builder().longOpt("save_video_props").hasArg().argName("opt8").desc("Full path to `save_video.properties` file").build();
        Option option_AP = Option.builder().longOpt("additional_props").hasArg().argName("opt9").desc("Full path to `additional.properties` file").build();

        // xml launcher argument
        Option option_XML = Option.builder("x").longOpt("xml").hasArg().argName("opt10").desc("Path to xml file for generating threads from eat").build();

        // add arguments inside option
        //Option option_r = Option.builder("r").argName("opt1").hasArg().desc("The r option").build();
        //Option option_test = Option.builder().longOpt("test").desc("The test option").required(true).build();
        options = new Options();
        options.addOption(option_C);
        options.addOption(option_H);
        options.addOption(option_V);
        options.addOption(option_I);

        options.addOption(option_PP);
        options.addOption(option_CP);
        options.addOption(option_MP);
        options.addOption(option_SIP);
        options.addOption(option_SVP);
        options.addOption(option_AP);

        options.addOption(option_XML);

        header = "               [<arg1> [<arg2> [<arg3> ...\n       Options, flags and arguments may be in any order";
        footer = "Command examples:" +
                "\n<application path> -c producer" +
                "\n<application path> -c consumer";
        formatter = new HelpFormatter();

        // create comand line parser & check user-input commands
        CommandLineParser parser = new DefaultParser();
        try
        {
            commandLine = parser.parse(options, args);

            // check input arguments before the start
            logger.info("Checking input arguments...");
            String[] remainder = commandLine.getArgs();
            if (remainder.length > 0)
            {
                logger.error(ERROR_NON_DOCUMENTED_ARGUMENT);
                for (String argument : remainder)
                    logger.error(ERROR_RAIMAINING_ARGS, argument);
                formatter.printHelp("CLIsample", header, options, footer, true);
                throw new Exception(ERROR_UNUSED_ARGS);
            }
        }
        catch (ParseException e)
        {
            commandLine = null;
            logger.error("{}, {}", e.getMessage(), e.getMessage());
        } catch (Exception e) {
            commandLine = null;
            logger.error("{}, {}", e.getMessage(), e.getMessage());
            System.exit(-1);
        }
    }

    /* Thread launchers based on command line arguments:
    * 1. check input arguments exist (all shall be setted)
    * 2. read arguments
    * 3. create thread based on class
    * */
    private static void create_thread_producer_video(CommandLine commandLine) throws Exception {
        if(!commandLine.hasOption("v") && !commandLine.hasOption("producer_props") && !commandLine.hasOption("consumer_props") && !commandLine.hasOption("additional_props"))
            throw new Exception(ERROR_SELECT_ONE_INPUT_TYPE);

        String videoFpath = commandLine.getOptionValue("v");
        String producerProp = commandLine.getOptionValue("producer_props");
        String comsumerProp = commandLine.getOptionValue("consumer_props");
        String additionalProp = commandLine.getOptionValue("additional_props");
        KafkaVideoProducer producer = new KafkaVideoProducer( videoFpath, new ProducerInfo(producerProp),
                new ConsumerInfo(comsumerProp), new AdditionalParamsInfo(additionalProp) );
    }
    private static void create_thread_producer_ipcam(CommandLine commandLine) throws Exception {
        if(!commandLine.hasOption("i") && !commandLine.hasOption("producer_props") && !commandLine.hasOption("consumer_props") && !commandLine.hasOption("additional_props"))
            throw new Exception(ERROR_SELECT_ONE_INPUT_TYPE);

        String camera_ip = commandLine.getOptionValue("i");
        String producerProp = commandLine.getOptionValue("producer_props");
        String comsumerProp = commandLine.getOptionValue("consumer_props");
        String additionalProp = commandLine.getOptionValue("additional_props");
        IpcamOpenCvProducer producer = new IpcamOpenCvProducer( camera_ip, new ProducerInfo(producerProp),
                new ConsumerInfo(comsumerProp), new AdditionalParamsInfo(additionalProp) );
    }
    private static void create_thread_consumer_img_saver(CommandLine commandLine) throws Exception {
        if(!commandLine.hasOption("consumer_props") && !commandLine.hasOption("mysql_props") && !commandLine.hasOption("save_img_props") && !commandLine.hasOption("additional_props"))
            throw new Exception(ERROR_SELECT_ONE_INPUT_TYPE);

        String comsumerProp = commandLine.getOptionValue("consumer_props");
        String mysqlProp = commandLine.getOptionValue("mysql_props");
        String saveImgProp = commandLine.getOptionValue("save_img_props");
        String additionalProp = commandLine.getOptionValue("additional_props");
        ImageSaverConsumer imageSaverConsumer = new ImageSaverConsumer( new ConsumerInfo(comsumerProp),
                new MysqlInfo(mysqlProp), new SaveImageInfo(saveImgProp), new AdditionalParamsInfo(additionalProp) );
    }
    private static void create_thread_consumer_video_saver(CommandLine commandLine) throws Exception {
        if(!commandLine.hasOption("consumer_props") && !commandLine.hasOption("mysql_props") && !commandLine.hasOption("save_video_props") && !commandLine.hasOption("additional_props"))
            throw new Exception(ERROR_SELECT_ONE_INPUT_TYPE);

        String comsumerProp = commandLine.getOptionValue("consumer_props");
        String mysqlProp = commandLine.getOptionValue("mysql_props");
        String saveVideoProp = commandLine.getOptionValue("save_video_props");
        String additionalProp = commandLine.getOptionValue("additional_props");
        VideoSaverConsumer videoSaverConsumer = new VideoSaverConsumer( new ConsumerInfo(comsumerProp),
                new MysqlInfo(mysqlProp), new SaveVideoInfo(saveVideoProp), new AdditionalParamsInfo(additionalProp) );
    }

    /*
    * Console launch examples:
    * --help
    * --class producer -v vtest.mp4 --producer_props ./properties/video/producer.properties --consumer_props ./properties/video/consumer.properties --additional_props ./properties/video/additional.properties
    * --class producer -i "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov" --producer_props ./properties/ipcam/producer.properties  --consumer_props ./properties/ipcam/consumer.properties --additional_props ./properties/ipcam/additional.properties
    * --class consumer_image_saver --consumer_props ./properties/img_saver/consumer.properties --mysql_props ./properties/img_saver/mysql.properties --save_img_props ./properties/img_saver/save_img.properties --additional_props ./properties/img_saver/additional.properties
    * --class consumer_video_saver --consumer_props ./properties/video_saver/consumer.properties --mysql_props ./properties/video_saver/mysql.properties --save_video_props ./properties/video_saver/save_video.properties --additional_props ./properties/video_saver/additional.properties
    * --xml ./3_sanofi/launch_examples/launcher.xml
    *  */
    public static void launcher(String[] args)
    {
        try {
            argsParser(args);
            if (commandLine.hasOption("c") || commandLine.hasOption("class"))
            {// select launched thread classes
                String threadType = commandLine.hasOption("c") ? commandLine.getOptionValue("c")  : commandLine.getOptionValue("class");
                switch(threadType)
                {
                    case "producer":
                        logger.info("--class, -c = producer");
                        if(commandLine.hasOption("v"))
                            //"/media/truecrypt2/Project_storage/kaffka_with_videos/maven_project/vtest.mp4"
                            create_thread_producer_video(commandLine);
                        else if (commandLine.hasOption("i"))
                            //rtsp://freja.hiof.no:1935/rtplive/_definst_/hessdalen02.stream
                            //rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov
                            //http://202.245.13.81:80/cgi-bin/camera?resolution=640
                            create_thread_producer_ipcam(commandLine);
                        break;
                    case "consumer_image_saver":
                        create_thread_consumer_img_saver(commandLine);
                        break;
                    case "consumer_video_saver":
                        create_thread_consumer_video_saver(commandLine);
                        break;
                    default:
                        throw new Exception(ERROR_WRONG_SELECTED_THREAD+threadType);
                }
            } else if(commandLine.hasOption("x")) {
                LauncherXML launcherXML = new LauncherXML(commandLine.getOptionValue("x"));
                launcherXML.launch();
            } else if(commandLine.hasOption("h") || commandLine.hasOption("help")) {
                formatter.printHelp("CLIsample", header, options, footer, true);
            }

        } catch (Exception e) {
            logger.error("{}, {}", e.getMessage(), e.getMessage());
            System.exit(-1);
        }

    }

    public static void main(String[] args)
    {
        /*
         *  -c consumer_image_saver
         *  -c producer -i "http://202.245.13.81:80/cgi-bin/camera?resolution=640"
         * */

        launcher(args);
    }


}
