import consumer.ConsumerInfo;
import consumer.ImageSaverConsumer;
import consumer.VideoSaverConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import logmanagers.SaveImageInfo;
import logmanagers.SaveVideoInfo;
import mysql.MysqlInfo;
import producer.IpcamOpenCvProducer;
import producer.KafkaVideoProducer;
import producer.ProducerInfo;
import templates.AdditionalParamsInfo;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class LauncherXML {
    /* Class Logger-messages */
    protected String ERROR_NO_PROPERTY = "no property found";
    protected String ERROR_NO_THREAD_TO_LAUNCH = "no selected thread to launch";
    /* end */

    private static final Logger logger = LogManager.getLogger(LauncherXML.class);
    Document doc;

    /* read XML file and parse info into doc file for extracting blocks */
    public LauncherXML(String fpath) throws ParserConfigurationException, IOException, SAXException {
        File fXmlFile = new File(fpath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        this.doc = dBuilder.parse(fXmlFile);

        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        this.doc.getDocumentElement().normalize();

    }

    /* Extract property from XML-block element based on specified name
    * [Warning] properties must be set */
    private String getProperty(Element eElement, String name) throws Exception {
        String content = "";
        NodeList restElmLst = eElement.getElementsByTagName(name);
        if (restElmLst != null && restElmLst.getLength() > 0)
            content = restElmLst.item(0).getTextContent();
        else
            throw new Exception(ERROR_NO_PROPERTY+":"+name);
        return content;
    }

    /* launchers based on XML tag extraction under the type `name`: extract tags (fpath, etc), launch thread*/
    private void start_video_producer(Element eElement) throws Exception {
        String stream_source = getProperty(eElement, "stream_source");
        String proudecer_properties = getProperty(eElement, "proudecer_properties");
        String consumer_properties = getProperty(eElement, "consumer_properties");
        String additional_properties = getProperty(eElement, "additional_properties");
        KafkaVideoProducer producer = new KafkaVideoProducer(
                stream_source,
                new ProducerInfo(proudecer_properties),
                new ConsumerInfo(consumer_properties),
                new AdditionalParamsInfo(additional_properties)
        );
    }
    private void start_ipcam_producer(Element eElement) throws Exception {
        String stream_source = getProperty(eElement, "stream_source");
        String proudecer_properties = getProperty(eElement, "proudecer_properties");
        String consumer_properties = getProperty(eElement, "consumer_properties");
        String additional_properties = getProperty(eElement, "additional_properties");
        IpcamOpenCvProducer producer = new IpcamOpenCvProducer(
                stream_source,
                new ProducerInfo(proudecer_properties),
                new ConsumerInfo(consumer_properties),
                new AdditionalParamsInfo(additional_properties)
        );
    }
    private void start_img_saver_consumer(Element eElement) throws Exception {
        String consumer_properties = getProperty(eElement, "consumer_properties");
        String mysql_properties = getProperty(eElement, "mysql_properties");
        String save_image_properties = getProperty(eElement, "save_image_properties");
        String additional_properties = getProperty(eElement, "additional_properties");
        ImageSaverConsumer imageSaverConsumer = new ImageSaverConsumer(
                new ConsumerInfo(consumer_properties),
                new MysqlInfo(mysql_properties),
                new SaveImageInfo(save_image_properties),
                new AdditionalParamsInfo(additional_properties) );
    }
    private void start_video_saver_consumer(Element eElement) throws Exception {
        String consumer_properties = getProperty(eElement, "consumer_properties");
        String mysql_properties = getProperty(eElement, "mysql_properties");
        String save_video_properties = getProperty(eElement, "save_video_properties");
        String additional_properties = getProperty(eElement, "additional_properties");
        VideoSaverConsumer videoSaverConsumer = new VideoSaverConsumer(
                new ConsumerInfo(consumer_properties),
                new MysqlInfo(mysql_properties),
                new SaveVideoInfo(save_video_properties),
                new AdditionalParamsInfo(additional_properties)
        );
    }

    /*
    * Parse XML file settings for and launch selected thread
    * */
    public void launch() throws Exception {
            logger.info("Root element :" + this.doc.getDocumentElement().getNodeName());
            logger.info("----------------------------");

            // select main XML-block
            NodeList nList = this.doc.getElementsByTagName("type");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                // select element of block
                Node nNode = nList.item(temp);
                logger.info("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    // run thread selection based on thrad name
                    Element eElement = (Element) nNode;
                    String name = eElement.getAttribute("name");
                    switch (name)
                    {
                        case "video":
                            start_video_producer(eElement);
                            break;
                        case "ipcam":
                            start_ipcam_producer(eElement);
                            break;
                        case "image_saver":
                            start_img_saver_consumer(eElement);
                            break;
                        case "video_saver":
                            start_video_saver_consumer(eElement);
                            break;
                        default:
                            throw new Exception(ERROR_NO_THREAD_TO_LAUNCH);
                    }
                }
            }// end for
    }
}
