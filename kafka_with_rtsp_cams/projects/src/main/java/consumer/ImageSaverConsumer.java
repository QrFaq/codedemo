package consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.logging.log4j.LogManager;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import mysql.MysqlDb;
import mysql.MysqlInfo;
import templates.AdditionalParamsInfo;
import templates.KafkaTemplateThread;
import logmanagers.PolicyManager;
import logmanagers.SaveImageInfo;
import transfer.FramePkg;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

public class ImageSaverConsumer extends KafkaTemplateThread {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
    private Imgcodecs imageCodecs;

    private AdditionalParamsInfo additionalParamsInfo;
    private ConsumerInfo consumerInfo;
    private MysqlInfo mysqlCfg;
    private MysqlDb mysqlDb = new MysqlDb();

    private HashMap<String, String> hmapSpathFpath;//key: stream-cam-path, value: folder-path

    private PolicyManager pman = new PolicyManager();
    //private long firstRecordTimeInFolder;
    //private long lastRecordCheckTimeByTime;

    /* Initialization of kafka consumer properties
     *   Input:
     *       ConsumerInfo info: user settings for consumer
     * */
    private void setConsumerProps(ConsumerInfo info) throws IOException {
        initConsumerProperties(info);
        this.propsConsumer.put("key.deserializer", StringDeserializer.class.getName());//, "org.apache.kafka.common.serialization.StringDeserializer");
        this.propsConsumer.put("value.deserializer", transfer.FramePkgDeserializer.class.getName());//"video.kafka.transfer.FramePkgDeserializer");
    }

    /* Private members setter and initializer and launch */
    public ImageSaverConsumer(ConsumerInfo consumerInfo, MysqlInfo mysqlInfo, SaveImageInfo saveImageInfo, AdditionalParamsInfo additionalParamsInfo)
    {
        try {
            this.consumerInfo = consumerInfo;
            this.additionalParamsInfo = additionalParamsInfo;

            logger = LogManager.getLogger(ImageSaverConsumer.class);

            // create consumer connected to topic (fetching images)
            setConsumerProps(this.consumerInfo);
            this.consumer = new KafkaConsumer<String, FramePkg>(this.propsConsumer);
            this.consumer.subscribe(Collections.singletonList(this.consumerInfo.topicName));

            // save images
            this.imageCodecs = new Imgcodecs();

            // init mysql connection
            this.mysqlCfg = mysqlInfo;
            this.mysqlDb.connect2db(mysqlInfo);

            // retreive information about ip-cameras from MySql
            this.hmapSpathFpath = this.mysqlDb.getListOfIpcamsFromTableByStreamKeyFolderpathValue();//key: cameraId==stream-link
            if(this.hmapSpathFpath.size() < 1)
                throw new Exception(ERROR_NO_CAMERAS_RETREIVED_FROM_IP_CAM_TABLE+", hmapSpathFpath()");

            // set policy variables
            this.pman.policyCheckTimeIntervalSeconds = saveImageInfo.policyCheckTimeIntervalSeconds;//2;
            this.pman.maxStoreTimeSeconds = saveImageInfo.maxStoreTimeSeconds;//5*60*60*14;
            this.pman.maxFolderSizeBytes = saveImageInfo.maxFolderSizeBytes;//707*5;
            this.pman.rmPercantage = 0.0 < saveImageInfo.rmPercentage && saveImageInfo.rmPercentage < 100.0 ? saveImageInfo.rmPercentage: this.pman.rmPercentageMin;//50

            // retreive information about last records in storage folders of such cameras
            // and update TimerPolicy Date
            pman.hmFpathRecordOldestTime = new HashMap<String, Long>();
            this.pman.hmFoldpath2LastTimePolicyDate = new HashMap<String, Long>();
            if(pman.updateIpcamFirstRecordsTime(this.hmapSpathFpath) && pman.hmFpathRecordOldestTime.size() < 1)
                throw new Exception(ERROR_NO_CAMERAS_RETREIVED_FROM_IP_CAM_TABLE+", hmapFpathRecordOldestTime()");

            logger.info(MSG_START_PROD + this);
        } catch (Exception e){
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
            System.exit(-1);
        } finally {
            start();
        }
    }

    /* save image on hdd by OpenCV */
    private boolean save2hdd(ConsumerRecords<String, FramePkg> messages)
    {
        boolean status = false;
        try {
            for (ConsumerRecord<String, FramePkg> message : messages)
            {
                FramePkg fpkg = message.value();
                String folderPath = hmapSpathFpath.get(fpkg.get_srcId()) + "/";
                folderPath = folderPath.replace("//", "/");
                String savePath = folderPath+fpkg.get_timestamp() + ".jpg";
                Mat simg = additionalParamsInfo.resizeMat(fpkg.byte2Mat());
                imageCodecs.imwrite( savePath,  simg);
            }
            status = true;
        } catch (Exception e) {
            logger.error(ERROR_CANT_PROC_PKG+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    public void run()
    {
        logger.info(MSG_START_CONS_RUN + this);

        int loopn = 0;
        int empty_record_poll = 0;
        try {
            this.consumer.poll(Duration.ofMillis(1000));
            if (this.consumerInfo.checkTopicZeroPos)
                setOffset2zero();

            Thread.sleep(this.consumerInfo.startDelayMs);
            while (true)
            {
                try {
                    // read topic img-messges
                    final ConsumerRecords<String, FramePkg> messages = this.consumer.poll(Duration.ofMillis(1000));

                    if (!messages.isEmpty())
                    {// If some data was read: process and save results for each non empty message
                        if (save2hdd(messages))
                        {// reset counter & commit data & check update storage Time Timer and Size Timer

                            // check update time policy
                            boolean isPolicyUpdateDone = pman.updateStorageByPolicies(this.hmapSpathFpath);

                            empty_record_poll = 0;
                            this.consumer.commitSync();
                        } else {// if cant process full received pkg: no sync & try again after sleep
                            logger.error(ERROR_CANT_PROC_FULL_NONEMPTY_PKG);
                            Thread.sleep(this.consumerInfo.sleeptMs);
                        }
                    } else {// If no data was read:
                        // if no data in topic more than: (emptyRecordPollLimit * sleeptMs) [ms]
                        if (empty_record_poll > this.consumerInfo.emptyRecordPollLimit)
                        {
                            logger.info(String.format(ERROR_CONS_TERMINATE_NO_DATA_LONGER_S,
                                    this.consumerInfo.sleeptMs / 1000 * this.consumerInfo.emptyRecordPollLimit, this.consumerInfo.sleeptMs, this.consumerInfo.emptyRecordPollLimit));
                            break;
                        } else {// count retries
                            empty_record_poll++;
                            logger.warn(String.format(WARN_EMPTY_RECORD_POLL_COUNTER, empty_record_poll, this.consumerInfo.emptyRecordPollLimit) );
                            Thread.sleep(this.consumerInfo.sleeptMs);
                        }
                    }

                } catch (Exception e) {
                    // if process cant process full received pkg - it doesn't sync with others
                    logger.error(ERROR_CANT_PROC_PKG+", {}, {}", e.getMessage(), e.getMessage());
                    Thread.sleep(this.consumerInfo.sleeptMs);
                }

                //if (loopn > 30) break; else loopn++;
            }
        } catch (Exception e) {
            logger.error("{}, {}", e.getMessage(), e.getMessage());
        } finally {
            this.consumer.close();
        }
        logger.info( MSG_END_CONS_RUN+ this);
    }

}
