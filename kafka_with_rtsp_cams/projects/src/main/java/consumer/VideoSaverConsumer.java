package consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.logging.log4j.LogManager;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoWriter;
import logmanagers.PolicyManager;
import logmanagers.SaveVideoInfo;
import mysql.MysqlDb;
import mysql.MysqlInfo;
import templates.AdditionalParamsInfo;
import templates.KafkaTemplateThread;
import transfer.FramePkg;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/** WARNING: the linux code driver for video writing is: x264, but on Windows MJPG used **/

public class VideoSaverConsumer extends KafkaTemplateThread {
    private final String ERROR_WHILE_SAVING_VIDEOS = "Can't save one of the videos";
    private final String ERROR_TO_CREATE_OPENCV_VIDEO_WRITERS = "Can't create opencv video writers";
    private final String ERROR_TO_CREATE_OPENCV_SINGLE_VIDEO_WRITERS = "Can't create one of video writers";

    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
    private Imgcodecs imageCodecs;

    private AdditionalParamsInfo additionalParamsInfo;
    private SaveVideoInfo saveVideoInfo;
    private ConsumerInfo consumerInfo;
    private MysqlInfo mysqlCfg;
    private MysqlDb mysqlDb = new MysqlDb();


    /** Additional info 1: may be its better to create Class with CammedVideo{values:frame_counter..., methods:VideoWriter, create(),save(), appendImg()}
    and place them inside hash-map ??? <String, CammedVideo> (now <String, VideoWriter> used)**/
    // Local hash-map variables used for storing ip-camera data
    private HashMap<String, VideoWriter> hmSpathSaveWriters;//key: stream-cam-path, value: VideoWriter
    private HashMap<String, Long> hmSpathSaveFcounter;//key: stream-cam-path, value: long

    private HashMap<String, String> hmapSpathFpath;//key: any, value: Folder (For Policy Purposes, cause all videos stored in one folder key=`any`)
    private HashMap<String, String> hmSpathIp;//key: stream-cam-path, value: VideoWriter
    private PolicyManager pman = new PolicyManager();

    /* Initialization of kafka consumer properties
     *   Input:
     *       ConsumerInfo info: user settings for consumer
     * */
    private void setConsumerProps(ConsumerInfo info) throws IOException {
        initConsumerProperties(info);
        this.propsConsumer.put("key.deserializer", StringDeserializer.class.getName());//, "org.apache.kafka.common.serialization.StringDeserializer");
        this.propsConsumer.put("value.deserializer", transfer.FramePkgDeserializer.class.getName());//"video.kafka.transfer.FramePkgDeserializer");
    }

    private String getDayOfMonth()
    {
        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        String dayOfMonthStr = String.valueOf(dayOfMonth);
        return dayOfMonthStr;
    }

    public static String getCurrentDate()
    {
        String DATE_FORMAT_NOW = "yyyyMMdd_HH-mm-ss";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    /* Create new video writer for each video source from hash-map
    * and reset Frame counter added for saving */
    private boolean createAllVideoWritersAndRstFcounter(String videoSaveFpath, double fps, double w, double h)
    {
        boolean status = false;
        try {
            // get the current date used for file name creation
            String day = getDayOfMonth();
            String date = getCurrentDate();

            // for each camera from hash-map create (based on HM:<key:stream path, camera ip>) new store stream and file
            Map<String, String> map = this.hmSpathIp;
            for (Map.Entry<String, String> entry : map.entrySet())
            {
                String key = entry.getKey();
                if(!createSingleVideoWriterAndRstCounter(key, fps, w, h, videoSaveFpath, day, date))
                    throw new Exception(ERROR_TO_CREATE_OPENCV_SINGLE_VIDEO_WRITERS);
            }// end of for each stream source
            status = true;
        }
        catch (Exception e){
            logger.error(ERROR_TO_CREATE_OPENCV_VIDEO_WRITERS+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* Useful additional method for coding purposes with using private variables */
    private boolean createAllVideoWritersAndRstFcounter(){return createAllVideoWritersAndRstFcounter(this.saveVideoInfo.saveFolderPath, this.saveVideoInfo.fps, this.saveVideoInfo.w, this.saveVideoInfo.h);}

    /* Close all saving streams and save already created files */
    private void saveVideoWriterAll()
    {
        Map<String, VideoWriter> map = this.hmSpathSaveWriters;
        for (Map.Entry<String, VideoWriter> entry : map.entrySet())
            saveVideoWriter(entry.getKey());
    }

    private void saveVideoWriter(String key)
    {
        try {
            VideoWriter videoWriter = this.hmSpathSaveWriters.get(key);
            videoWriter.release();
        } catch (Exception e) {
            logger.error(ERROR_WHILE_SAVING_VIDEOS+": {}, {}, {}", key, e.getMessage(), e.getMessage());
        }
    }

    /* Create saving stream and set to zero frame counter */
    private boolean createSingleVideoWriterAndRstCounter(String key, double fps, double w, double h, String videoSaveFpath, String day, String date)
    {
        boolean status = false;
        String ip = this.hmSpathIp.get(key);
        String outputFile = videoSaveFpath + ip + "_" + date  + "___" + day+ ".avi";
        try {
            // create new save stream to the file
            VideoWriter videoWriter = new VideoWriter(outputFile, VideoWriter.fourcc('x', '2','6','4'),//('M','J','P','G'),//
                    fps, new Size(w,h) );

            // store new created writer and set stored frame counter to zero
            if (this.hmSpathSaveWriters.containsKey(key)) {
                this.hmSpathSaveWriters.replace(key, videoWriter);
                this.hmSpathSaveFcounter.replace(key, new Long(0));
            }
            else {
                this.hmSpathSaveWriters.put(key, videoWriter);
                this.hmSpathSaveFcounter.put(key, new Long(0));
            }
            status = true;
        }
        catch (Exception e){
            logger.error(ERROR_TO_CREATE_OPENCV_SINGLE_VIDEO_WRITERS+", {}, {}, {}, {}", key, outputFile, e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* Useful additional method for coding purposes with using private variables */
    private boolean createSingleVideoWriterAndRstCounter(String key){return createSingleVideoWriterAndRstCounter(key,
            this.saveVideoInfo.fps, this.saveVideoInfo.w, this.saveVideoInfo.h, this.saveVideoInfo.saveFolderPath, getDayOfMonth(),getCurrentDate());}

    /* Private members setter and initializer and launch */
    public VideoSaverConsumer(ConsumerInfo consumerInfo, MysqlInfo mysqlInfo, SaveVideoInfo saveVideoInfo, AdditionalParamsInfo additionalParamsInfo)
    {
        try {
            // store input configs
            this.consumerInfo = consumerInfo;
            this.additionalParamsInfo = additionalParamsInfo;
            this.saveVideoInfo = saveVideoInfo;

            logger = LogManager.getLogger(VideoSaverConsumer.class);

            // create consumer connected to topic (fetching images)
            setConsumerProps(this.consumerInfo);
            this.consumer = new KafkaConsumer<String, FramePkg>(this.propsConsumer);
            this.consumer.subscribe(Collections.singletonList(this.consumerInfo.topicName));

            // init video-variables
            this.hmSpathSaveWriters = new HashMap<String, VideoWriter>();
            this.hmSpathSaveFcounter = new HashMap<String, Long>();

            // init mysql connection
            this.mysqlCfg = mysqlInfo;
            this.mysqlDb.connect2db(mysqlInfo);

            // retreive information about ip-cameras from MySql
            this.hmSpathIp = this.mysqlDb.getListOfIpcamsFromTableByStreamKeyIpValue();//key: cameraId==Stream-path
            if(this.hmSpathIp.size() < 1)
                throw new Exception(ERROR_NO_CAMERAS_RETREIVED_FROM_IP_CAM_TABLE+", hmSpathIp()");

            // create hash-map for checking policies folder
            this.hmapSpathFpath = new HashMap<String, String>();
            this.hmapSpathFpath.put("any", this.saveVideoInfo.saveFolderPath);

            // set policy variables
            this.pman.policyCheckTimeIntervalSeconds = saveVideoInfo.policyCheckTimeIntervalSeconds;//2;
            this.pman.maxStoreTimeSeconds = saveVideoInfo.maxStoreTimeSeconds;//5*60*60*14;
            this.pman.maxFolderSizeBytes = saveVideoInfo.maxFolderSizeBytes;//707*5;
            this.pman.rmPercantage = 0.0 < saveVideoInfo.rmPercentage && saveVideoInfo.rmPercentage < 100.0 ? saveVideoInfo.rmPercentage: this.pman.rmPercentageMin;//50

            // retreive information about last records in storage folders of such cameras and update TimerPolicy Date
            this.pman.hmFpathRecordOldestTime = new HashMap<String, Long>();
            this.pman.hmFoldpath2LastTimePolicyDate = new HashMap<String, Long>();
            if(this.pman.updateIpcamFirstRecordsTime(this.hmapSpathFpath) && this.pman.hmFpathRecordOldestTime.size() < 1)
                throw new Exception(ERROR_NO_CAMERAS_RETREIVED_FROM_IP_CAM_TABLE+", hmapFpathRecordOldestTime()");

            logger.info(MSG_START_PROD + this);
        } catch (Exception e){
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
            System.exit(-1);
        } finally {
            start();
        }
    }

    /* save image on hdd by OpenCV */
    private boolean writeToVideoWwriter(ConsumerRecords<String, FramePkg> messages)
    {
        boolean status = false;
        try {
            for (ConsumerRecord<String, FramePkg> message : messages)
            {
                FramePkg fpkg = message.value();
                String spath = fpkg.get_srcId();

                Long counter = this.hmSpathSaveFcounter.get(spath);
                if (counter >= this.saveVideoInfo.totalNumbFrameInVideo)
                {// start new videos and rst counter
                    saveVideoWriter(spath);
                    createSingleVideoWriterAndRstCounter(spath);
                    counter = new Long(0);
                    //this.hmSpathSaveFcounter.replace(spath, counter);
                }
                // write to VideoWriter resized to standard image
                Mat simg = new Mat();//additionalParamsInfo.resizeMat(fpkg.byte2Mat());
                Imgproc.resize(fpkg.byte2Mat(), simg, new Size(this.saveVideoInfo.w, this.saveVideoInfo.h));
                this.hmSpathSaveWriters.get(spath).write(simg);

                // increment counter
                this.hmSpathSaveFcounter.replace(spath, counter+1);
            }
            status = true;
        } catch (Exception e) {
            logger.error(ERROR_CANT_PROC_PKG+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    public void run()
    {
        logger.info(MSG_START_CONS_RUN + this);

        int loopn = 0;
        int empty_record_poll = 0;
        try {
            createAllVideoWritersAndRstFcounter();
            this.consumer.poll(Duration.ofMillis(1000));
            if (this.consumerInfo.checkTopicZeroPos)
                setOffset2zero();

            Thread.sleep(this.consumerInfo.startDelayMs);
            while (true)
            {
                try {
                    // read topic img-messges
                    final ConsumerRecords<String, FramePkg> messages = this.consumer.poll(Duration.ofMillis(1000));

                    if (!messages.isEmpty())
                    {// If some data was read: process and save results for each non empty message
                        if (writeToVideoWwriter(messages))
                        {// reset counter & commit data & check update storage Time Timer and Size Timer

                            // check update time policy
                            boolean isPolicyUpdateDone = this.pman.updateStorageByPolicies(this.hmapSpathFpath);

                            empty_record_poll = 0;
                            this.consumer.commitSync();
                        } else {// if cant process full received pkg: no sync & try again after sleep
                            logger.error(ERROR_CANT_PROC_FULL_NONEMPTY_PKG);
                            Thread.sleep(this.consumerInfo.sleeptMs);
                        }
                    } else {// If no data was read:
                        // if no data in topic more than: (emptyRecordPollLimit * sleeptMs) [ms]
                        if (empty_record_poll > this.consumerInfo.emptyRecordPollLimit)
                        {
                            logger.info(String.format(ERROR_CONS_TERMINATE_NO_DATA_LONGER_S,
                                    this.consumerInfo.sleeptMs / 1000 * this.consumerInfo.emptyRecordPollLimit, this.consumerInfo.sleeptMs, this.consumerInfo.emptyRecordPollLimit));
                            break;
                        } else {// count retries
                            empty_record_poll++;
                            logger.warn(String.format(WARN_EMPTY_RECORD_POLL_COUNTER, empty_record_poll, this.consumerInfo.emptyRecordPollLimit));
                            Thread.sleep(this.consumerInfo.sleeptMs);
                        }
                    }

                } catch (Exception e) {
                    // if process cant process full received pkg - it doesn't sync with others
                    logger.error(ERROR_CANT_PROC_PKG+", {}, {}", e.getMessage(), e.getMessage());
                    Thread.sleep(this.consumerInfo.sleeptMs);
                }

            }
        } catch (Exception e) {
            logger.error("{}, {}", e.getMessage(), e.getMessage());
        } finally {
            saveVideoWriterAll();
            this.consumer.close();
        }
        logger.info( MSG_END_CONS_RUN+ this);
    }

}
