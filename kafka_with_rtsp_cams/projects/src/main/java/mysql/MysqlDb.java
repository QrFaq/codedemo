package mysql;

import java.sql.*;
import java.util.HashMap;

/**
 * Database information:
 * CREATE DATABASE TestDb;
 * Ip-camera info table:
 * CREATE TABLE ipcam_table(id TINYINT, name TEXT, ip TEXT, port TEXT, video_stream_link TEXT, frame_storage_folder TEXT);
 * Test data:
 * INSERT INTO ipcam_table VALUES  (1, "roof", "202.245.13.81", "00", "http://202.245.13.81:80/cgi-bin/camera?resolution=640", "/media/truecrypt2/Project_storage/kaffka_with_videos/maven_project/PhotoDB/roof_202.245.13.81");
 * INSERT INTO ipcam_table VALUES  (2, "garden", "freja.hiof.no", "1935", "rtsp://freja.hiof.no:1935/rtplive/_definst_/hessdalen02.stream", "/media/truecrypt2/Project_storage/kaffka_with_videos/maven_project/PhotoDB/garden_freja.hiof.no");
 * INSERT INTO ipcam_table VALUES  (3, "BigBuckBunny_115k.mov", "wowzaec2demo.streamlock.net", "0", "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov", "/media/truecrypt2/Project_storage/kaffka_with_videos/maven_project/PhotoDB/BigBuckBunny_115k.mov_wowzaec2demo.streamlock.net");
 *
 * Grant acces to user with ip (if needed):
 * CREATE USER 'testdb'@'192.168.56.56' IDENTIFIED BY '1234';
 * GRANT ALL PRIVILEGES ON *.* TO 'testdb'@'192.168.56.56' WITH GRANT OPTION;
 * CREATE USER 'testdb'@'%' IDENTIFIED BY '1234';
 * GRANT ALL PRIVILEGES ON *.* TO 'testdb'@'%' WITH GRANT OPTION;
 * FLUSH PRIVILEGES;
 */

public class MysqlDb extends MySqlTemplate {

    private PreparedStatement dbPrepareStat = null;

    //private String serverIp = "localhost";
    //private String serverPort = "3306";
    //private String dbName = "";
    //private String jdbcDbPath = "jdbc:mysql://"+serverIp+":"+serverPort+"/"+dbName;
    //private String uname = "";
    //private String dbPass = "";

    public String ip = null;
    public String streamPath = null;
    public String folderPath = null;

    /* Retreive query results (input is a full query row)
    *  For current used purpose just a 3 cols of row was retreived
    *  */
    private void setIpcamTableRow(ResultSet rs)
    {
        try {
            //id TINYINT, name TEXT, ip TEXT, port TEXT, video_stream_link TEXT, frame_storage_folder TEXT
            this.ip = rs.getString("ip");
            this.streamPath = rs.getString("video_stream_link");
            this.folderPath = rs.getString("frame_storage_folder");

            // Simply Print the results
            logger.info(String.format("%s, %s, %s\n", this.ip, this.streamPath, this.folderPath));
        } catch (SQLException e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
    }

    /* Get list of ip-caneras by video-stream source link (rtsp-link) */
    public void selectIpCamByUniqueRtsplink(String video_stream_link)
    {
        try {
            // MySQL Select Query Tutorial
            String getQueryStatement = "SELECT * FROM ipcam_table WHERE video_stream_link=\""+video_stream_link+"\"";

            this.dbPrepareStat = this.dbConn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            ResultSet rs = this.dbPrepareStat.executeQuery();

            // Let's iterate through the java ResultSet
            while (rs.next()) {
                setIpcamTableRow(rs);
            }
        } catch (SQLException e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
    }

    /* Return hash-map of ip-cameras stream source with Folder path to store data
     * hash-map key: String, stream source
     * hash-map value: String, full foolder path
     * */
    public HashMap<String, String> getListOfIpcamsFromTableByStreamKeyFolderpathValue()
    {
        // stream path to frame storage folder path hash-map
        HashMap<String, String> spath_fpath = new HashMap<String, String>();
        try {
            // MySQL Select Query Tutorial
            String getQueryStatement = "SELECT * FROM ipcam_table";

            this.dbPrepareStat = this.dbConn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            ResultSet rs = this.dbPrepareStat.executeQuery();

            // Let's iterate through the java ResultSet
            while (rs.next()) {
                setIpcamTableRow(rs);
                spath_fpath.put(streamPath, folderPath);
            }
        } catch (SQLException e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
        return spath_fpath;
    }

    /* Return hash-map of ip-cameras stream source with Ip
    * hash-map key: String, stream source
    * hash-map value: String, camera ip
    * */
    public HashMap<String, String> getListOfIpcamsFromTableByStreamKeyIpValue()
    {
        // stream path to frame storage folder path hash-map
        HashMap<String, String> spath_fpath = new HashMap<String, String>();
        try {
            // MySQL Select Query Tutorial
            String getQueryStatement = "SELECT * FROM ipcam_table";

            this.dbPrepareStat = this.dbConn.prepareStatement(getQueryStatement);

            // Execute the Query, and get a java ResultSet
            ResultSet rs = this.dbPrepareStat.executeQuery();

            // Let's iterate through the java ResultSet
            while (rs.next()) {
                setIpcamTableRow(rs);
                spath_fpath.put(streamPath, ip);
            }
        } catch (SQLException e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
        return spath_fpath;
    }

}