package consumer;

import templates.Info;

import java.io.IOException;
import java.util.Properties;

/**
 * Consumer settings class used for creating consumer
 * (with reading and checking set properties)
 **/
public class ConsumerInfo extends Info {
    /* input errors */
    protected final String ERROR_sleeptMs_IS_NOT_SET = "pls, set producer `sleeptMs` property inside `producer.properties` file";
    protected final String ERROR_checkTopicZeroPos_IS_NOT_SET = "pls, set producer `checkTopicZeroPos` property inside `producer.properties` file";
    protected final String ERROR_startDelayMs_IS_NOT_SET = "pls, set producer `startDelayMs` property inside `producer.properties` file";
    protected final String ERROR_emptyRecordPollLimit_IS_NOT_SET = "pls, set producer `emptyRecordPollLimit` property inside `producer.properties` file";
    protected final String ERROR_topicName_IS_NOT_SET = "pls, set producer `topicName` property inside `producer.properties` file";
    /* end */

    public long sleeptMs;                       // thread delay time between retries to poll when data wasnt read
    public boolean checkTopicZeroPos;           // thread flag for resetting topic offset (Useful, when data in topic
                                                //      already exists and 1st started consumer from the group shall
                                                //      set topic offset to 0 and read data + make first commit, otherwise,
                                                //      all topics start from the topic end)
    public int startDelayMs;                    // thread start delay
    public int emptyRecordPollLimit;            // thread empty poll retries limit (when nothing was read), as a result:
                                                //      thread will wait (emptyRecordPollLimit * sleeptMs)[ms] int total
                                                //      before consumer will be killed cause no processing data
    public String topicName;


    public Properties fprops;
    //public String fpathProperties;

    public ConsumerInfo(String fpathProperties) throws IOException {
        this.fprops = readPropertiesFile(fpathProperties);

        if (this.fprops.containsKey("sleeptMs"))
            this.sleeptMs = Long.parseLong(this.fprops.getProperty("sleeptMs"));
        else
            throw new IOException(ERROR_sleeptMs_IS_NOT_SET);


        // unused parameters by `checkers`
        if (this.fprops.containsKey("checkTopicZeroPos"))
            this.checkTopicZeroPos = Boolean.parseBoolean(this.fprops.getProperty("checkTopicZeroPos"));
        else
            throw new IOException(ERROR_checkTopicZeroPos_IS_NOT_SET);

        if (this.fprops.containsKey("startDelayMs"))
            this.startDelayMs = Integer.parseInt(this.fprops.getProperty("startDelayMs"));
        else
            throw new IOException(ERROR_startDelayMs_IS_NOT_SET);

        if (this.fprops.containsKey("emptyRecordPollLimit"))
            this.emptyRecordPollLimit = Integer.parseInt(this.fprops.getProperty("emptyRecordPollLimit"));
        else
            throw new IOException(ERROR_emptyRecordPollLimit_IS_NOT_SET);

        if (this.fprops.containsKey("topicName"))
            this.topicName = this.fprops.getProperty("topicName");
        else
            throw new IOException(ERROR_topicName_IS_NOT_SET);

    }

}
