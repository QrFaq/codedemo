package logmanagers;

import templates.Info;

/**
 * Class for storing time/size checking policies
 * Used for log-rotation
 * **/

public class PolicyInfo extends Info {
    public long policyCheckTimeIntervalSeconds = -1;            // check folders time period [second]
    public long maxStoreTimeSeconds = -1;                       // any files storage time duration on hdd [seconds]
    public long maxFolderSizeBytes = -1;                        // maximum folder size for removing the data (turned off in code)
    public float rmPercentage = 50;                             // remove data percentage based on storage data time, when folder is full [%] (turned off in code)
}
