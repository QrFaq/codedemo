package logmanagers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PolicyManager {
    /* Class Logger-messages */
    protected final String ERROR_WHILE_GETTING_FIRST_RECORD_TIME_IN_FOLDER = "can't get the time of the oldest recordf";
    protected final String ERROR_TO_UPDATE_IP_CAM_FIRST_RECORD_TIME = "Can't retreive the data of created files in folders";
    protected final String ERROR_WHILE_REMOVING_STORED_FILES = "Error occurred while trying to remove old files from hdd";
    protected final String ERROR_CAN_NOT_GET_FILE_ATTRIBUTE = "Can't read file attribute";
    protected final String ERROR_WHILE_REMOVING_STORED_DATA_BY_SIZE_POLICY = "Error occurred while trying to remove files from hdd by Size-policy";
    protected final String ERROR_WHILE_REMOVING_STORED_DATA_BY_TIME_POLICY = "Error occurred while trying to remove old files from hdd by Time-policy";
    protected final String INFO_WRITE_FOLDER_STORAGE_INFORMATION = "Folder size/first modified file date/last modified file date";
    protected final String ERROR_TO_UPDATE_UPDATE_STORAGE_BY_POLICIES = "can't update storage information by policies";
    /* end */

    protected Logger logger;
    public HashMap<String, Long> hmFpathRecordOldestTime;//key: folder path
    public HashMap<String, Long> hmFoldpath2LastTimePolicyDate;//key: fpath
    public HashMap<String, Long> hmFoldpath2LastSizePolicyDate;
    public long policyCheckTimeIntervalSeconds = -1;
    public long maxStoreTimeSeconds = -1;
    public long maxFolderSizeBytes = -1;
    public float rmPercantage = 50;
    public float rmPercentageMin = 25;

    public PolicyManager() {logger = LogManager.getLogger(PolicyManager .class);}

    /* Method for resetting time counter for data checker */
    public void rstTimeCheckPolicy2dateByKey(String key, long time)
    {
        if (this.hmFoldpath2LastTimePolicyDate.containsKey(key))
            this.hmFoldpath2LastTimePolicyDate.replace(key, time);
        else
            this.hmFoldpath2LastTimePolicyDate.put(key, time);
    }

    /* Get the oldest time in folder or return the current data if smth goes wrong */
    private long getFirstRecordTimeInFolder(String fpath, long curTime)
    {
        long firstRecordTimeInFolder = curTime;
        try {
            List<Path> pathList = Files.walk(Paths.get(fpath))
                    .filter(Files::isRegularFile)//.map(Path::toFile)
                    .collect(Collectors.toList());
            for (Path path : pathList)
            {
                BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                long recordTime = attr.creationTime().toMillis();
                if (recordTime < firstRecordTimeInFolder)
                    firstRecordTimeInFolder = recordTime;
            }
        }
        catch (Exception e){
            logger.error(ERROR_WHILE_GETTING_FIRST_RECORD_TIME_IN_FOLDER+", {}, {}", e.getMessage(), e.getMessage());
        }
        return firstRecordTimeInFolder;
    }

    /* Update the ip-camera information about the oldest stored data */
    public boolean updateIpcamFirstRecordsTime(HashMap<String, String> hmapSpathFpath)
    {
        boolean status = false;
        try {
            Map<String, String> map = hmapSpathFpath;
            long curTime = new Date().getTime();
            for (Map.Entry<String, String> entry : map.entrySet())
            {
                //String key = entry.getKey();
                String fpath = entry.getValue();
                long firstRecTime = getFirstRecordTimeInFolder(fpath, curTime);

                // load time
                if (this.hmFpathRecordOldestTime.containsKey(fpath))
                    this.hmFpathRecordOldestTime.replace(fpath, new Long(firstRecTime));
                else
                    this.hmFpathRecordOldestTime.put(fpath, new Long(firstRecTime));

                // update time policy for current update time
                rstTimeCheckPolicy2dateByKey(fpath, curTime);
            }// end of for each stream source
            status = true;
        }
        catch (Exception e){
            logger.error(ERROR_TO_UPDATE_IP_CAM_FIRST_RECORD_TIME+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* remove the oldest files from folder based on files-attribute-time */
    private boolean removeOldFilesByTimePolicy(List<Path> pathList, long curTime, long maxStoreTimeSeconds)//x*24 * 60 * 60
    {
        boolean status = false;
        try {
            for(Path path : pathList)
            {
                if (Files.exists(path))
                {
                    File file = path.toFile();
                    BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                    long diff = new Date().getTime() - attr.creationTime().toMillis();
                    //long diff = new Date().getTime() - file.lastModified();
                    if (diff > maxStoreTimeSeconds * 1000)
                        file.delete();
                }
            }
            status = true;
        } catch (Exception e) {
            logger.error(ERROR_WHILE_REMOVING_STORED_FILES+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* method for removing files in selected time interval [start, end] */
    private boolean removeFilesInTimeInterval(List<Path> pathList, long startTimeMillis, long endTimeMillis)
    {
        boolean status = false;
        try {
            for(Path path : pathList)
            {
                if (Files.exists(path))
                {
                    File file = path.toFile();
                    BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                    long creationtime = attr.creationTime().toMillis();//"lastModifiedTime: " + attr.lastModifiedTime()
                    if (startTimeMillis <= creationtime && creationtime <= endTimeMillis)
                        file.delete();
                }
            }
            status = true;
        } catch (Exception e) {
            logger.error(ERROR_WHILE_REMOVING_STORED_FILES+" removeFilesInTimeInterval(), {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* remove files from folders based on folder size (additionl info in: README) */
    private boolean removeFilesBySizePolicy(List<Path> pathList, long maxFolderSizeBytes, float rmPercantage)
    {
        boolean status = false;
        /*Files.find( Paths.get(fpath),
                Integer.MAX_VALUE,
                (filePath, fileAttr) -> fileAttr.isRegularFile())
                .collect(Collectors.toList());
                //.forEach(System.out::println);*/

        long currentSize = 0;
        long firstModified = Long.MAX_VALUE;
        long lastModifiedTime = Long.MIN_VALUE;
        for(Path path : pathList)
        {
            try {
                if (Files.exists(path))
                {
                    BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                    currentSize += attr.size();
                    long creationtime = attr.creationTime().toMillis();//"lastModifiedTime: " + attr.lastModifiedTime()

                    // search oldest & newest file time
                    if (creationtime < firstModified)
                        firstModified = creationtime;
                    if (creationtime > lastModifiedTime)
                        lastModifiedTime = creationtime;
                }
            } catch (IOException e) {
                logger.error(ERROR_CAN_NOT_GET_FILE_ATTRIBUTE + ", {}, {}, {}", path.toString(), e.getMessage(), e.getMessage());
            }
        }
        logger.info(INFO_WRITE_FOLDER_STORAGE_INFORMATION+": {} , {}, {}", currentSize, firstModified, lastModifiedTime);

        try {
            if(currentSize > maxFolderSizeBytes && firstModified < lastModifiedTime)
            {// free memory by removing old data in time interval: [firstModified, x% * lastmodified)
                long tdelta = firstModified+(long)( (lastModifiedTime-firstModified)*rmPercantage/100);
                if(!removeFilesInTimeInterval(pathList, firstModified, tdelta))
                    throw new Exception(ERROR_WHILE_REMOVING_STORED_DATA_BY_SIZE_POLICY);
            }
            status = true;
        } catch (Exception e) {
            logger.error(ERROR_CAN_NOT_GET_FILE_ATTRIBUTE + ", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* Remove Old files OR remove by lack of storage based on Time */
    public boolean updateStorageByPolicies(HashMap<String, String> hmapSpathFpath, long policyCheckTimeIntervalSeconds, long maxStoreTimeSeconds, long maxFolderSizeBytes, float rmPercantage)
    {
        boolean status = false;
        long curTime = new Date().getTime();
        try {
            Map<String, String> map = hmapSpathFpath;
            for (Map.Entry<String, String> entry : map.entrySet())
            {
                //String camId = entry.getKey();
                String folderPath = entry.getValue();
                long policyTime = this.hmFoldpath2LastTimePolicyDate.get(folderPath);
                long diff = curTime - policyTime;
                boolean isTimeCheckPolicyOverflow = diff > (policyCheckTimeIntervalSeconds*1000);
                if (isTimeCheckPolicyOverflow)
                {
                    // retreive paths about files from the folder
                    List<Path> pathList = Files.walk(Paths.get(folderPath))
                            .filter(Files::isRegularFile)//.map(Path::toFile)
                            .collect(Collectors.toList());

                    // remove old created elements
                    if(!removeOldFilesByTimePolicy(pathList, curTime, maxStoreTimeSeconds))
                        throw new Exception(ERROR_WHILE_REMOVING_STORED_DATA_BY_TIME_POLICY);

                    // remove `rmPercantage`: canceled by Vadim
                    //if(!removeFilesBySizePolicy(pathList, maxFolderSizeBytes, rmPercantage))
                    //    throw new Exception(ERROR_WHILE_REMOVING_STORED_DATA_BY_SIZE_POLICY);

                    // update time policy
                    rstTimeCheckPolicy2dateByKey(folderPath, curTime);
                }
            }// end of for each stream source
            status = true;
        }
        catch (Exception e){
            logger.error(ERROR_TO_UPDATE_UPDATE_STORAGE_BY_POLICIES+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

    /* Remove Old files OR remove by lack of storage based on Time */
    public boolean updateStorageByPolicies(HashMap<String, String> hmapSpathFpath){return updateStorageByPolicies(hmapSpathFpath, this.policyCheckTimeIntervalSeconds, this.maxStoreTimeSeconds, this.maxFolderSizeBytes, this.rmPercantage);}
}
