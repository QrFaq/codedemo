package logmanagers;

import mysql.MysqlInfo;

import java.io.IOException;
import java.util.Properties;

/**
 * Class with information about saving images
 * (with reading and checking properties)
 * **/

public class SaveImageInfo extends PolicyInfo {
    /* input errors */
    protected final String ERROR_policyCheckTimeIntervalSeconds_IS_NOT_SET = "pls, set SaveImageInfo `policyCheckTimeIntervalSeconds` " +
            "`saveImageInfo.properties` configuration file";
    /* end */
    protected final String ERROR_maxStoreTimeSeconds_IS_NOT_SET = "pls, set SaveImageInfo `maxStoreTimeSeconds` " +
            "`saveImageInfo.properties` configuration file";
    protected final String ERROR_maxFolderSizeBytes_IS_NOT_SET = "pls, set SaveImageInfo `maxFolderSizeBytes` " +
            "`saveImageInfo.properties` configuration file";
    protected final String ERROR_rmPercentage_IS_NOT_SET = "pls, set SaveImageInfo `rmPercentage` " +
            "`saveImageInfo.properties` configuration file";

    public SaveImageInfo (){}
    public SaveImageInfo (long policyCheckTimeIntervalSeconds, long maxStoreTimeSeconds, long maxFolderSizeBytes,
                          float rmPercentage)
    {
        this.policyCheckTimeIntervalSeconds = policyCheckTimeIntervalSeconds;//2;
        this.maxStoreTimeSeconds = maxStoreTimeSeconds;//5*60*60*14;
        this.maxFolderSizeBytes = maxFolderSizeBytes;//707*5;
        this.rmPercentage = rmPercentage;//50
    }

    public SaveImageInfo(String propertiesPath) throws IOException {
        Properties fprops = readPropertiesFile(propertiesPath);

        if (fprops.containsKey("policyCheckTimeIntervalSeconds"))
            this.policyCheckTimeIntervalSeconds = Long.parseLong(fprops.getProperty("policyCheckTimeIntervalSeconds"));
        else
            throw new IOException(ERROR_policyCheckTimeIntervalSeconds_IS_NOT_SET);

        if (fprops.containsKey("maxStoreTimeSeconds"))
            this.maxStoreTimeSeconds = Long.parseLong(fprops.getProperty("maxStoreTimeSeconds"));
        else
            throw new IOException(ERROR_maxStoreTimeSeconds_IS_NOT_SET);

        if (fprops.containsKey("maxFolderSizeBytes"))
            this.maxFolderSizeBytes = Long.parseLong(fprops.getProperty("maxFolderSizeBytes"));
        else
            throw new IOException(ERROR_maxFolderSizeBytes_IS_NOT_SET);

        if (fprops.containsKey("rmPercentage"))
            this.rmPercentage = Float.parseFloat(fprops.getProperty("rmPercentage"));
        else
            throw new IOException(ERROR_rmPercentage_IS_NOT_SET);
    }

}
