package logmanagers;

import java.io.IOException;
import java.util.Properties;

/**
 * Class with information about video saving
 * (with reading and checking properties)
 * **/

public class SaveVideoInfo extends PolicyInfo {
    /* input errors */
    protected final String ERROR_policyCheckTimeIntervalSeconds_IS_NOT_SET = "pls, set SaveVideo `policyCheckTimeIntervalSeconds` in `videoSaveInfo.properties`";
    protected final String ERROR_maxStoreTimeSeconds_IS_NOT_SET = "pls, set SaveVideo `maxStoreTimeSeconds` in `videoSaveInfo.properties`";
    protected final String ERROR_maxFolderSizeBytes_IS_NOT_SET = "pls, set SaveVideo `maxFolderSizeBytes` in `videoSaveInfo.properties`";
    protected final String ERROR_rmPercentage_IS_NOT_SET = "pls, set SaveVideo `rmPercentage` in `videoSaveInfo.properties`";
    protected final String ERROR_totalNumbFrameInVideo_IS_NOT_SET = "pls, set SaveVideo `totalNumbFrameInVideo` in `videoSaveInfo.properties`";
    protected final String ERROR_fps_IS_NOT_SET = "pls, set SaveVideo `w` in `videoSaveInfo.properties`";
    protected final String ERROR_w_IS_NOT_SET = "pls, set SaveVideo `w` in `videoSaveInfo.properties`";
    protected final String ERROR_h_IS_NOT_SET = "pls, set SaveVideo `h` in `videoSaveInfo.properties`";
    protected final String ERROR_saveFolderPath_IS_NOT_SET = "pls, set SaveVideo `saveFolderPath` in `videoSaveInfo.properties`";
    /* end */
    public long totalNumbFrameInVideo = 5;                      // for saving video files
    public double fps = 25.D;
    public double w = 320;
    public double h = 240;
    public String saveFolderPath = "";

    public SaveVideoInfo (){}
    public SaveVideoInfo (long policyCheckTimeIntervalSeconds, long maxStoreTimeSeconds, long maxFolderSizeBytes,
                          float rmPercentage, long totalNumbFrameInVideo, double fps, double w, double h,
                          String saveFolderPath)
    {
        this.policyCheckTimeIntervalSeconds = policyCheckTimeIntervalSeconds;//2;
        this.maxStoreTimeSeconds = maxStoreTimeSeconds;//5*60*60*14;
        this.maxFolderSizeBytes = maxFolderSizeBytes;//707*5;
        this.rmPercentage = rmPercentage;//50
        this.totalNumbFrameInVideo = totalNumbFrameInVideo;
        this.fps = fps;
        this.w = w;
        this.h = h;
        this.saveFolderPath = saveFolderPath;
    }

    public SaveVideoInfo(String propertiesPath) throws IOException {
        Properties fprops = readPropertiesFile(propertiesPath);

        if (fprops.containsKey("policyCheckTimeIntervalSeconds"))
            this.policyCheckTimeIntervalSeconds = Long.parseLong(fprops.getProperty("policyCheckTimeIntervalSeconds"));
        else
            throw new IOException(ERROR_policyCheckTimeIntervalSeconds_IS_NOT_SET);

        if (fprops.containsKey("maxStoreTimeSeconds"))
            this.maxStoreTimeSeconds = Long.parseLong(fprops.getProperty("maxStoreTimeSeconds"));
        else
            throw new IOException(ERROR_maxStoreTimeSeconds_IS_NOT_SET);

        if (fprops.containsKey("maxFolderSizeBytes"))
            this.maxFolderSizeBytes = Long.parseLong(fprops.getProperty("maxFolderSizeBytes"));
        else
            throw new IOException(ERROR_maxFolderSizeBytes_IS_NOT_SET);

        if (fprops.containsKey("rmPercentage"))
            this.rmPercentage = Float.parseFloat(fprops.getProperty("rmPercentage"));
        else
            throw new IOException(ERROR_rmPercentage_IS_NOT_SET);

        if (fprops.containsKey("totalNumbFrameInVideo"))
            this.totalNumbFrameInVideo = Long.parseLong(fprops.getProperty("totalNumbFrameInVideo"));
        else
            throw new IOException(ERROR_totalNumbFrameInVideo_IS_NOT_SET);

        if (fprops.containsKey("fps"))
            this.fps = Double.parseDouble(fprops.getProperty("fps"));
        else
            throw new IOException(ERROR_fps_IS_NOT_SET);

        if (fprops.containsKey("w"))
            this.w = Double.parseDouble(fprops.getProperty("w"));
        else
            throw new IOException(ERROR_w_IS_NOT_SET);

        if (fprops.containsKey("h"))
            this.h = Double.parseDouble(fprops.getProperty("h"));
        else
            throw new IOException(ERROR_h_IS_NOT_SET);

        if (fprops.containsKey("saveFolderPath"))
            this.saveFolderPath = fprops.getProperty("saveFolderPath");
        else
            throw new IOException(ERROR_saveFolderPath_IS_NOT_SET);
    }


}
