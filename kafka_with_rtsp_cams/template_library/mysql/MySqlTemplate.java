package mysql;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Mysql connection template class (the most usefull for every child)
 * **/

public class MySqlTemplate {
    /* Class Logger-messages */
    protected final String MSG_TRY_CONNECT_JDBC = "-------- Connect to Mysql DB by JDBC ------------";
    protected final String MSG_CONFIRM_JDBC_REGISTERED = "MySQL JDBC Driver Registered!";
    protected final String MSG_MYSQL_CONNECTION_SUCCESS = "MySql Connection Successful";
    protected final String MSG_MYSQL_QUERY_INFO = "Execute:";

    protected final String ERROR_MYSQL_CONNECTION_FAILED = "Failed to make connection";
    protected final String ERROR_JDBC_DRIVER_NOT_FOUND = "Sorry, couldn't found JDBC driver. Make sure you have added JDBC Maven Dependency Correctly";
    /* end */
    protected Logger logger;

    protected Connection dbConn = null;
    protected MysqlInfo mysqlInfo;

    // Examples:
    //private String serverIp = "localhost";
    //private String serverPort = "3306";
    //private String dbName = "LDetector";
    //private String jdbcDbPath = "jdbc:mysql://"+serverIp+":"+serverPort+"/"+dbName;
    //private String uname = "root";
    //private String dbPass = "";

    public boolean connect2db(String serverIp, String serverPort, String dbName, String uname, String dbPass)
    {
        boolean status = false;
        try {
            logger = LogManager.getLogger(MySqlTemplate.class);
            this.mysqlInfo = new MysqlInfo(serverIp, serverPort, dbName, uname, dbPass);
            logger.info(MSG_TRY_CONNECT_JDBC);
            if(makeJDBCConnection())
                status = true;
        } catch (Exception e) {
            logger.error(ERROR_MYSQL_CONNECTION_FAILED+", {}, {}", e.getMessage(), e.getMessage());
            //System.exit(-1);
        }
        return status;
    }

    public boolean connect2db(MysqlInfo mysqlInfo)
    {
        boolean status = false;
        try {
            logger = LogManager.getLogger(MySqlTemplate.class);
            this.mysqlInfo = mysqlInfo;
            logger.info(MSG_TRY_CONNECT_JDBC);
            if(makeJDBCConnection())
                status = true;
        } catch (Exception e) {
            logger.error(ERROR_MYSQL_CONNECTION_FAILED+", {}, {}", e.getMessage(), e.getMessage());
            //System.exit(-1);
        }
        return status;
    }

    private boolean makeJDBCConnection()
    {
        boolean status = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            logger.info(MSG_CONFIRM_JDBC_REGISTERED);

            //DriverManager.getConnection("jdbc:mysql://localhost:3306/crunchify", "root", "root");
            this.dbConn = DriverManager.getConnection(this.mysqlInfo.jdbcPath, this.mysqlInfo.uname, this.mysqlInfo.pass);
            if (dbConn != null) {
                logger.info(MSG_MYSQL_CONNECTION_SUCCESS);
            } else {
                logger.info(ERROR_MYSQL_CONNECTION_FAILED);
                throw new Exception(ERROR_MYSQL_CONNECTION_FAILED);
            }
            status = true;
        } catch (ClassNotFoundException e) {
            logger.info(ERROR_JDBC_DRIVER_NOT_FOUND+", {}, {}", e.getMessage(), e.getMessage());
        } catch (SQLException e) {
            logger.error(ERROR_MYSQL_CONNECTION_FAILED+", {}, {}", e.getMessage(), e.getMessage());
        } catch (Exception e) {
            logger.error(ERROR_MYSQL_CONNECTION_FAILED+", {}, {}", e.getMessage(), e.getMessage());
        }
        return status;
    }

}
