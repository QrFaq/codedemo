package mysql;

import templates.Info;

import java.io.IOException;
import java.util.Properties;

/**
 * MySql connection information class
 * (with reading from properties)
 * **/

public class  MysqlInfo extends Info {
    /* input errors */
    protected final String ERROR_ip_IS_NOT_SET = "pls, set MySqlInfo `ip` property inside `mysql.properties` configuration file depends on your task";
    protected final String ERROR_port_IS_NOT_SET = "pls, set MySqlInfo `port` property inside `mysql.properties` configuration file depends on your task";
    protected final String ERROR_db_IS_NOT_SET = "pls, set MySqlInfo `db` property inside `mysql.properties` configuration file depends on your task";
    protected final String ERROR_uname_IS_NOT_SET = "pls, set MySqlInfo `uname` property inside `mysql.properties` configuration file depends on your task";
    protected final String ERROR_pass_IS_NOT_SET = "pls, set MySqlInfo `pass` property inside `mysql.properties` configuration file depends on your task";
    /* end */

    String ip = "";
    String port = "";
    String db = "";
    String uname = "";
    String pass = "";
    String jdbcPath = "jdbc:mysql://...";

    public MysqlInfo (){}
    public MysqlInfo (String propertiesPath) throws IOException {
        Properties fprops = readPropertiesFile(propertiesPath);

        if (fprops.containsKey("ip"))
            this.ip = fprops.getProperty("ip");
        else
            throw new IOException(ERROR_ip_IS_NOT_SET);

        if (fprops.containsKey("port"))
            this.port = fprops.getProperty("port");
        else
            throw new IOException(ERROR_port_IS_NOT_SET);

        if (fprops.containsKey("db"))
            this.db = fprops.getProperty("db");
        else
            throw new IOException(ERROR_db_IS_NOT_SET);

        this.jdbcPath = "jdbc:mysql://"+this.ip+":"+this.port+"/"+this.db;

        if (fprops.containsKey("uname"))
            this.uname = fprops.getProperty("uname");
        else
            throw new IOException(ERROR_uname_IS_NOT_SET);

        if (fprops.containsKey("pass"))
            this.pass = fprops.getProperty("pass");
        else
            throw new IOException(ERROR_pass_IS_NOT_SET);

    }
    public MysqlInfo (String ip, String port, String db, String uname, String pass)
    {
        this.ip = ip;
        this.port = port;
        this.db = db;
        this.jdbcPath = "jdbc:mysql://"+this.ip+":"+this.port+"/"+this.db;
        this.uname = uname;
        this.pass = pass;
    }
    public MysqlInfo (MysqlInfo info)
    {
        this.ip = info.ip;
        this.port = info.port;
        this.db = info.db;
        this.jdbcPath = info.jdbcPath;
        this.uname = info.uname;
        this.pass = info.pass;
    }

};
