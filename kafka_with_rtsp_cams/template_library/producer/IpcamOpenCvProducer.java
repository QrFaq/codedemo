package producer;

import consumer.ConsumerInfo;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import templates.AdditionalParamsInfo;
import templates.KafkaTemplateThread;
import transfer.FramePkg;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class IpcamOpenCvProducer extends KafkaTemplateThread {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    private AdditionalParamsInfo additionalParamsInfo;
    private ProducerInfo producerInfo;
    private ConsumerInfo consumerInfo;

    //private String cameraId = "";                   // image IP-camera source (Not used now)
    private String vstreamPath = "";                  // image video-file/IP-camera source
    private VideoCapture capture;

    private KafkaConsumer consumerTopicChecker;//used this for a kind of renaming

    private Long MAX_MES_NUMBER_PER_TOPIC = new Long(this.topicMesLimit);               // the same as `topicMesLimit`, but type<Long>
    private Long HALF_MES_NUMBER_PER_TOPIC = new Long((int) (this.topicMesLimit/2) );   // the half of `topicMesLimit`
    private int frameCounterSendMsg = 0;
    private int maxNumberToSkip = 20;

    /** flush() with ack=all guarantees that the messages have been sent and has been replicated on the cluster with required replication factor.
     * https://kafka-python.readthedocs.io/en/master/apidoc/KafkaProducer.html
     * https://stackoverflow.com/questions/56019211/kakfa-producer-message-delivery-with-acks-all-and-flush
    * */

    private void setProducerProps(ProducerInfo info) throws IOException {
        initProducerProperties(info);
        this.propsProducer.put("key.serializer", StringSerializer.class.getName());
        this.propsProducer.put("value.serializer", transfer.FramePkgSerializer.class.getName());

        // maxNumberToSkip
        if (info.fprops.containsKey("maxNumberToSkip"))
            this.maxNumberToSkip = Integer.parseInt(info.fprops.getProperty("maxNumberToSkip"));
        else
            throw new IOException(ERROR_maxNumberToSkip_IS_NOT_SET);

    }

    /* Initialization of kafka consumer (Topic size checker) properties
     *   Input:
     *       ProducerInfo consumerInfo: user settings for consumer
     *   Return: Properties: kafka properties class
     * */
    private void setConsumerTopicCheckerProps(ConsumerInfo info) throws IOException {
        initConsumerProperties(info);
        this.propsConsumer.put("key.deserializer", StringDeserializer.class.getName());
        this.propsConsumer.put("value.deserializer", transfer.FramePkgDeserializer.class.getName());
    }

    /* Private members setter and initializer and launch */
    public IpcamOpenCvProducer(String camIp, ProducerInfo producerInfo, ConsumerInfo consumerInfo, AdditionalParamsInfo additionalParamsInfo)
    {
        try {
            logger = LogManager.getLogger(KafkaVideoProducer.class);
            logger.trace(MSG_CREATE_PROD);

            this.additionalParamsInfo = additionalParamsInfo;
            this.producerInfo = producerInfo;
            this.consumerInfo = consumerInfo;

            // set class variables
            this.vstreamPath = camIp != "" ? camIp : null;
            if (vstreamPath == null)
                throw new Exception(ERROR_VIDEO_SOURCE_UNSPECIFIED);
            this.maxNumberToSkip = this.maxNumberToSkip < 0 ? 0 : this.maxNumberToSkip;

            // initialize producer for sending img-messages to topic
            setProducerProps(producerInfo);
            this.producer = new KafkaProducer<String, FramePkg>(this.propsProducer);

            // Initialize consumer for checking number of messages inside the topic
            setConsumerTopicCheckerProps(consumerInfo);
            this.consumer = new KafkaConsumer<String, FramePkg>(this.propsConsumer);
            this.consumerTopicChecker = this.consumer;
            this.consumerTopicChecker.subscribe(Collections.singletonList(this.consumerInfo.topicName));

            logger.info(MSG_START_PROD + this);
        } catch (Exception e){
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
            System.exit(-1);
        } finally {
            start();
        }
    }

    /*
     * Pkg sender with sending structuring
     *   Input:
     *           Mat img: sending image
     *           int frameId: image number (from counter)
     *           long timestamp: reading time
     * */
    public void sendFramePkg(Mat img, int frameId, long timestamp, long fps_time_ms)
    {
        logger.debug(MSG_START_CREATE_FRAMEPKG);
        FramePkg framePkg = new FramePkg(this.vstreamPath, img,
                img.rows(), img.cols(), img.channels(), img.type(),
                frameId, timestamp, fps_time_ms);
        logger.debug(MSG_END_CREATE_FRAMEPKG);

        logger.debug(MSG_START_SEND_MSG);
        try {
            this.producer.send(
                    new ProducerRecord<String, FramePkg>(this.producerInfo.topicName, framePkg),
                    new KafkaTemplateThread.EventGeneratorCallback(this.vstreamPath)
            );
            this.producer.flush();
        } catch (KafkaException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
    }

    /*
     * Processing thread:
     *   0. read image from topic
     *   1. fetch data and commit
     *   2. send to topic
     *    . control: source is opened
     *   4. control: number of empty inputs
     *   3. control: topic size
     * */

    private void waitAndReconnect2Ipcam()
    {
        try {
            this.capture.release();
            Thread.sleep(this.sleeptMs);
            this.capture = new VideoCapture(this.vstreamPath);
        } catch (Exception e)
        {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
    }

    private void connect2disconnect()
    {
        int totalFrameCounter = 0;
        int empty_frameCounter = 0;
        while (true)
        {
            try{
                Mat matOrig = new Mat();
                if( this.capture.isOpened())
                {
                    if (!isTopicFull())
                    {
                        //this.capture.read(matOrig);
                        boolean ret = this.capture.grab();
                        ret = this.capture.retrieve(matOrig);

                        // get some meta data about frame.
                        double fps = this.capture.get(Videoio.CAP_PROP_FPS);//5
                        double frameCount = this.capture.get(Videoio.CAP_PROP_FRAME_COUNT);//7
                        double h = this.capture.get(Videoio.CAP_PROP_FRAME_HEIGHT);//4
                        double w = this.capture.get(Videoio.CAP_PROP_FRAME_WIDTH);//3
                        double posFrames = this.capture.get(Videoio.CAP_PROP_POS_FRAMES);//0
                        double posMsec = this.capture.get(Videoio.CAP_PROP_POS_MSEC);//1
                        double speed = this.capture.get(Videoio.CAP_PROP_SPEED);//30
                        logger.info(String.format(MSG_READED_FRAME_INFO, fps, frameCount, h, w, posFrames, posMsec, speed ) );

                        if( !matOrig.empty() )
                        {// send non-empty image
                            // resize image
                            Mat resizeimage = additionalParamsInfo.resizeMat(matOrig);
                            logger.info(String.format(MSG_RESEIZED_IMG_INFO, resizeimage.height(), resizeimage.width() ) );

                            long timestamp = Instant.now().getEpochSecond();
                            sendFramePkg(resizeimage, (int) totalFrameCounter, (long) timestamp, (long) Math.round(posFrames/fps*1000));
                            totalFrameCounter = totalFrameCounter < Integer.MAX_VALUE-1 ? totalFrameCounter+1 : 0;
                            this.frameCounterSendMsg++;

                            // Draw frame: Debug//
                            //drawFrameDebug.display(resizeimage);

                            // reset & continue receive frames
                            empty_frameCounter = 0;
                        } else {
                            // if number of empty frame is greater than maximum
                            if (empty_frameCounter > this.maxEmptyFramesNumb) {
                                logger.info(ERROR_PROD_TERMINATED_TOOMUCH_EMPTY_FRAMES);
                                waitAndReconnect2Ipcam();
                            } else {// try to reinit connection after some pause of empty frame was read
                                empty_frameCounter++;
                                logger.warn(String.format(WARN_EMPTY_RECORD_SRC_FRAME_COUNTER+": %d/%d ", empty_frameCounter, this.maxEmptyFramesNumb) );
                                waitAndReconnect2Ipcam();
                            }
                        }
                    }// end isTopicFull()
                } else {
                    logger.warn(String.format(WARN_NO_CONNCETION_TO_IP_CAM_RECONNECTING) );
                    waitAndReconnect2Ipcam();
                }// end isOpened()
            } catch (KafkaException e) {
                logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
                //System.exit(-1);
                waitAndReconnect2Ipcam();
            } catch (Exception e) {
                logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
                //System.exit(-1);
                waitAndReconnect2Ipcam();
            } finally {
                logger.info(MSG_END_PROD_RUN);
                //drawFrameDebug.dispose();
            }
        }// end while
    }

    public void run()
    {
        // Load native library
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        logger.info(MSG_START_PROD_RUN + this.producerInfo.topicName);

        this.capture = new VideoCapture(this.vstreamPath);
        //DrawFrameDebug drawFrameDebug = new DrawFrameDebug();

        try {
            Mat resizeimage = new Mat();
            Mat matOrig = new Mat();
            int totalFrameCounter = 0;
            int empty_frameCounter = 0;
            int skip_counter = 0;
            while (true)
            {
                try{
                    if( this.capture.isOpened())
                    {
                        if (!isTopicFull())
                        {
                            this.capture.read(matOrig);
                            //boolean ret = this.capture.grab();//Alternative for message reading, but example above worked well
                            //ret = this.capture.retrieve(matOrig);

                            // get some meta data about frame (most of the parameters work only for videos)
                            double fps = this.capture.get(Videoio.CAP_PROP_FPS);//5
                            double frameCount = this.capture.get(Videoio.CAP_PROP_FRAME_COUNT);//7
                            double h = this.capture.get(Videoio.CAP_PROP_FRAME_HEIGHT);//4
                            double w = this.capture.get(Videoio.CAP_PROP_FRAME_WIDTH);//3
                            double posFrames = this.capture.get(Videoio.CAP_PROP_POS_FRAMES);//0:
                            double posMsec = this.capture.get(Videoio.CAP_PROP_POS_MSEC);//1
                            double speed = this.capture.get(Videoio.CAP_PROP_SPEED);//30
                            logger.info(String.format(MSG_READED_FRAME_INFO, fps, frameCount, h, w, posFrames, posMsec, speed ) );

                            // send non-empty image
                            if( !matOrig.empty() )
                            {
                                if (skip_counter >= this.maxNumberToSkip)
                                {
                                    // resize image
                                    resizeimage = additionalParamsInfo.resizeMat(matOrig);
                                    logger.info(String.format(MSG_RESEIZED_IMG_INFO, resizeimage.height(), resizeimage.width()));

                                    long timestamp = Instant.now().getEpochSecond();
                                    sendFramePkg(resizeimage, (int) totalFrameCounter, (long) timestamp, (long) Math.round(posFrames / fps * 1000));
                                    this.frameCounterSendMsg++;
                                    skip_counter = 0;

                                    // Draw frame: Debug//
                                    //drawFrameDebug.display(resizeimage);
                                }
                                // reset & continue receive frames
                                empty_frameCounter = 0;
                                skip_counter++;
                                totalFrameCounter = totalFrameCounter < Integer.MAX_VALUE - 1 ? totalFrameCounter + 1 : 0;
                            } else {
                                // if number of empty frame is greater than maximum
                                if (empty_frameCounter > this.maxEmptyFramesNumb) {
                                    logger.info(ERROR_PROD_TERMINATED_TOOMUCH_EMPTY_FRAMES);
                                    waitAndReconnect2Ipcam();
                                } else {// try to reinit connection after some pause of empty frame was read
                                    empty_frameCounter++;
                                    logger.warn(String.format(WARN_EMPTY_RECORD_SRC_FRAME_COUNTER+": %d/%d ", empty_frameCounter, this.maxEmptyFramesNumb) );
                                    waitAndReconnect2Ipcam();
                                }
                            }
                        }// end isTopicFull()
                    } else {
                        logger.warn(String.format(WARN_NO_CONNCETION_TO_IP_CAM_RECONNECTING) );
                        waitAndReconnect2Ipcam();
                    }// end isOpened()
                } catch (KafkaException e) {
                    logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
                    //System.exit(-1);
                    waitAndReconnect2Ipcam();
                } catch (Exception e) {
                    logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
                    //System.exit(-1);
                    waitAndReconnect2Ipcam();
                } finally {
                    logger.info(MSG_END_PROD_RUN);
                    //drawFrameDebug.dispose();
                }
            }// end while
        } catch (Exception e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        } finally {
            logger.info(MSG_END_PROD_RUN);
            this.producer.close();
        }
    }

    private Long countTopicSize()
    {
        Long totalCount = new Long(0);
        try {
            this.consumerTopicChecker.poll(Duration.ofMillis(1000));
            Set<TopicPartition> assignment = this.consumerTopicChecker.assignment();

            final Map<TopicPartition, Long> endOffsets = this.consumerTopicChecker.endOffsets(assignment);
            final Map<TopicPartition, Long> beginningOffsets = this.consumerTopicChecker.beginningOffsets(assignment);

            assert (endOffsets.size() == beginningOffsets.size());
            assert (endOffsets.keySet().equals(beginningOffsets.keySet()));

            totalCount = beginningOffsets.entrySet().stream().mapToLong(entry -> {
                TopicPartition tp = entry.getKey();
                Long beginningOffset = entry.getValue();
                Long endOffset = endOffsets.get(tp);
                return endOffset - beginningOffset;
            }).sum();
        } catch (KafkaException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
        return totalCount;
    }

    /*
     * Method is checking the fullness of the topic queue
     * based on max-parameters:
     * 1. check if total number of messages greater `MAX_MES_NUMBER_PER_TOPIC`
     *       State `Full`: topic is fulled by producer
     * 2. When Full-state is active: sleep for waiting messages will be deleted by time or received
     *       State `Sleep`
     * 3.    State `Check Size`
     * 4. If size After sleep is less `HALF_MES_NUMBER_PER_TOPIC`, than reset `frameCounter`,
     *       State `Ready-to-Send-Again`
     * */
    private boolean isTopicFull()
    {
        boolean isTotalSendMsgNumbGreaterMax = this.frameCounterSendMsg >= this.MAX_MES_NUMBER_PER_TOPIC.intValue();
        try {// 1.
            if (isTotalSendMsgNumbGreaterMax)
            {
                Thread.sleep(this.sleeptMs);// 2.

                Long lSz = countTopicSize();// 3.
                boolean isTopicSizeGreaterHalf = lSz.compareTo(this.HALF_MES_NUMBER_PER_TOPIC) >= 0;
                if (!isTopicSizeGreaterHalf)// 4.
                    this.frameCounterSendMsg = lSz.intValue();
            }
        } catch (KafkaException | InterruptedException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
        return isTotalSendMsgNumbGreaterMax;
    }
}
