package producer;

import consumer.ConsumerInfo;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.time.Duration;
import java.util.*;

import org.opencv.core.Mat;
import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import templates.AdditionalParamsInfo;
import templates.KafkaTemplateThread;
import transfer.FramePkg;

import java.time.Instant;

public class KafkaVideoProducer extends KafkaTemplateThread {
    /* input errors */
    /* end */
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    private AdditionalParamsInfo additionalParamsInfo;
    private ProducerInfo producerInfo;
    private ConsumerInfo consumerInfo;

    private String videoPath = "";                   // image IP-camera source (Not used now)

    private KafkaConsumer consumerTopicChecker;//used this for a kind of renaming

    private Long MAX_MES_NUMBER_PER_TOPIC = new Long(this.topicMesLimit);               // the same as `topicMesLimit`, but type<Long>
    private Long HALF_MES_NUMBER_PER_TOPIC = new Long((int) (this.topicMesLimit/2) );   // the half of `topicMesLimit`
    private int frameCounterSendMsg = 0;
    private int maxEmptyFramesNumb = 0;

    private void setProducerProps(ProducerInfo info) throws IOException {
        initProducerProperties(info);
        this.propsProducer.put("key.serializer", StringSerializer.class.getName());
        this.propsProducer.put("value.serializer", transfer.FramePkgSerializer.class.getName());

        // maxEmptyFramesNumb
        if (info.fprops.containsKey("maxEmptyFramesNumb"))
            this.maxEmptyFramesNumb = Integer.parseInt(info.fprops.getProperty("maxEmptyFramesNumb"));
        else
            throw new IOException(ERROR_maxEmptyFramesNumb_IS_NOT_SET);

    }

    /* Initialization of kafka consumer (Topic size checker) properties
     *   Input:
     *       ProducerInfo consumerInfo: user settings for consumer
     *   Return: Properties: kafka properties class
     * */
    private void setConsumerTopicCheckerProps(ConsumerInfo info) throws IOException {
        //this.propsConsumer = new Properties();
        initConsumerProperties(info);
        this.propsConsumer.put("key.deserializer", StringDeserializer.class.getName());
        this.propsConsumer.put("value.deserializer", transfer.FramePkgDeserializer.class.getName());

    }

    /* Private members setter and initializer and launch */
    public KafkaVideoProducer(String videoFpath, ProducerInfo producerInfo, ConsumerInfo consumerInfo, AdditionalParamsInfo additionalParamsInfo)
    {
        try {
            logger = LogManager.getLogger(KafkaVideoProducer.class);
            logger.trace(MSG_CREATE_PROD);

            this.additionalParamsInfo = additionalParamsInfo;
            this.producerInfo = producerInfo;
            this.consumerInfo = consumerInfo;

            // set class variables
            this.videoPath = videoFpath!="" ? videoFpath : null;
            if (videoPath == null)
                throw new Exception(ERROR_VIDEO_SOURCE_UNSPECIFIED);
            this.topicName = producerInfo.topicName;
            this.sleeptMs = consumerInfo.sleeptMs;

            // initialize producer for sending img-messages to topic
            setProducerProps(producerInfo);
            this.producer = new KafkaProducer<String, FramePkg>(this.propsProducer);

            // Initialize consumer for checking number of messages inside the topic
            setConsumerTopicCheckerProps(consumerInfo);
            this.consumer = new KafkaConsumer<String, FramePkg>(this.propsConsumer);
            this.consumerTopicChecker = this.consumer;
            this.consumerTopicChecker.subscribe(Collections.singletonList(this.consumerInfo.topicName));

            //
            this.additionalParamsInfo = additionalParamsInfo;

            logger.info(MSG_START_PROD + this);
        } catch (Exception e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        } finally {
            start();
        }
    }

    /*
    * Pkg sender with sending structuring
    *   Input:
    *           Mat img: sending image
    *           int frameId: image number (from counter)
    *           long timestamp: reading time
    * */
    public void sendFramePkg(Mat img, int frameId, long timestamp, long fps_time_ms)
    {
        logger.debug(MSG_START_CREATE_FRAMEPKG);
        FramePkg framePkg = new FramePkg(this.videoPath, img,
                img.rows(), img.cols(), img.channels(), img.type(),
                frameId, timestamp, fps_time_ms);
        logger.debug(MSG_END_CREATE_FRAMEPKG);

        logger.debug(MSG_START_SEND_MSG);
        try {
            this.producer.send(
                    new ProducerRecord<String, FramePkg>(this.producerInfo.topicName, framePkg),
                    new EventGeneratorCallback(this.videoPath)
            );
        } catch (KafkaException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
    }

    /*
     * Processing thread:
     *   0. read image from topic
     *   1. fetch data and commit
     *   2. send to topic
     *    . control: source is opened
     *   4. control: number of empty inputs
     *   3. control: topic size
     * */
    public void run()
    {
        // Load native library
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        logger.info(MSG_START_PROD_RUN + this.producerInfo.topicName);

        Mat matOrig = new Mat();
        VideoCapture capture = new VideoCapture(this.videoPath);
        //DrawFrameDebug drawFrameDebug = new DrawFrameDebug();

        try{
            int empty_frameCounter = 0;
            if( capture.isOpened())
            {
                while (true)
                {
                    if (!isTopicFull())
                    {
                        capture.read(matOrig);
                        boolean isClsd = capture.isOpened();

                        // get some meta data about frame.
                        double fps = capture.get(Videoio.CAP_PROP_FPS);//5
                        double frameCount = capture.get(Videoio.CAP_PROP_FRAME_COUNT);//7
                        double h = capture.get(Videoio.CAP_PROP_FRAME_HEIGHT);//4
                        double w = capture.get(Videoio.CAP_PROP_FRAME_WIDTH);//3
                        double posFrames = capture.get(Videoio.CAP_PROP_POS_FRAMES);//0
                        double posMsec = capture.get(Videoio.CAP_PROP_POS_MSEC);//1
                        double speed = capture.get(Videoio.CAP_PROP_SPEED);//30
                        logger.info(String.format(MSG_READED_FRAME_INFO, fps, frameCount, h, w, posFrames, posMsec, speed ) );

                        if( !matOrig.empty() )
                        {// send non-empty image
                            // resize image for test purposes
                            Mat resizeimage = additionalParamsInfo.resizeMat(matOrig);
                            logger.info(String.format(MSG_RESEIZED_IMG_INFO, resizeimage.height(), resizeimage.width() ) );

                            long timestamp = Instant.now().getEpochSecond();
                            sendFramePkg(resizeimage, (int) posFrames, (long) timestamp, (long) Math.round(posFrames/fps*1000));
                            this.frameCounterSendMsg++;

                            // Draw frame: debug//
                            //drawFrameDebug.display(resizeimage);

                            // reset & continue receive frames
                            empty_frameCounter = 0;
                        } else {
                            // if number of empty frame is greater than maximum
                            if (empty_frameCounter > this.maxEmptyFramesNumb) {
                                logger.info(ERROR_PROD_TERMINATED_TOOMUCH_EMPTY_FRAMES);
                                break;
                            } else {
                                empty_frameCounter++;
                                logger.warn(String.format(WARN_EMPTY_RECORD_SRC_FRAME_COUNTER, empty_frameCounter, this.maxEmptyFramesNumb) );
                            }
                        }
                    }// end !isTopicFull()
                }// end while
            } else {
                throw new Exception(ERROR_CAPTURED_SRC_CLOSED);
            }
        } catch (KafkaException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
            System.exit(-1);
        } catch (Exception e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
            System.exit(-1);
        } finally {
            logger.info(MSG_END_PROD_RUN);
            this.producer.close();
            //drawFrameDebug.dispose();
        }

    }

    private Long countTopicSize()
    {
        Long totalCount = new Long(0);
        try {
            this.consumerTopicChecker.poll(Duration.ofMillis(1000));
            Set<TopicPartition> assignment = this.consumerTopicChecker.assignment();

            final Map<TopicPartition, Long> endOffsets = this.consumerTopicChecker.endOffsets(assignment);
            final Map<TopicPartition, Long> beginningOffsets = this.consumerTopicChecker.beginningOffsets(assignment);

            assert (endOffsets.size() == beginningOffsets.size());
            assert (endOffsets.keySet().equals(beginningOffsets.keySet()));

            totalCount = beginningOffsets.entrySet().stream().mapToLong(entry -> {
                TopicPartition tp = entry.getKey();
                Long beginningOffset = entry.getValue();
                Long endOffset = endOffsets.get(tp);
                return endOffset - beginningOffset;
            }).sum();
        } catch (KafkaException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
        return totalCount;
    }

    /*
    * Method is checking the fullness of the topic queue
    * based on max-parameters:
    * 1. check if total number of messages greater `MAX_MES_NUMBER_PER_TOPIC`
    *       State `Full`: topic is fulled by producer
    * 2. When Full-state is active: sleep for waiting messages will be deleted by time or received
    *       State `Sleep`
    * 3.    State `Check Size`
    * 4. If size After sleep is less `HALF_MES_NUMBER_PER_TOPIC`, than reset `frameCounter`,
    *       State `Ready-to-Send-Again`
    * */
    private boolean isTopicFull()
    {
        boolean isTotalSendMsgNumbGreaterMax = this.frameCounterSendMsg >= this.MAX_MES_NUMBER_PER_TOPIC.intValue();
        try {// 1.
            if (isTotalSendMsgNumbGreaterMax)
            {
                Thread.sleep(this.consumerInfo.sleeptMs);// 2.

                Long lSz = countTopicSize();// 3.
                boolean isTopicSizeGreaterHalf = lSz.compareTo(this.HALF_MES_NUMBER_PER_TOPIC) >= 0;
                if (!isTopicSizeGreaterHalf)// 4.
                    this.frameCounterSendMsg = lSz.intValue();
            }
        } catch (KafkaException | InterruptedException e) {
            logger.error(ERROR_CHECK_LOG_DETAILS+", {}, {}", e.getMessage(), e.getMessage());
        }
        return isTotalSendMsgNumbGreaterMax;
    }

    private BufferedImage Mat2BufferedImage(Mat m)
    {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1)
        {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }
}
