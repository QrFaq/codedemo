package producer;

import templates.Info;

import java.io.IOException;
import java.util.Properties;

/*
 * Producer settings class used for
 * creating consumer afterwards
 * Default values: see ProducerInfoBuilder.java
 * */
public class ProducerInfo extends Info {
    /* input errors */
    protected final String ERROR_camIp_or_videoFpath_IS_NOT_SET = "pls, set ProducerInfo `camIp` or `videoFpath` property inside `producer.properties` configuration file depends on your task";
    protected final String ERROR_serverIp_IS_NOT_SET = "pls, set producerInfo `bootstrap.ip` property inside `producer.properties` configuration file , used as server-ip for connection";
    protected final String ERROR_topicMesLimit_SOFTWARE_PROPERTIES_NOT_SET = "pls, set `topicMesLimit`, even to default example value: 30";
    protected final String ERROR_maxEmptyFramesNumb_SOFTWARE_PROPERTIES_NOT_SET = "pls, set `maxEmptyFramesNumb`, even to default example value: 30";
    protected final String ERROR_topicName_IS_NOT_SET = "pls, set producer `topicName` property inside `producer.properties` file";
    /* end */

    public String  id,                 // Producer id
            serverIp,                  // ip where the topic is
            topicName                  // name of the topic which will be read for new messages
                    ;

    public Properties fprops;

    public ProducerInfo(String fpathProperties) throws IOException {
        this.fprops = readPropertiesFile(fpathProperties);

        if (this.fprops.containsKey("client.id"))
            this.id = this.fprops.getProperty("client.id");
        else
            throw new IOException(ERROR_camIp_or_videoFpath_IS_NOT_SET);

        /*if (this.fprops.containsKey("camIp"))
            this.camIp = this.fprops.getProperty("camIp");
        else if(this.fprops.containsKey("videoFpath"))
            this.videoFpath = this.fprops.getProperty("videoFpath");
        else
            throw new IOException(ERROR_camIp_or_videoFpath_IS_NOT_SET);*/

        if (this.fprops.containsKey("bootstrap.servers"))
            this.serverIp = this.fprops.getProperty("bootstrap.servers");
        else
            throw new IOException(ERROR_serverIp_IS_NOT_SET);

        if (this.fprops.containsKey("topicName"))
            this.topicName = this.fprops.getProperty("topicName");
        else
            throw new IOException(ERROR_topicName_IS_NOT_SET);

        //this.fpathProperties = fpathProperties;
    }
}
