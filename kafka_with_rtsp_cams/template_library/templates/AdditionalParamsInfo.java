package templates;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.Properties;

/**
 * Class used as template for storing methods/params/etc which is not dependent on Kafka
 * (for now there is methods for easier working with cv::Mat images)
 * **/

public class AdditionalParamsInfo extends Info {
    /* input errors */
    protected final String ERROR_INPUT_WH_OR_SCALEFACTOR_OR_BOTH_IS_NOT_SET = "pls, set one of the possible combinations" +
            "of parameters: (1) scaleFactorDivisor (2) newW, newH (3) all of them in `.properties` configuration file " +
            "depends on your task";
    /* end */
    public double scaleFactorDivisor = -1.0;
    public double newW = -1.0;
    public double newH = -1.0;

    public AdditionalParamsInfo(){ }

    public AdditionalParamsInfo(String propertiesPath) throws IOException {
        Properties fprops = readPropertiesFile(propertiesPath);

        boolean isExist_scaleFactorDivisor = fprops.containsKey("scaleFactorDivisor");
        boolean isExist_newW = fprops.containsKey("newW");
        boolean isExist_newH = fprops.containsKey("newH");

        if (isExist_scaleFactorDivisor && isExist_newW && isExist_newH) {
            this.scaleFactorDivisor = Double.parseDouble(fprops.getProperty("scaleFactorDivisor"));
            this.newW = Double.parseDouble(fprops.getProperty("newW"));
            this.newH = Double.parseDouble(fprops.getProperty("newH"));}
        else if (isExist_scaleFactorDivisor) {
            this.scaleFactorDivisor = Double.parseDouble(fprops.getProperty("scaleFactorDivisor"));
        }
        else if (isExist_newW && isExist_newH) {
            this.newW = Double.parseDouble(fprops.getProperty("newW"));
            this.newH = Double.parseDouble(fprops.getProperty("newH"));
        } else {
            throw new IOException(ERROR_INPUT_WH_OR_SCALEFACTOR_OR_BOTH_IS_NOT_SET);
        }
    }

    public AdditionalParamsInfo(double scaleFactorDivisor)
    {
        this.scaleFactorDivisor = scaleFactorDivisor;
    }

    public AdditionalParamsInfo(double newW, double newH)
    {
        this.newW = newW;
        this.newH = newH;
    }
    public AdditionalParamsInfo(double scaleFactorDivisor, double newW, double newH)
    {
        this.scaleFactorDivisor = scaleFactorDivisor;
        this.newW = newW;
        this.newH = newH;
    }

    /* resize value by `scaleFactorDivisor` */
    private double resize(double v)
    {
        double res = Math.round(v/this.scaleFactorDivisor);
        return (res < 5.0 ? 5.0 : res);
    }
    /* Resize method image
    *  Scenarios:
    * A. resize image by selecting needed `newW`, `newH` and applying resize for this values by `scaleFactorDevisor`
    * B. resize image to the selected size, defined by `newW`, `newH`
    * C. resize image by applying to the current image width and height `scaleFactorDevisor`
    * [WARNING]: In worst case: image will be the same size OR will be scaled with `scaleFatorDivisor`
    * */
    public Mat resizeMat(Mat img)
    {
        Mat resImg = new Mat();
        try {
            if (!img.empty()) {
                boolean isNewSz = this.newH != img.rows() && this.newW != img.cols();
                // set by newW, newH, factor
                if (this.newH > 1 && this.newW > 1 && this.scaleFactorDivisor > 0 && isNewSz)
                    Imgproc.resize(img, resImg, new Size(resize(newW), resize(newH)));
                else {
                    if (this.newH > 1 && this.newW > 1 && isNewSz) {
                        // set by newW, newH
                        Imgproc.resize(img, resImg, new Size(newW, newH));
                    } else {
                        if (this.scaleFactorDivisor > 0 && this.scaleFactorDivisor != 1) {
                            // set by factor
                            Imgproc.resize(img, resImg, new Size(resize(img.cols()), resize(img.rows())));
                        } else {//if (this.newH <= 0 && this.newW <= 0 && this.scaleFactorDivisor <= 0)
                            // return input Image
                            resImg = img;
                        }
                    }
                }
            }
        } catch (Exception e)
        {
            throw e;
        }
        return resImg;
    }

    /*public Mat resizeByScaleFactor(Mat img, double newW, double newH, boolean withScaleFactor)
    {
        Mat resImg = new Mat();
        if(!img.empty() && newW>0 && newW>0)
        {
            if (withScaleFactor)
                Imgproc.resize(img, resImg, new Size(resize(newW),resize(newH)));//resImg = resizeMatWithFdivisor(img, newW, newH);
            else {
                if (img.cols() == newW && img.rows() == newH)
                    resImg = img;
                else
                    Imgproc.resize(img, resImg, new Size(newW,newH));
            }
        }
        return resImg;
    }*/
}
