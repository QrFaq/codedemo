package templates;

import consumer.ConsumerInfo;
import producer.ProducerInfo;

/**
 * Class for simplifiend argument passing if information about producer and consumer is needed
 * **/

public class ConsumerProducerInfo {
    public ProducerInfo producerInfo;
    public ConsumerInfo consumerInfo;
    public ConsumerProducerInfo(ProducerInfo producerInfo, ConsumerInfo consumerInfo)
    {
        this.producerInfo = producerInfo;
        this.consumerInfo = consumerInfo;
    }
}
