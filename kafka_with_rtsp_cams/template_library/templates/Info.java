package templates;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Information template for sharing property read method for now
 * **/

public class Info {
    protected Properties readPropertiesFile(String fpath) throws IOException {
        InputStream stream = new FileInputStream(fpath);//Properties.class.getResourceAsStream(fpath);
        Properties properties = new Properties();
        properties.load(stream);
        stream.close();
        return properties;
    }

}
