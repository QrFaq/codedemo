package templates;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.logging.log4j.Logger;
import consumer.ConsumerInfo;
import producer.ProducerInfo;
import transfer.FramePkg;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class KafkaTemplateThread extends Thread {
    /* Class Logger-messages */
    protected final String MSG_CALLBACK_CAM_PART = "cameraId={} partition={}";
    protected final String MSG_TRY_SET_ZERO_TOPIC_OFFSET = "trying to set topic offset to zero";
    protected final String MSG_START_SEND_MSG = "Start sending messages...";
    protected final String ERROR_CHECK_LOG_DETAILS = "Exception occurred – Check log for more details.\n";

    /* consumer */
    protected final String MSG_START_CONS = "Start consumer ";
    protected final String MSG_START_CREATE_CHPKG = "Start Create ChannelsPkg...";
    protected final String MSG_END_CREATE_CHPKG = "End Create ChannelsPkg...";
    protected final String MSG_RECEIVED = "Message received ";
    protected final String MSG_START_CONS_RUN = "Start consumer.run ";
    protected final String MSG_END_CONS_RUN = "End consumer.run ";

    protected final String ERROR_CANT_PROC_PKG = "Cant process polled pkg, error";
    protected final String ERROR_CANT_PROC_FULL_NONEMPTY_PKG = "Cant process non-empty full polled pkg";
    protected final String ERROR_CONS_TERMINATE_NO_DATA_LONGER_S = "Cosumer is terminated, cause: no data in topic more than %d%n [s], Settings: sleept_ms=%d%n, empty_record_poll_limit=%df\n";

    /* producer */
    protected final String MSG_CREATE_PROD = "Creating Kafka Producer...";
    protected final String MSG_START_PROD = "Start producer ";
    protected final String MSG_START_CREATE_FRAMEPKG = "Start Create FramePkg...";
    protected final String MSG_END_CREATE_FRAMEPKG = "End Create FramePkg...";
    protected final String MSG_START_PROD_RUN = "Starting KafkaVideoProducer.run(...), topicName=";
    protected final String MSG_RESEIZED_IMG_INFO = "Readed resizeimage info: h:%d, w:%d";
    protected final String MSG_READED_FRAME_INFO="Readed frame info: fps=%.0f, frame_n=%.0f, sz(h:%.0f, w:%.0f), posFrames=%.0f, posMsec=%f, speed=%f,";
    protected final String MSG_END_PROD_RUN = "End producer.run ";

    protected final String ERROR_PROD_TERMINATED_TOOMUCH_EMPTY_FRAMES = "Producer is terminated, cause: number of empty frames is greater than maximum\n";
    protected final String ERROR_CAPTURED_SRC_CLOSED = "Captured source is closed";
    protected final String ERROR_VIDEO_SOURCE_UNSPECIFIED = "video source is empty (-v parameter or -i unspecified)";
    protected final String ERROR_NO_CAMERAS_RETREIVED_FROM_IP_CAM_TABLE = "Hash map of k:`ip camera link`, v:`folder to save images` is empty";

    /* other */
    protected final String MSG_CREATE_KAFKA_PRODUCER = "Creating Kafka Producer, topicName=";
    protected final String MSG_START_KAFKA_PRODUCER = "Start producer ";
    protected final String WARN_EMPTY_RECORD_POLL_COUNTER = "Empty input count for stopping the process: %d/%d ";
    protected final String WARN_EMPTY_RECORD_SRC_FRAME_COUNTER = "Empty source frame counter for stopping the process";
    protected final String WARN_NO_CONNCETION_TO_IP_CAM_RECONNECTING = "no connection with ip-camera, will try to reconnect after some delay..";

    /* input errors */
    protected final String ERROR_TOPIC_NAME_KAFKA_PROPERTIES_NOT_SET = "pls, set `TOPIC_NAME` property inside `.properties` configuration file";

    protected final String ERROR_topicMesLimit_SOFTWARE_PROPERTIES_NOT_SET = "pls, set `topicMesLimit`, even to default example value: 30";
    protected final String ERROR_maxEmptyFramesNumb_SOFTWARE_PROPERTIES_NOT_SET = "pls, set `maxEmptyFramesNumb`, even to default example value: 30";

    protected final String ERROR_sleeptMs_CONSUMER_PROPERTY_NOT_SET = "pls, set `sleeptMs`, even to default example value: 5000";
    protected final String ERROR_emptyRecordPollLimit_CONSUMER_PROPERTY_NOT_SET = "pls, set `emptyRecordPollLimit`, even to default example value: 5";
    protected final String ERROR_startDelayMs_CONSUMER_PROPERTY_NOT_SET = "pls, set `startDelayMs`, even to default example value: 1000";
    protected final String ERROR_checkTopicZeroPos_CONSUMER_PROPERTY_NOT_SET = "pls, set `checkTopicZeroPos`, even to default example value: false";
    protected final String ERROR_maxEmptyFramesNumb_IS_NOT_SET = "pls, set producer `maxEmptyFramesNumb` property inside `producer.properties` file";
    protected final String ERROR_maxNumberToSkip_IS_NOT_SET = "pls, set producer `maxNumberToSkip` property inside `producer.properties` file";
    /* end */

    protected Logger logger;

    protected Properties propsProducer, propsConsumer;                // kafka consumer properties
    protected String topicName= "";                                   // topic name to subscribe

    protected KafkaProducer producer;
    protected int topicMesLimit = 30;                                 // maximum number of messages in topic for producer pushing
    protected int maxEmptyFramesNumb = 30;                            // thread empty source images limit

    protected KafkaConsumer consumer;
    protected long sleeptMs = 5000;                                   // thread delay time between retries to poll when data wasnt read
    protected int emptyRecordPollLimit = 5;                           // thread empty poll retries limit
    protected int startDelayMs = 1000;                                // thread start delay
    protected boolean checkTopicZeroPos = false;                      // thread flag for resetting topic offset

    /* Initialization of kafka producer properties
     *   Input:
     *       ProducerInfo producerInfo: user settings for producer
     *   Return: Properties: kafka properties class
     * */
    // [WARNING]: Serializer shall be set in child class `propsProducer`
    protected void initProducerProperties(ProducerInfo info) throws IOException
    {
        this.propsProducer = info.fprops;

        if (this.propsProducer.containsKey("topicName"))
            this.topicName = this.propsProducer.getProperty("topicName");
        else
            throw new IOException(ERROR_TOPIC_NAME_KAFKA_PROPERTIES_NOT_SET);

        if (this.propsProducer.containsKey("topicMesLimit"))
            this.topicMesLimit = Integer.parseInt(this.propsProducer.getProperty("topicMesLimit"));
        else
            throw new IOException(ERROR_topicMesLimit_SOFTWARE_PROPERTIES_NOT_SET);

        if (this.propsProducer.containsKey("maxEmptyFramesNumb"))
            this.maxEmptyFramesNumb = Integer.parseInt(this.propsProducer.getProperty("maxEmptyFramesNumb"));
        else
            throw new IOException(ERROR_maxEmptyFramesNumb_SOFTWARE_PROPERTIES_NOT_SET);

        /*this.propsProducer.put(ProducerConfig.CLIENT_ID_CONFIG, info.id);
        this.propsProducer.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, info.serverIp);
        this.propsProducer.put(ProducerConfig.BATCH_SIZE_CONFIG, info.batchSz);
        this.propsProducer.put(ProducerConfig.LINGER_MS_CONFIG, info.lingerMs);
        this.propsProducer.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, info.compression);
        this.propsProducer.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, info.maxRequestSz);*/
    }

    // [WARNING]: Deserializer shall be set in child class for `propsConsumer`
    protected void initConsumerProperties(ConsumerInfo info) throws IOException {
        this.propsConsumer = info.fprops;

        if (this.propsConsumer.containsKey("sleeptMs"))
            this.sleeptMs = Long.parseLong(this.propsConsumer.getProperty("sleeptMs"));
        else
            throw new IOException(ERROR_sleeptMs_CONSUMER_PROPERTY_NOT_SET);

        if (this.propsConsumer.containsKey("emptyRecordPollLimit"))
            this.emptyRecordPollLimit = Integer.parseInt(this.propsConsumer.getProperty("emptyRecordPollLimit"));
        else
            throw new IOException(ERROR_emptyRecordPollLimit_CONSUMER_PROPERTY_NOT_SET);

        if (this.propsConsumer.containsKey("startDelayMs"))
            this.startDelayMs = Integer.parseInt(this.propsConsumer.getProperty("startDelayMs"));
        else
            throw new IOException(ERROR_startDelayMs_CONSUMER_PROPERTY_NOT_SET);

        if (this.propsConsumer.containsKey("checkTopicZeroPos"))
            this.checkTopicZeroPos = Boolean.parseBoolean(this.propsConsumer.getProperty("checkTopicZeroPos"));
        else
            throw new IOException(ERROR_checkTopicZeroPos_CONSUMER_PROPERTY_NOT_SET);

        /*this.propsConsumer = new Properties();
        this.propsConsumer.put("bootstrap.servers", consumerInfo.serverIp);
        this.propsConsumer.put("group.id", consumerInfo.groupId);
        this.propsConsumer.put("max.poll.records", consumerInfo.maxPollRecords);
        this.propsConsumer.put("session.timeout.ms", consumerInfo.sessionTimeoutMs);
        this.propsConsumer.put("enable.auto.commit", "false");
        //props.put("enable.auto.commit", "true");props.put("auto.commit.interval.ms", "1000");*/
    }

    public void run() { }

    public class EventGeneratorCallback implements Callback {
        private String camId;

        public EventGeneratorCallback(String camId) {
            super();
            this.camId = camId;
        }

        @Override
        public void onCompletion(RecordMetadata rm, Exception e) {
            if (rm != null) {
                logger.info(MSG_CALLBACK_CAM_PART, camId, rm.partition());
            }
            if (e != null) {
                logger.info(MSG_CALLBACK_CAM_PART+", {}, {}", e.getMessage(), e.getMessage());
            }
        }
    }

    public void setOffset2zero()//TODO: doesn't work always cause of Kafka-source-bug
    {
        while(true)
        {
            // pull and set offset to zero
            logger.info(MSG_TRY_SET_ZERO_TOPIC_OFFSET);
            this.consumer.poll(Duration.ofMillis(1000));
            Set<TopicPartition> partitions = this.consumer.assignment();
            this.consumer.seekToEnd(partitions);
            this.consumer.seekToBeginning(partitions);

            // check that at the all partitions its in the begining
            Long offset = new Long(0);
            ArrayList<Long> endOffsets = new ArrayList<Long>();
            for (TopicPartition part : partitions) {
                offset = this.consumer.position(part);
                endOffsets.add(offset);
            }
            if (endOffsets.size()>0) {
                boolean allEqual = new HashSet<Long>(endOffsets).size() <= 1;
                if (allEqual && endOffsets.get(0) == 0)
                    break;
            }
        }

    }

}
