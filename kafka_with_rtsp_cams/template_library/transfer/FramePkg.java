package transfer;

//import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.opencv.core.Mat;

/**
 * Class used as kafka message unit to store
 * received images from cameras in topic
 * **/

//@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class FramePkg {
    private String _srcId;          // rtsp camera ip/Full path to the video
    private byte[] _img;            // image array
    private int _rows;              // height
    private int _cols;              // width
    private int _nchannels;         // picture number of channels
    private int _type;              // opencv image type
    private int _frameId;           // most of the time it is a frame number
    private long _timestamp;        // calendar time of creating packge
    private long _fps_time_ms;      // frame-time based on camera-fps (if it wsa extracted by opencv)

    public FramePkg() { }

    public FramePkg(String srcId, byte[] img,
                    int rows,int cols,int nchannels,
                    int type, int frameId, long timestamp,
                    long fps_time_ms)
    {
        this._srcId = srcId;
        this._img = img;
        this._rows = rows;
        this._cols = cols;
        this._type = type;
        this._nchannels = nchannels;
        this._frameId = frameId;
        this._timestamp = timestamp;
        this._fps_time_ms = fps_time_ms;
    }
    public FramePkg(String srcId, Mat img,
                    int rows,int cols, int nchannels,
                    int type, int frameId, long timestamp,
                    long fps_time_ms)
    {
        this._srcId = srcId;
        this._img = new byte[rows * cols * nchannels];
        img.get(0, 0, this._img);
        this._rows = rows;
        this._cols = cols;
        this._type = type;
        this._nchannels = nchannels;
        this._frameId = frameId;
        this._timestamp = timestamp;
        this._fps_time_ms = fps_time_ms;
    }

    public String get_srcId() {
        return this._srcId;
    }
    public byte[] get_img() {
        return this._img;
    }
    public Mat byte2Mat() {
        Mat mat = new Mat(this._rows, this._cols, this._type);
        mat.put(0, 0, this._img);
        return mat;
    }
    public int get_rows() {
        return this._rows;
    }
    public int get_cols() {
        return this._cols;
    }
    public int get_nchannels(){
        return this._nchannels;
    }
    public int get_type() {
        return this._type;
    }
    public int get_frameId() {
        return this._frameId;
    }
    public long get_timestamp() {
        return this._timestamp;
    }
    public long get_fps_time_ms() {
        return this._fps_time_ms;
    }

    /*public void setToCameraIp(String cameraIp) { this._cameraIp = cameraIp; }
    public void setToImg(byte[] img) { this._img = img; }
    public void setToRows(int rows) { this._rows = rows; }
    public void setToCols(int cols) { this._cols = cols; }
    public void setToType(int type) { this._type = type; }
    public void setToFrameId(int frameId) { this._frameId = frameId; }
    public void setToTimestamp(long timestamp) { this._timestamp = timestamp; }*/

    @Override
    public String toString()
    {
        return "FramePkg(" +
                this._srcId + ", " +
                "<img byte[] skipped>" + ", " +
                this._rows + ", " + this._cols + ", " + this._nchannels + ", " +
                this._type + ", " + this._frameId + ", " +
                this._fps_time_ms + ")";
    }
}
