package transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class FramePkgDeserializer implements Deserializer<FramePkg> {
    private final Logger logger = LogManager.getLogger(FramePkgDeserializer.class);

    @Override
    public void configure(Map configs, boolean isKey) { }

    @Override
    public FramePkg deserialize(String s, byte[] bytes)
    {
        ObjectMapper mapper = new ObjectMapper();
        FramePkg framePkg = null;
        try {
            framePkg = mapper.readValue(bytes, FramePkg.class);
        } catch (Exception e) {
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
        return framePkg;
    }

    @Override
    public void close() { }
}
