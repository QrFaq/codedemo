package transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class FramePkgSerializer implements Serializer<FramePkg> {
    private final Logger logger = LogManager.getLogger(FramePkgSerializer.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) { }

    @Override
    public byte[] serialize(String topic, FramePkg data)
    {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            retVal = objectMapper.writeValueAsBytes(data);
        } catch (Exception e){
            logger.error("{}, {}",e.getMessage(), e.getStackTrace());
        }
        return retVal;
    }

    @Override
    public void close() { }
}
