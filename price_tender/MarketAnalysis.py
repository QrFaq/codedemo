"""
        Legend:
S - market number
I - inventory number
D - day number
N - number of sailed inventory

1. if S==nan, I==nan, D==nan ==> N=0
2. S<= 10k, I <= 50k, t=3 years==365 * 3
3. Асортимент [A]: {S,I,D} in A, if D in [D1, D2] for {S, I}

Prognose: sum(N<A>) for {S,I} for <i> Week of i=0,..,n (week line, n=4)


# Исходим из того, что в БД есть таблицы `sales` с историческими данными о продажах (колонки S, I, D, N) и `products` (колонки S, I, D1, D2), где
# * S - название магазина
# * I - категория товара
# * D - дата продажи товара
# * N - количество проданного товара
# * D1 - дата ввода товара в ассортимент
# * D2 - дата вывода товара из ассортимента

"""

class MarketAnalysis:
    def __init__(self):
        self.db_config = {
            'user': 'my_user',
            'pwd': 'my_user_password',
            'host': 'localhost',
            'port': 5432,
            'db': 'store'
        }

        # analyzied dataframe, global
        self.df = None

        # number of store clusters, global
        self.nclusters = 15

        # Clusterization work-around
        self.km = None
        self.labels = None

        # data set splitting
        self.df_train = None
        self.df_test = None
        
        # Prognosing model list
        self.model_list = []

    def db_connect(self):
        connection_string = 'postgresql://{}:{}@{}:{}/{}'.format(
            self.db_config['user'],
            self.db_config['pwd'],
            self.db_config['host'],
            self.db_config['port'],
            self.db_config['db']
        )
        self.engine = create_engine(connection_string)

    def db_cook_csv(self, fpath="test_ds.tsv", sep='\t' ):
        """
            Cook database from: https://www.kaggle.com/saitosean/mercari
            
            Set class global variable
            name: df, type: pandas_dataframe
        """
        def gen_item():
            '''
            функция для выбора случайной категории товара
            '''
            i = np.random.randint(0, 3900)
            return items[i]

        self.df = pd.read_csv(
            'test_ds.tsv',
            sep='\t'
        )
        self.df = self.df.rename(
            columns={'category_name': 'S', 'brand_name': 'I', 'shipping': 'N'}
        )
        self.df = self.df[ ['S', 'I', 'N'] ]

        self.df['D'] = 0
        self.df['D'] = self.df['D'].apply(
            lambda x: self.gen_datetime(min_year=2018, max_year=datetime.now().year)
        )
        items = self.df.I.unique()

        self.df['I'] = self.df['I'].apply(lambda x: gen_item()).fillna('B Darlin')
        self.df['S'] = self.df['S'].fillna('Men/Sweaters')
        self.df['N'] = self.df['N'].apply(lambda x: np.random.randint(0, 1000))

        # data encoding
        encoder =  LabelEncoder()

        # кодировка категориальных данных
        self.df['S'] = encoder.fit_transform(df['S'])
        self.df['I'] = encoder.fit_transform(df['I'])

        # выделение недели и года в выборке
        self.df.loc[:, 'W'] = pd.to_datetime(df.loc[:, 'D']).dt.week
        self.df.loc[:, 'Y'] = pd.to_datetime(df.loc[:, 'D']).dt.year


    def data_preprocessing(self, df_train):
        # подсчет количества каждой категории
        # проданного товара в каждом магазине
        # за каждую неделю
        self.df_train = self.df_train.groupby(
            ['cluster', 'Y', 'W', 'S', 'I']).agg({'N': 'sum'}).reset_index()
        self.df_test = self.df_test.groupby(
            ['cluster', 'Y', 'W', 'S', 'I']).agg({'N': 'sum'}).reset_index()
        
        self.df = self.add_week_chain_columns(self.df, nweeks=4)


        #features_train = self.df_train.drop(labels=['N'], axis=1)
        #features_test = self.df_test.drop(labels=['N'], axis=1)
        #target_train = df_train['N']
        #target_test = df_test['N']

    def train_ridgereg(self, ):
        """
            Method fills class <model_list>
            by analysis models
        """
        ctrain = {
            0:["W", "S", "I", 'cluster'],
            1:["S", "I", 'cluster'],
            2:["S", "I", 'cluster'],
            3:["S", "I", 'cluster']
        }
        caddons = {
            0:[],                   # predict N
            1:["N"],                # predict N_1|N
            2:["N", "N_1"],         # predict N_2|N,N_1
            3:["N", "N_1","N_2"]    # predict N_3|N,N_1,N_2
        }
        ctarget = {
            0:["N"],    #polynomial
            1:["N_1"],
            2:["N_2"],
            3:["N_3"]
        }
        for i in range(0, nweeks):
            self.model_list.append( Ridge() )
            self.model_list[i].fit(
                self.df[ ctrain[i] + caddons[i] ].to_numpy(),
                self.df[ ctarget[i] ].to_numpy()
            )

    def predict_row_ridgereg(self, row=None):
        """
            Input:
            row: np.array (?) or pandas (?)
                row=[ W, S, I ]
        """
        def get_cluster(w,s,i):
            clustn = None
            return clustn

        W, S, I = row
        cluster = get_cluster(W, S, I)
        res_storage = []
        rdata = [
            ,
            [S, I, cluster],
            [S, I, cluster],
            [S, I, cluster]
        ]
            1:["N"],                # predict N_1|N
            2:["N", "N_1"],         # predict N_2|N,N_1
            3:["N", "N_1","N_2"]    # predict N_3|N,N_1,N_2

        N = self.model_list[0].predict( [[W, S, I, cluster]] )
        N_1 = self.model_list[0].predict( [[W, S, I, cluster, N]] )
        N_2 = self.model_list[0].predict( [[W, S, I, cluster, N_1]] )
        N_3 = self.model_list[0].predict( [[W, S, I, cluster, N_1, N_2]] )

        return np.array( [N, N_1, N_2, N_3] ) 
    
    def mae_error(self, target_test, predictions):
        mae = mean_absolute_error(target_test, predictions)


    """
        Data preprocessing methods
    """
    def split_dataset():
        """
        # деление данных на обучающую и тестовую выборку
        # ytnс учетом временного ряда
        tss = TimeSeriesSplit(n_splits = 3)
        for train_index, test_index in tss.split(df):
            df_train, df_test = df.iloc[train_index, :], df.iloc[test_index,:]
        """
        self.df_train = None
        self.df_test = None


    def data_clustering(self, USEDEFAULT=True):
        """
            0. Merge stores into groups of similar market tendency
            by K-means
            1. Search optimal clusters number by aglomiration algorithm
            Return:
                nclusters, int, global
        """
        # нормализуем данные
        sc = StandardScaler()
        df_sc = sc.fit_transform(self.df)

        # применим алгоритм агломеративной кластеризации для
        # выделения групп клиентов
        linked = linkage(df_sc, method='average')

        if not USEDEFAULT:
            # нарисуем дендрограмму полученных кластеров
            plt.figure(figsize=(15, 10))  
            dendrogram(linked, orientation='top')
            plt.show()

            self.nclusters = input("> Define number of cluster based on dendrogram")

        # разобьем клиентов на группы в соответствии с результатами агломеративной кластеризации
        self.km = KMeans(
            n_clusters=self.nclusters,
            random_state=345
        )
        self.labels = self.km.fit_predict(df_sc)

        # создадим колонку, в которую запишем соответствующий
        # каждому клиенту номер группы, полученный методом k-means
        self.df['cluster'] = self.labels


    """
        Data base manipulation methods
    """
    def db_save_data(self):
        """
            Запись результатов прогноза в БД на ближайшие 4 недели
        """
        # получаем из таблицы с ассортиментом данные по
        # наличию товаров в магазинах на ближайшие 4 недели
        query_products_future = '''
            SELECT *
            FROM products
            WHERE D1 > current_date AND (D2 - current_date) <= 28
        '''

        # данные по ассортименту на 4 недели вперед
        products_future = pd.io.sql.read_sql(
            query_products,
            con=engine
        )

        # записываем каждый день наличия товара в ассортименте в отдельную строку
        products_future['D'] = get_date_range(products_future)
        products_future = products_future.explode('D')

        # перезаписываем категориальные данные
        products_future['S'] = encoder.fit_transform(products_future['S'])
        products_future['I'] = encoder.fit_transform(products_future['I'])

        #выделяем неделю и год для каждого товара и удаляем дубликаты 
        products_future['W'] = pd.to_datetime(products_future['D']).dt.week
        products_future['Y'] = pd.to_datetime(products_future['D']).dt.year
        products_future = products_future.drop_duplicates()

        # составляем прогноз и записываем в отдельную колонку
        products_pred = model_2.predict(products_future)
        products_future['N'] = products_pred

        #запись дат начала и конца недели в отдельные колонки
        products_pred['D1'] = self.convert_week(products_pred)[0]
        products_pred['D2'] = self.convert_week(products_pred)[1]

        #запись датафрейма без номера недели и года в специальную таблицу в БД demand_predicted, где должны храниться следующие данные:
        # S - название магазина
        # I - категория товара
        # N - прогнозируемый спрос
        # D1 - дата начала недели
        # D2 - дата конца недели
        products_pred.drop(['Y', 'W'], axis=1).to_sql('demand_predicted', engine)


    def db_get_data(self):
        """
            Set class global variable
            name: df, type: pandas_dataframe
        """

        #исторические данные о продажах
        #таблица sales со следующими данными:
        #S, I, D, N
        query_sales = '''
            SELECT *
            FROM sales
        '''

        #исторические данные об ассортименте
        #таблица products со следующими данными:
        #S, I, D1, D2
        query_products = '''
            SELECT *
            FROM products
            WHERE D1 < current_date
        '''

        # датафрейм с историческими данными по продажам
        sales = pd.io.sql.read_sql(
            query_sales,
            con=engine
        )

        # датафрейм с историческими данными по ассортименту
        products = pd.io.sql.read_sql(
            query_products,
            con=engine
        )

        # создаем колонку, куда записываем списком все даты, в которые товар I находился в магазине S в ассортименте
        products['D'] = get_date_range(products)

        # перезаписываем каждый элемент списка с датами в отдельную ячейку
        products = products.explode('D')


        #объединяем датасеты с продажами и ассортиментом
        #там, где в колонке N стоят NaN,заполняем нулями, делая вывод, что товар был в ассортименте, но не был продан 
        self.df = sales.merge(products, on=['S', 'I', 'D'], how='right').fillna(0)



    """
        Common methods
    """
    def get_date_range(self, df):
        '''
            функция для получения списка дат в диапазоне
            от даты ввода до даты вывода товара из ассортимента
        '''
        date_range_list = []
        for i in range(len(df)):
            try:
                date_range = pd.date_range(start=df.loc[i, 'D1'].date(), end=df.loc[i, 'D2'].date()).to_list()
            except ValueError:
                date_range = np.nan
            date_range_list.append(date_range)
        return date_range_list


    def gen_datetime(self, min_year=2018, max_year=datetime.now().year):
        '''
            функция для генерации даты в формате yyyy-mm-dd hh:mm:ss.000000
        '''
        start = datetime(min_year, 1, 1, 00, 00, 00)
        years = max_year - min_year + 1
        end = start + timedelta(days=365 * years)
        return start + (end - start) * random.random()

    def convert_week(self, df):
        '''
            функция для конвертирования номера недели и года
            в дату начала недели и дату конца недели
        '''
        D1_list = []
        D2_list = []
        for i in range(len(df)):
            d1 = str(df.loc[i, 'Y']) + ' ' + str(df.loc[i, 'W'])
            d1_converted = datetime.strptime(d1 + '-1', "%Y %W-%w")
            D1_list.append(pd.to_datetime(d1))
            d2 = d1_converted + timedelta(days=6)
            D2_list.append(pd.to_datetime(d2))
        return D1_list, D2_list

    def add_week_chain_columns(self, df_s, nweeks=4):
        """
            Add "chain" columns:
            N   - week 0
            N_1 - week 0+1 (next week)jrtq/ 
            N_2 - week 0+2 (2nd next week)
            N_3 - week 0+1 (3rd next week)
        """
        for i in range(1, nweeks):
            df_s[ "N_"+str(i) ] = 0

        # dunno how to do this faster
        for i in range(0, df_s.shape[0]):
            row = df_s.iloc[i, :]
            print(row.Y, row.W, row.S, row.I )
            for nw in range(0, nweeks):
                sel =\
                    (df_s.Y == row.Y) &\
                    (df_s.W == row.W+nw) &\
                    (df_s.S == row.S) &\
                    (df_s.I == row.I)
                sel_df = df_s[sel]
                if sel_df.shape[0] > 0:
                    (df_s.iloc[i, :])["N_"+str(nw)]= sel_df.N
        return df_s #dunno, may be its linked by "pointers"


    """
        Test methods
    """
    def TEST_add_week_chain_columns(self, df_s):
        df_s = df_train[df_train.I<1000]
        df_s.I=0
        df_s.S=0
        df_s = df_s.drop_duplicates(['Y','W'])
        df_s = df_s[df_s.Y==2020]

        # test
        add_week_chain_columns( df_s, nweeks=4)

        print("> df_s.head:")
        print( df_s.head() )


