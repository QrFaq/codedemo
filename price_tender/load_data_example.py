#!/usr/bin/env python
# coding: utf-8

# In[50]:


import pandas as pd
import numpy as np
import random
from sqlalchemy import create_engine
from datetime import datetime, timedelta
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import TimeSeriesSplit
from sklearn.linear_model import Ridge
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error


# #### Загрузка данных из БД для обучения модели

# Исходим из того, что в БД есть таблицы `sales` с историческими данными о продажах (колонки S, I, D, N) и `products` (колонки S, I, D1, D2), где
# * S - название магазина
# * I - категория товара
# * D - дата продажи товара
# * N - количество проданного товара
# * D1 - дата ввода товара в ассортимент
# * D2 - дата вывода товара из ассортимента

# In[ ]:


db_config = {'user': 'my_user',
             'pwd': 'my_user_password',
             'host': 'localhost',
             'port': 5432,
             'db': 'store'}


# In[ ]:


connection_string = 'postgresql://{}:{}@{}:{}/{}'.format(db_config['user'], db_config['pwd'],
                                                         db_config['host'], db_config['port'],
                                                         db_config['db'])

engine = create_engine(connection_string)

#исторические данные о продажах
#таблица sales со следующими данными:
#S, I, D, N
query_sales = '''
            SELECT *
            FROM sales
        '''

#исторические данные об ассортименте
#таблица products со следующими данными:
#S, I, D1, D2
query_products = '''
            SELECT *
            FROM products
            WHERE D1 < current_date
        '''

sales = pd.io.sql.read_sql(query_sales, con=engine) #датафрейм с историческими данными по продажам
products = pd.io.sql.read_sql(query_products, con=engine) #датафрейм с историческими данными по ассортименту

def get_date_range(df):
    '''
    функция для получения списка дат в диапазоне от даты ввода до даты вывода товара из ассортимента
    '''
    date_range_list = []
    for i in range(len(df)):
        try:
            date_range = pd.date_range(start=df.loc[i, 'D1'].date(), end=df.loc[i, 'D2'].date()).to_list()
        except ValueError:
            date_range = np.nan
        date_range_list.append(date_range)
    return date_range_list

#создаем колонку, куда записываем списком все даты, в которые товар I находился в магазине S в ассортименте
products['D'] = get_date_range(products)

#перезаписываем каждый элемент списка с датами в отдельную ячейку
products = products.explode('D')

#объединяем датасеты с продажами и ассортиментом
#там, где в колонке N стоят NaN,заполняем нулями, делая вывод, что товар был в ассортименте, но не был продан 
df = sales.merge(products, on=['S', 'I', 'D'], how='right').fillna(0)


# #### Формирования тестового датасета для обучения модели

#отсюда https://www.kaggle.com/saitosean/mercari
df = pd.read_csv('test_ds.tsv', sep='\t')

df.head()
df.info()
df = df.rename(columns={'category_name': 'S', 'brand_name': 'I', 'shipping': 'N'})
df = df[['S', 'I', 'N']]
df.head()

def gen_datetime(min_year=2018, max_year=datetime.now().year):
    '''
    функция для генерации даты в формате yyyy-mm-dd hh:mm:ss.000000
    '''
    start = datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random.random()

df['D'] = 0
df['D'] = df['D'].apply(lambda x: gen_datetime(min_year=2018, max_year=datetime.now().year))
items = df.I.unique()

def gen_item():
    '''
    функция для выбора случайной категории товара
    '''
    i = np.random.randint(0, 3900)
    return items[i]

df['I'] = df['I'].apply(lambda x: gen_item()).fillna('B Darlin')
df['S'] = df['S'].fillna('Men/Sweaters')
df['N'] = df['N'].apply(lambda x: np.random.randint(0, 1000))


# #### Работа с итоговым датафреймом

#итоговый датафрейм без пропусков
df.head()
encoder =  LabelEncoder()

#кодировка категориальных данных
df['S'] = encoder.fit_transform(df['S'])
df['I'] = encoder.fit_transform(df['I'])
df.head()


#деление данных на обучающую и тестовую выборку с учетом временного ряда
tss = TimeSeriesSplit(n_splits = 3)
for train_index, test_index in tss.split(df):
    df_train, df_test = df.iloc[train_index, :], df.iloc[test_index,:]

#выделение недели и года в обучающей выборке
df_train.loc[:, 'W'] = pd.to_datetime(df_train.loc[:, 'D']).dt.week
df_train.loc[:, 'Y'] = pd.to_datetime(df_train.loc[:, 'D']).dt.year

#выделение недели и года в тестовой выборке
df_test.loc[:, 'W'] = pd.to_datetime(df_test.loc[:, 'D']).dt.week
df_test.loc[:, 'Y'] = pd.to_datetime(df_test.loc[:, 'D']).dt.year

#подсчет количества каждой категории проданного товара в каждом магазине за каждую неделю
df_train = df_train.groupby(
    ['Y', 'W', 'S', 'I']).agg({'N': 'sum'}).reset_index()
df_test = df_test.groupby(
    ['Y', 'W', 'S', 'I']).agg({'N': 'sum'}).reset_index()

features_train = df_train.drop(labels=['N'], axis=1)
features_test = df_test.drop(labels=['N'], axis=1)
target_train = df_train['N']
target_test = df_test['N']


#попробуем модель ridge регрессии, чтобы нивелировать возможный эффект слишком сильных корреляций связанных параметров
model = Ridge()
model.fit(features_train, target_train)
predictions = model.predict(features_test)

#оценим веса каждого параметра, которые были присвоены алгоритмом
coef_dict = {}
for coef, feature in zip(model.coef_, features_test.columns):
    coef_dict[feature] = coef


coef_dict
print('MAE:', mean_absolute_error(target_test, predictions))



def mean_absolute_percentage_error(target, pred):
    '''
    функция для вычисления средней относительной ошибки по модулю
    '''
    sum_errors = 0
    for i in range(len(target)):
        if abs(target[i]) > 0:
            sum_errors += abs(target[i] - pred[i]) / abs(target[i])
    mape = 1/len(target) * sum_errors * 100
    return mape

print('MAPE:', mean_absolute_percentage_error(target_test, predictions))




#попробуем случайный лес в качестве ансамблевой модели с подбором решения с наименьшей средней квадратичной ошибкой
model_2 = RandomForestRegressor(criterion='mse')
model_2.fit(features_train, target_train)
predictions_2 = model_2.predict(features_test)
print('MSE:', mean_squared_error(target_test, predictions_2))
print('MAPE:', mean_absolute_percentage_error(target_test, predictions_2))


# #### Запись результатов прогноза в БД на ближайшие 4 недели

#получаем из таблицы с ассортиментом данные по наличию товаров в магазинах на ближайшие 4 недели
query_products_future = '''
            SELECT *
            FROM products
            WHERE D1 > current_date AND (D2 - current_date) <= 28
        '''

products_future = pd.io.sql.read_sql(query_products, con=engine) #данные по ассортименту на 4 недели вперед

#записываем каждый день наличия товара в ассортименте в отдельную строку
products_future['D'] = get_date_range(products_future)
products_future = products_future.explode('D')

#перезаписываем категориальные данные
products_future['S'] = encoder.fit_transform(products_future['S'])
products_future['I'] = encoder.fit_transform(products_future['I'])

#выделяем неделю и год для каждого товара и удаляем дубликаты 
products_future['W'] = pd.to_datetime(products_future['D']).dt.week
products_future['Y'] = pd.to_datetime(products_future['D']).dt.year
products_future = products_future.drop_duplicates()

#составляем прогноз и записываем в отдельную колонку
products_pred = model_2.predict(products_future)
products_future['N'] = products_pred


def convert_week(df):
    '''
    функция для конвертирования номера недели и года в дату начала недели и дату конца недели
    '''
    D1_list = []
    D2_list = []
    for i in range(len(df)):
        d1 = str(df.loc[i, 'Y']) + ' ' + str(df.loc[i, 'W'])
        d1_converted = datetime.strptime(d1 + '-1', "%Y %W-%w")
        D1_list.append(pd.to_datetime(d1))
        d2 = d1_converted + timedelta(days=6)
        D2_list.append(pd.to_datetime(d2))
    return D1_list, D2_list

#запись дат начала и конца недели в отдельные колонки
products_pred['D1'] = convert_week(products_pred)[0]
products_pred['D2'] = convert_week(products_pred)[1]


#запись датафрейма без номера недели и года в специальную таблицу в БД demand_predicted, где должны храниться следующие данные:
#S - название магазина
#I - категория товара
#N - прогнозируемый спрос
#D1 - дата начала недели
#D2 - дата конца недели
products_pred.drop(['Y', 'W'], axis=1).to_sql('demand_predicted', engine)


