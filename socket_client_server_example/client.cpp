#include <arpa/inet.h> 
#include <stdio.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <unistd.h> 

#include "cmds.h"

int main(int argc, char* argv[]) 
{ 
    int sock; 
    struct sockaddr_in server; 
    float server_reply[10]; 
    float number[10] = { 5.f, 4.f, 3.f, 8.f, 9.f, 1.f, 2.f, 0.f, 6.f };
    //int i, temp; 
  
    // Create socket 
    sock = socket(AF_INET, SOCK_STREAM, 0); 
    if (sock == -1) { 
        printf("Could not create socket"); 
    } 
    puts("Socket created"); 
  
    server.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    server.sin_family = AF_INET; 
    server.sin_port = htons(8880); 
  
    // Connect to remote server 
    if (connect(sock, (struct sockaddr*)&server, sizeof(server)) < 0) { 
        perror("connect failed. Error"); 
        return 1; 
    } 
  
    puts("Connected\n"); 
  
    // imitation of sending the data
    int cmd[4] = {SORT_ARR_CMD,1,10, -1};
    int out_cmd[4];
    if ( cmdWithConfirmation(sock, cmd, out_cmd, 4) ) return 1;
    for (int i = 0; i < 4; i++) printf( "s-resp: cmd[%i]:%i\n", i, cmd[i] );

    //if (send(sock, cmd, 4 * sizeof(int), 0) < 0) { puts("Send failed"); return 1; }
    //if (recv(sock, cmd, 4 * sizeof(int), 0) < 0) { puts("recv failed"); return 1; }

    if ( sendToServer(sock, number, 10) ) return 1;
    if ( readFromServer(sock, server_reply, 10) ) return 1;
    //if (send(sock, &number, 10 * sizeof(float), 0) < 0) { puts("Send failed"); return 1; }
    //if (recv(sock, &server_reply, 10 * sizeof(float), 0) < 0) { puts("recv failed"); return 1; }
  
    puts("Server reply :"); 
    for (int i = 0; i < 10; i++)
        printf("%.2f, ", server_reply[i]); 
    printf("\n");
    
    if( shutdownServer(sock) ) return 1;

    // close the socket 
    close(sock); 
    return 0; 
}