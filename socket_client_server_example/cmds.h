#include <arpa/inet.h> 
#include <stdio.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <unistd.h> 

#define SORT_ARR_CMD 0xAA
#define EXIT_CMD 0xFF
#define PROCESS_IMG 0xCD

/*
Small info mark:

cmd - data type - size - task

1. send cmd "give you photo" of "size", pls, do "task",
    waiting "confirm message with data size" OR wating until end flag?
OR set fixed sizes for everythin ???
2. Exit: [exit, 0xFF, 0xFF, 0xFF]

server:
    1. waits cmd array
    2. confirmation + data sizes (or they are fixed==just confirmation OR no confirmation)
    3. send data
    4. wait another cmds
*/

extern int kill_cmd[4];
extern int shutdownServer(int sock);

template <typename T>
int readFromServer(int sock, T *arr, int sz)
{
    if (recv(sock, arr, sz * sizeof(T), 0) < 0) { puts("recv failed"); return 1; }
    return 0;
}

template <typename T>
int sendToServer(int sock, T *arr, int sz)
{
    if (send(sock, arr, sz * sizeof(T), 0) < 0) { puts("Send failed"); return 1; }
    return 0;
}

template <typename T>
int cmdWithConfirmation(int sock, T *arr, T *out_arr, int sz)
{
    if ( sendToServer(sock, arr, sz) )
        return 1;
    if ( readFromServer(sock, out_arr, sz) )
        return 1;
    return 0;
}