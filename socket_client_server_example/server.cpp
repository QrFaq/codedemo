#include <arpa/inet.h> 
#include <stdio.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <unistd.h> 
#include <iostream>
#include "cmds.h"
#define CHAR 0
#define INT 1
#define FLOAT 2

template <typename T>
void bubble_sort(T list[], int n) 
{ 
    int c, d;
    T t; 
  
    for (c = 0; c < (n - 1); c++) { 
        for (d = 0; d < n - c - 1; d++) { 
            if (list[d] > list[d + 1]) { 
  
                /* Swapping */
                t = list[d]; 
                list[d] = list[d + 1]; 
                list[d + 1] = t; 
            } 
        } 
    } 
} 

class SocketServer
{
private:
    int error = 0;
    int socket_desc, client_sock, c;
    struct sockaddr_in server, client;
    int cmd[4];//cmd - data type - size - task
    const int confirm_msg[4] = {0xCC, 0xCC, 0xCC, 0xCC};
    const int wait_msg[4] = {0xAA, 0xAA, 0xAA, 0xAA};
public:
    int port = 8880;


    SocketServer()
    {
        // Create socket 
        socket_desc = socket(AF_INET, SOCK_STREAM, 0);
        if (socket_desc == -1)
            printf("Could not create socket");
        puts("Socket created");
      
        // Prepare the sockaddr_in structure
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons(port);
      
        // Bind the socket 
        if (bind(socket_desc, (struct sockaddr*)&server, sizeof(server)) < 0)
        {
            // print the error message
            perror("bind failed. Error");
            error = 1;
        } 
        puts("bind done"); 
      
        // lsiten to the socket 
        listen(socket_desc, 3); 
      
        puts("Waiting for incoming connections..."); 
        c = sizeof(struct sockaddr_in); 
      
        // accept connection from an incoming client 
        client_sock = accept(socket_desc, (struct sockaddr*)&client, (socklen_t*)&c); 
      
        if (client_sock < 0) { 
            perror("accept failed"); 
            error = 1; 
        } 
      
        puts("Connection accepted"); 
    }
    ~SocketServer(){}

    template <typename T>
    int read_socket(T *arr, int sz)
    {
        int retflag = 0;

        // Receive a cmd from client 
        int read_size = recv(client_sock, arr, sz * sizeof(T), 0);

        if (read_size == 0)
        {
            puts("Client disconnected");
            retflag = 1;
        }
        else if (read_size == -1)
        { 
            perror("recv failed"); 
            retflag = 2;
        }
        return retflag; 
    }

    void main()
    {
        bool kill_flag = false;
        int cmd[4];
        while (true)//???(~kill_flag)
        {
            if (kill_flag) break;
            for (int i = 0; i < 4; i++)
                cmd[i] = 0;
            
            // read command
            read_socket<int>(cmd, 4);

            if ( (cmd[0] == kill_cmd[0]) && (cmd[1] == kill_cmd[1]) && (cmd[2] == 0xFF) && (cmd[3] == kill_cmd[3]) )
            {
                std::cout << "Server got: 0xFF" << std::endl;
                std::cout << "Server send: response to the client" << std::endl;
                write(client_sock, confirm_msg, 4 * sizeof(int));
                kill_flag = true;
            }
            else if (cmd[0] == PROCESS_IMG)
            {

            }
            else if (cmd[0] == SORT_ARR_CMD)
            {
                int type = cmd[1];
                const int arr_sz = cmd[2];
                int task = cmd[3];
                for (int i = 0; i < 4; i++)
                    printf( "c-cmd: cmd[%i]:%i\n,", i, cmd[i] ); 

                std::cout << "Server got: 0xAA" << std::endl;
                write(client_sock, wait_msg, 4 * sizeof(int));

                std::cout << "Server got: wait data" << std::endl;
                float msg[arr_sz];
                read_socket<float>(msg, arr_sz);

                std::cout << "Server sorting the data..." << std::endl;
                bubble_sort<float>(msg, arr_sz);

                write(client_sock, msg, arr_sz * sizeof(float));
                printf( "before kill_flag=%d\n,", kill_flag ); 
                //kill_flag = true;
                printf( "not kill_flag=%d\n,", kill_flag ); 
            }
            //if (counter >7) break; else{counter++;printf( "kill_flag=%d\n,", kill_flag );}
        }// end while
        std::cout << "Server status: Shuted down" << std::endl;
    }
};

int main(int argc, char* argv[]) 
{
    SocketServer socket;
    socket.main();  
    return 0; 
}