import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import mean_absolute_error
from sklearn.linear_model import LinearRegression
from sklearn.experimental import enable_hist_gradient_boosting
from sklearn.ensemble import HistGradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV

from sklearn.model_selection import validation_curve
from sklearn.preprocessing import PolynomialFeatures
from sklearn.tree import DecisionTreeRegressor

"""
    Warning: script shall be run after launching `task2.ipynb`
    
    task2.ipynb:
        1. data observation
        2. creates learning and test files used by `task2.py`
"""

class PrognoseModelTest:
    def __init__(self):
        self.X = None
        self.y = None
        self.Xt = None
        self.yt = None

    def load_data(self, train_fpath="./train", test_fpath="./test"):        
        train = pd.read_pickle(train_fpath)
        test = pd.read_pickle(test_fpath)
        self.X = train.iloc[:,:train.shape[1]-1].to_numpy()
        self.y = train.iloc[:,train.shape[1]-1].to_numpy().reshape( (-1,1) )
        self.Xt = test.iloc[:,:test.shape[1]-1].to_numpy()
        self.yt = test.iloc[:,test.shape[1]-1].to_numpy().reshape( (-1,1) )
    

    def daw_results(self, yt, y_pred):
        plt.plot(yt.reshape( (-1,) ), "-o",  label="1", c='orange',markersize=1.0)
        plt.plot(y_pred.reshape( (-1,) ), "--o",  label="2", c='red',markersize=1.0)
        plt.plot(yt.reshape( (-1,) ) - y_pred.reshape( (-1,) ), "--o",  label="3", markersize=1.0)
        plt.legend()
        plt.show()

    def test_model(self, X, y, Xt, yt, model ):
        reg = model.fit(X, y)
        #print( reg.coef_ )
        y_pred = reg.predict(Xt)
        mae = mean_absolute_error(yt, y_pred)
        print( "Model score:", reg.score(X, y) )
        print( "MAE:", mae)
        return model, mae, y_pred

    """
        Test functions
    """
    def linearRegr_test(self):
        model, mae, y_pred = self.test_model(
            self.X, self.y, self.Xt, self.yt,
            model=LinearRegression()
        )
        #Model score: 0.77
        #MAE: 15.2
        return model, mae, y_pred

    def polynomialRegr_test(self):
        polynomial_features= PolynomialFeatures(degree=3)
        x_poly = polynomial_features.fit_transform(self.X)
        Xt_poly = polynomial_features.fit_transform(self.Xt)

        model, mae, y_pred = self.test_model(
            x_poly, self.y.reshape((-1,)), Xt_poly, self.yt,
            model=LinearRegression()
        )
        #Model score: 0.88
        #MAE: 13.25
        return model, mae, y_pred

    def histGradBoost_test(self):
        model, mae, y_pred = self.test_model(
            self.X, self.y, self.Xt, self.yt,
            model=HistGradientBoostingRegressor()
        )
        #Model score: 0.95
        #MAE: 9.37
        return model, mae, y_pred

    def randForestBestfit_test(self):
        model, mae, y_pred = self.test_model(
            self.X, self.y.reshape((-1,)), self.Xt, self.yt,
            model=RandomForestRegressor(
                max_depth=25,
                min_samples_leaf=2,
                min_samples_split=5,
                n_estimators=1100,
                criterion="mae"
            )
        )# ~ a couple of hours, best model
        #Model score: 0.9703871244873001
        #MAE: 9.697675240180955
        return model, mae, y_pred
    
    def decisionTree_Test(self):
        model, mae, y_pred = self.test_model( self.X, self.y.reshape((-1,)), self.Xt, self.yt,
            model=DecisionTreeRegressor(max_depth=7, criterion="mae", min_samples_split=10, min_samples_leaf=15)
        )
        #Model score: 0.8575646230437242
        #MAE: 14.228327765078248
        return model, mae, y_pred

    def randForest_ModelSearch(self, fRun=False):
        # Number of trees in random forest
        n_estimators = [int(x) for x in np.linspace(start = 100, stop = 1200, num = 12)]
        max_features = ['auto', 'sqrt']# at every split
        max_depth = [int(x) for x in np.linspace(5, 30, num = 6)]# levels in tree
        min_samples_split = [2, 5, 10, 15, 100]
        min_samples_leaf = [1, 2, 5, 10]
        # bootstrap = [True, False]

        # Create the random grid
        random_grid = {'n_estimators': n_estimators,
                    'max_features': max_features,
                    'max_depth': max_depth,
                    'min_samples_split': min_samples_split,
                    'min_samples_leaf': min_samples_leaf}
        
        if fRun:
            rf = RandomForestRegressor()
            rf_random = RandomizedSearchCV(
                estimator=rf,
                param_distributions=random_grid,
                n_iter = 100,
                cv = 3,
                verbose=2,
                random_state=42,
                n_jobs = 1
            )
            rf_random.fit(self.X, self.y)
            return (
                rf_random.best_estimator_,#Res: RandomForestRegressor(max_depth=25, min_samples_leaf=2, min_samples_split=5,n_estimators=1100)
                rf_random.best_score_,#0.9223171731031993
                rf_random.best_params_,#{'n_estimators': 1100, 'min_samples_split': 5, 'min_samples_leaf': 2, 'max_features': 'auto', 'max_depth': 25}
                rf_random.refit_time_#202.37738871574402
            )
        else:
            return None

def create_submit():
    df_submit = pd.read_csv("./task2/Y_submit.csv", sep=";", header=None, names=["time","quality"])
    df_xtrain = pd.read_csv("./task2/X_data.csv", sep=";").rename( columns={"Unnamed: 0": "time"} )
    df_xsubmit = pd.merge(
        left=df_xtrain,
        right=df_submit,
        left_on='time',
        right_on='time'
    )
    del df_submit, df_xtrain
    
    # fit model
    prognoseModel = PrognoseModelTest()
    prognoseModel.load_data( train_fpath="./train", test_fpath="./test" )
    model, _, _ = prognoseModel.histGradBoost_test()

    # predictions
    X_submit = df_xsubmit.iloc[:,1:df_xsubmit.shape[1]-1].to_numpy()
    y_pred = model.predict(X_submit)
    df_xsubmit.iloc[:,df_xsubmit.shape[1]-1] = y_pred
    
    # save submited data
    df_xsubmit[['time', 'quality']].to_csv('res_submit_Y.csv')

def main():
    prognoseModel = PrognoseModelTest()
    prognoseModel.load_data(
        train_fpath="./train",
        test_fpath="./test"
    )
    model, mae, y_pred = prognoseModel.histGradBoost_test()


if __name__ == "__main__":
    create_submit()
