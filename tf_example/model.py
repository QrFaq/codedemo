"""
64  x conv 1x1
128 x conv 3x3
128 x conv 5x5
128 x conv 7x7
16  x (max_pool 5x5 + conv 1x1)
16  x (avg_pool 3x3 + conv 1x1)
16  x (avg_pool 5x5 + conv 1x1)
"""

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
##from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
from data_preprocessing import SoundPreprocessors

#log.setLevel(logging.DEBUG)
from keras.utils.vis_utils import plot_model


class Conv_norm_relu_layer(tf.keras.layers.Layer):
    def __init__(self,
        filters, kernel_size=1,
        padding="same", kernel_initializer="glorot_uniform",
        epsilon = 1e-5, momentum = 0.999#, training=False
    ):
        super(Conv_norm_relu_layer, self).__init__()
        self.conv = tf.keras.layers.Conv1D(
            filters=filters,
            kernel_size=kernel_size,
            kernel_initializer=kernel_initializer,
            padding=padding#, training=training
        )
        self.batch_norm = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )

    def call(self, input_x, training=False):
        x = self.conv(input_x)
        x = self.batch_norm(x, training=training)
        x_output = tf.keras.layers.Activation(
            activation=tf.nn.relu
        )(x)
        return x_output

class Convolution_layer(tf.keras.layers.Layer):
    def __init__(self,
        filters_top=None,
        kernel_size_top=None,
        filters_bottom=None,
        kernel_size_bottom=None,
        padding="same", kernel_initializer="glorot_uniform",
        epsilon = 1e-5, momentum = 0.999
    ):
        super(Convolution_layer, self).__init__()
        self.cnr_top = Conv_norm_relu_layer(
            filters=filters_top,
            kernel_size=kernel_size_top,
            padding=padding,
            kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.cnr_bottom = Conv_norm_relu_layer(
            filters=filters_bottom,
            kernel_size=kernel_size_bottom,
            padding=padding,
            kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

    def call(self, input_x, training=False):
        x = self.cnr_top(input_x, training=training)
        x_output = self.cnr_bottom (x, training=training)
        return x_output

class Maxpool_layer(tf.keras.layers.Layer):
    def __init__(self,
        pool_size=None, padding='same', strides=1, 
        momentum=0.999, epsilon=1e-5, 
        filters=None, kernel_size=1,
        kernel_initializer="glorot_uniform"
    ):
        super(Maxpool_layer, self).__init__()
        self.maxpool = tf.keras.layers.MaxPool1D(
            pool_size=pool_size,
            strides=strides,
            padding=padding
        )
        self.batch_norm = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )
        self.conv = tf.keras.layers.Conv1D(
            filters=filters,
            kernel_size=kernel_size,
            kernel_initializer=kernel_initializer,
            padding=padding
        )

    def call(self, input_x, training=False):
        x = self.maxpool(input_x)
        x = self.batch_norm(x, training=training)
        x_output = self.conv(x)
        return x_output

class Avgpool_layer(tf.keras.layers.Layer):
    def __init__(self,
        pool_size=None, padding='same', strides=1, 
        momentum=0.999, epsilon=1e-5, 
        filters=None, kernel_size=1,
        kernel_initializer="glorot_uniform"
    ):
        super(Avgpool_layer, self).__init__()
        self.avgpool = tf.keras.layers.AveragePooling1D(
            pool_size=pool_size,
            strides=strides,
            padding=padding
        )
        self.batch_norm = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )
        self.conv = tf.keras.layers.Conv1D(
            filters=filters,
            kernel_size=kernel_size,
            kernel_initializer=kernel_initializer,
            padding=padding
        )

    def call(self, input_x, training=False):
        x = self.avgpool(input_x)
        x = self.batch_norm(x, training=training)
        x_output = self.conv(x)
        return x_output

class Inception1d(tf.keras.layers.Layer):
    def __init__(self,
        depth,
        padding="same", kernel_initializer="glorot_uniform",
        epsilon=1e-5, momentum=0.999
    ):
        super(Inception1d, self).__init__()
        self.batch_norm = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )
        
        # Branch 1: 64 x conv 1x1
        self.branch_conv_1_1 = Conv_norm_relu_layer(
            filters=16*depth, kernel_size=1,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 2: 128 x conv 3x3
        self.branch_conv_3_3 = Convolution_layer(
            filters_top=16,
            kernel_size_top=1,
            filters_bottom=32*depth,
            kernel_size_bottom=3,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 3: 128 x conv 5x5
        self.branch_conv_5_5 = Convolution_layer(
            filters_top=16,
            kernel_size_top=1,
            filters_bottom=32*depth,
            kernel_size_bottom=5,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 4: 128 x conv 7x7
        self.branch_conv_7_7 = Convolution_layer(
            filters_top=16,
            kernel_size_top=1,
            filters_bottom=32*depth,
            kernel_size_bottom=7,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 5: 16 x (max_pool 3x3 + conv 1x1)
        self.branch_maxpool_3_3 = Maxpool_layer(
            pool_size=3, filters=16,
            kernel_size=1, strides=1,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 6: 16 x (max_pool 5x5 + conv 1x1)
        self.branch_maxpool_5_5 = Maxpool_layer(
            pool_size=5, filters=16,
            kernel_size=1, strides=1,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 7: 16 x (avg_pool 3x3 + conv 1x1)
        self.branch_avgpool_3_3 = Avgpool_layer(
            pool_size=3, filters=16,
            kernel_size=1, strides=1,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        # Branch 8: 16 x (avg_pool 5x5 + conv 1x1)
        self.branch_avgpool_5_5 = Avgpool_layer(
            pool_size=5, filters=16,
            kernel_size=1, strides=1,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

    def call(self, input_x, training=False):
        x_norm = self.batch_norm(input_x, training=training)
        bconv_1_1 = self.branch_conv_1_1(x_norm)
        bconv_3_3 = self.branch_conv_3_3(x_norm)
        bconv_5_5 = self.branch_conv_5_5(x_norm)
        bconv_7_7 = self.branch_conv_7_7(x_norm)
        bmaxpool_3_3 = self.branch_maxpool_3_3(x_norm)
        bmaxpool_5_5 = self.branch_maxpool_5_5(x_norm)
        bavgpool_3_3 = self.branch_avgpool_3_3(x_norm)
        bavgpool_5_5 = self.branch_avgpool_5_5(x_norm)

        x_output = tf.concat([
            bconv_1_1, bconv_3_3, bconv_5_5, bconv_7_7,
            bmaxpool_3_3, bmaxpool_5_5,
            bavgpool_3_3, bavgpool_5_5
            ], axis=-1
        )
        return x_output

class Inception1d_pool(tf.keras.layers.Layer):
    def __init__(self,
        depth, pool_size=2, strides=2,
        padding="same", kernel_initializer="glorot_uniform",
        epsilon=1e-5, momentum=0.999
    ):
        super(Inception1d_pool, self).__init__()
        self.inception_1 = Inception1d(
            depth=depth,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        
        self.inception_2 = Inception1d(
            depth=depth,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool = tf.keras.layers.MaxPool1D(
            pool_size=pool_size,
            strides=strides,
            padding=padding
        )

    def call(self, input_x, training=False):
        x = self.inception_1(input_x, training=training)
        x = self.inception_2(x, training=training)
        x_output = self.maxpool(x)
        return x_output

class ATRModel(tf.keras.Model):
    def __init__(self, class_cardinality,
        epsilon = 1e-5, momentum = 0.999,
        padding="same", kernel_initializer="glorot_uniform"
    ):
        super(ATRModel, self).__init__()
        #self.inception_1 = Inception1d(
        #    depth=1,
        #    padding=padding, kernel_initializer=kernel_initializer,
        #    epsilon=epsilon, momentum=momentum
        #)
        self.maxpool_1 = Inception1d_pool(#
            depth=1, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_2 = Inception1d_pool(
            depth=1, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_3 = Inception1d_pool(
            depth=2, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_4 = Inception1d_pool(
            depth=2, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_5 = Inception1d_pool(
            depth=3, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_6 = Inception1d_pool(
            depth=3, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_7 = Inception1d_pool(
            depth=4, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_8 = Inception1d_pool(
            depth=4, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )
        self.maxpool_9 = Inception1d_pool(
            depth=4, pool_size=2, strides=2,
            padding=padding, kernel_initializer=kernel_initializer,
            epsilon=epsilon, momentum=momentum
        )

        self.flatten = tf.keras.layers.Flatten()
        self.dense_1 = tf.keras.layers.Dense(
            128,
            activation=tf.nn.relu
        )
        self.bn_dense_1 = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )
        self.dense_2 = tf.keras.layers.Dense(
            class_cardinality,
            activation=None
        )
        self.bn_dense_2 = tf.keras.layers.BatchNormalization(
            momentum=momentum,
            epsilon=epsilon
        )
        #self.dense_2 = tf.keras.layers.Dense(1)#TEST

    def call(self, inputs, training=False):
        #print(">>>>>> inputs", inputs.shape, type(inputs) )
        x = self.maxpool_1(inputs)
        x = self.maxpool_2(x)
        x = self.maxpool_3(x)
        x = self.maxpool_4(x)
        x = self.maxpool_5(x)
        x = self.maxpool_6(x)
        x = self.maxpool_7(x)
        x = self.maxpool_8(x)
        x = self.maxpool_9(x)
        x = self.flatten(x)
        x = self.dense_1( self.bn_dense_1(x) )
        output = self.dense_2( self.bn_dense_2(x) )
        #output = self.dense_2( inputs )
        return output

    #def model(self, shape):
    #    x = tf.keras.layers.Input(shape=shape)
    #    return tf.keras.Model(inputs=[x], outputs=self.call(x))


if __name__ == "__main__":
    """
    64  x conv 1x1
    128 x conv 3x3
    128 x conv 5x5
    128 x conv 7x7
    16  x (max_pool 5x5 + conv 1x1)
    16  x (avg_pool 3x3 + conv 1x1)
    16  x (avg_pool 5x5 + conv 1x1)
    
    input_shape = (32, 32, 3)
    model = tf.keras.Sequential()
    model.add(layers.Conv2D(
        64, (1, 1), activation='relu', input_shape=input_shape ))
    model.add(layers.Conv2D(# ??? very strange working pattern
        128, (3, 3), activation='relu', input_shape=input_shape ))
    model.add(layers.Conv2D(
        128, (5, 5), activation='relu', input_shape=input_shape ))
    """
    def padding(data, sample_size=8000):
        data = list(data)
        for i in range(0, len(data) ):
            b = data[i]
            tsz = sample_size - len(b)
            lsz = int(tsz/2)
            rsz = tsz - lsz
            #print(tsz, lsz, rsz)
            b = np.pad( b, ( lsz, rsz ), 'constant', constant_values=(0, 0) )
            #print( type(b), type(b[0]) )
            data[i] = b
            #print( len(b) )
        return np.asarray(data, dtype=np.float32)

    def paddingAndRead(fpath, sample_size=8000):
        data = np.load(fpath, allow_pickle=True)
        return padding( list(data), sample_size)

    #for b in padding(ampl_list): print( len(b) )

    def get_model_memory_usage(batch_size, model):
        import numpy as np
        from keras import backend as K

        shapes_mem_count = 0
        internal_model_mem_count = 0
        for l in model.layers:
            layer_type = l.__class__.__name__
            if layer_type == 'Model':
                internal_model_mem_count += get_model_memory_usage(batch_size, l)
            single_layer_mem = 1
            for s in l.output_shape:
                if s is None:
                    continue
                single_layer_mem *= s
            shapes_mem_count += single_layer_mem

        trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
        non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

        number_size = 4.0
        if K.floatx() == 'float16':
             number_size = 2.0
        if K.floatx() == 'float64':
             number_size = 8.0

        total_memory = number_size*(batch_size*shapes_mem_count + trainable_count + non_trainable_count)
        gbytes = np.round(total_memory / (1024.0 ** 3), 3) + internal_model_mem_count
        return gbytes


    nsample_rate = 8000
    svfile_name_list = np.load("./data_example/svname.npy", allow_pickle=True)
    xf_list = paddingAndRead("./data_example/xf.npy", nsample_rate)
    ampl_list = paddingAndRead("./data_example/ampl.npy", nsample_rate )
    phase_list = paddingAndRead("./data_example/phase.npy", nsample_rate )

    trainX = xf_list.reshape( (xf_list.shape[0], xf_list.shape[1], 1) )
    trainY = np.asarray( [ int( svfname.split('.')[0].split('_')[2] ) for svfname in svfile_name_list ] )
    trainY = np.reshape( trainY, (-1,1) )

    print(trainY.shape, trainX.shape, type(trainX) )
    print(trainY)
    print()

    print(svfile_name_list)
    print(xf_list.shape)
    print(ampl_list.shape)
    print(phase_list.shape)
    #exit()

    model = ATRModel( class_cardinality=1 )#class_cardinality
    print("\nModel:\n", model)

    # initialize the optimizer and compile the model
    model.compile(optimizer='adam', loss='mean_squared_error')

    # [batch_size, time_steps, feature_size]
    #model.build( input_shape=trainX.shape )
    trainX = trainX[:, :100, :]
    fit_histrory = model.fit(trainX, trainY)#epochs,#callbacks=[tensorboard_callback] 
    print("> model info:\n", model.summary() )
    #print("> RAM usage of model, GB:", get_model_memory_usage(1, model))
    #print(">>>>>> model info:\n", model.model( (trainX.shape)[1:] ).summary() )

    print("> Save into file...")
    model.save('./test_dir/model')
    print("> Save into file done")

    print("> END <")

