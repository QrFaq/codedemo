"""
http://www.guitarpitchshifter.com/algorithm.html
"""

import numpy as np
import scipy
from scipy.io import wavfile
from scipy.interpolate import interp1d
from playsound import playsound
from tqdm import tqdm
import optparse

class Vocoder:
    def __init__(self, factor_fraction=4, frame_sz=1024, overlap=75.):
        if frame_sz < 64:
            raise Exception( "Error: frame width shall be bigger than 64" )
            exit()
        if np.abs(factor_fraction) > 30:
            raise Exception( "Error: factor fraction shall be in [-30, 30]" )
        if overlap < 0 or overlap >= 100.:
            raise Exception( "Error: window overlapping shall be in [0, 100]%, default: 75%" )

        self.factor_fraction = factor_fraction
        self.frame_sz = frame_sz# samples per frame
        self.overlap = overlap  # %
        self.factor = 2**(self.factor_fraction/12)

        # Define ham-window coefficients with step=2, cause
        # source signal is Re (Im==0)
        self.__hamcoef = np.hamming( self.frame_sz*2+1 )#np.hamming( self.frame_sz )
        self.__hamcoef = np.take(self.__hamcoef, np.arange(2, self.__hamcoef.shape[0], 2) )
        self.f_window = lambda frame, hop: frame * self.__hamcoef[:frame.shape[0] ]/np.sqrt( self.frame_sz/(2*hop) )

        self.fs = None      # sample rate
        self.res = None     # result signal buffer
        self.s_sign = None  # synthesized signal buffer
        
        self.__sind_res = 0   # signal time shift (overlappint purpose)
        self.__prev_phase_a = np.zeros( (self.frame_sz,) )
        self.__prev_phase_s = np.zeros( (self.frame_sz,) )


    def processing(self, frame_a, hop_a, hop_s):
        """
            Processing step:
            1. frame specture
            2. phase shift searching
            3. detect true freq, synthesis phase
            4. generate new specture by (3)
            5. save previouse phases for the next step
            Input:
                frame_a - np.array, frame after analysis step
        """
        yf_back = None
        try:
            frame_sz = frame_a.shape[0]
            yf = scipy.fft( frame_a )# complex 
            
            ampl = np.abs( yf )#symmetry
            phase_a = np.angle( yf )
            w_bins = np.arange(frame_sz)/frame_sz*2*np.pi*hop_a

            delta_phase_a = phase_a - self.__prev_phase_a[:frame_sz]
            delta_w = delta_phase_a - w_bins #frequency deviation
            delta_w_wrapped = np.mod( delta_w + np.pi, 2*np.pi ) - np.pi #wrapped frequency deviation
                
            w_true = (w_bins + delta_w_wrapped)/hop_a #true frequency
            phase_s = self.__prev_phase_s[:frame_sz] + w_true * hop_s
            
            yf_back = ampl * (np.cos(phase_s) + np.sin(phase_s) * 1j )

            # buffer
            self.__prev_phase_a = phase_a
            self.__prev_phase_s = phase_s
        except Exception as error:
            raise Exception( "Error [processing]: %s"%(error) )
        return yf_back

    def synthesis(self, frame_fft, hop_s):
        """
            Synthesis step function.
            Input:
                frame_fft - np.array, array of complex numbers
                    fft result
                hop_s - int, window distance
            Result, fills global variables:
                s_sign - np.array, synthesis signal
                __sind_res - int, shifted index counter
        """
        frame_sz = frame_fft.shape[0]#frame_back
        frame_back = np.real( scipy.ifft(frame_fft) )
        frame_back = self.f_window(frame_back, hop_s)
        
        self.s_sign[self.__sind_res:self.__sind_res+frame_sz] += frame_back
        self.__sind_res += hop_s

    def resultInterp(self):
        """
            Interpolate data with float time step results
            beween two bins, caused by new 'time-step' (factor)
            Input:
                 s_sign - np.array, global, synthesized signal data
                 factor - float, global
            Result:
                res - np.array, global, vcoder result signal 
        """
        try:
            # interpolation function
            lineInterp = interp1d( np.arange(0, self.s_sign.shape[0], 1), self.s_sign)
            
            # define 'time'-axis by time-step of result signal
            x = np.linspace( 0, self.s_sign.shape[0], num=int(self.s_sign.shape[0]-1/self.factor), endpoint=False)
            
            # signal interpolation and back to source signal data type
            self.res = np.round( lineInterp(x) ).astype(np.int16)
        except Exception as error:
            raise Exception( "Error [resultInterp]: %s"%(error) )

    def loadSoundFile(self, fpath):
        self.fs, samples = wavfile.read(fpath)
        
        # convert input data format to calculation data type
        samples = samples.astype(np.int32)

        # N channel to 1 channel convertation
        if np.ndim(samples) > 1:
            samples = np.sum( samples, axis=1)/np.ndim(samples) 
        return samples

    def run(self, fpath):
        """
            Vocoder launch function:
            1. read input file
            2. calcullate hop a/s, delta t a/s,etc.
            3. processing file by moving frame window
            4. Interpolation
            Input:
                fpath - string, path to processing file, wav
            Result:
                res - np.array, result processed signal

        """
        try:
            # [1]
            samples = self.loadSoundFile(fpath)
            
            # [2]
            fduration = samples.shape[0]/self.fs
            hop_a = int( (100 - self.overlap)/100 * self.frame_sz )
            hop_s = int(np.ceil(hop_a * self.factor))
            n_frames = np.floor( (samples.shape[0])/hop_a ) # - self.frame_sz
            res_sz = int( (n_frames) * np.ceil(hop_s) + self.frame_sz )

            print(
                "Input:\n> infile=%s\n\tfs=%f[]\n\tN-samples=%i\n\tfduration=%f\n> Processing parameters:\n\tframe_sz=%i\n\toverlap=%f\n\tn_frames=%s\n\thop_a=%f\n\thop_s=%f\n"%(
                fpath, self.fs, samples.shape[0],fduration, self.frame_sz, self.overlap, n_frames,hop_a, hop_s)
            )

            # [3] Analysis & Processing & Synthesis
            print("Start file processing:")
            self.__sind_res = 0
            self.s_sign = np.zeros( (res_sz,) )
            for sind in tqdm(range(0, samples.shape[0], hop_a)):
                # select processing frame by window
                frame = samples[sind:sind+self.frame_sz]
                frame_a = self.f_window(frame, hop_a)

                frame_fft = self.processing(frame_a, hop_a, hop_s)
                self.synthesis(frame_fft, hop_s)
                
            # [4]
            print("Result interpolation...")
            self.resultInterp()
            print("Processing done")
        except Exception as error:
            raise Exception( "Error [run]: %s"%(error) )

    def save_wav(self, fname="example.wav"):
        wavfile.write(fname, self.fs, self.res)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option( '-p', '--path', type=str, action="store", dest="fpath",
        help="full sound file path, default=\"./example_hq_2ch.wav\"", default="./gs-16b-1c-44100hz_1ch.wav" )
    parser.add_option( '-f', '--factor_fraction', type=int, action="store", dest="factor_fraction",
        help="compression factor fraction (i), factor=2^(i/12), shalle be [-30,30], default=4", default=4 )
    parser.add_option( '-w', '--frame_width', type=int, action="store", dest="frame_sz",
        help="frame size in samples, frame_sz >= 64, default=1024", default=1024 )
    parser.add_option( '-o', '--overlap', type=float, action="store", dest="overlap",
        help="frame overlapping percentage, shalle be [0,100), default=75.0%", default=75. )
    parser.add_option( '-s', '--save_path', type=str, action="store", dest="spath",
        help="full saving processed file path, default=\"example.wav\"", default="example.wav" )

    options, args = parser.parse_args()
    option_dict = vars(options)
    
    vo = Vocoder(
        factor_fraction=option_dict['factor_fraction'],
        frame_sz=option_dict['frame_sz'],
        overlap=option_dict['overlap']
    )
    vo.run( fpath=option_dict['fpath'] )
    vo.save_wav(fname=option_dict['spath'])